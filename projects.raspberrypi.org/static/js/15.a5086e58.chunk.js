(window.webpackJsonp = window.webpackJsonp || []).push([
    [15], {
        473: function(t, e, i) {
            (function(t) {
                ! function() {
                    var e = "function" == typeof Object.defineProperties ? Object.defineProperty : function(t, e, i) {
                            if (i.get || i.set) throw new TypeError("ES3 does not support getters and setters.");
                            t != Array.prototype && t != Object.prototype && (t[e] = i.value)
                        },
                        i = "undefined" != typeof window && window === this ? this : "undefined" != typeof t && null != t ? t : this;

                    function n() {
                        n = function() {}, i.Symbol || (i.Symbol = o)
                    }
                    var r = 0;

                    function o(t) {
                        return "jscomp_symbol_" + (t || "") + r++
                    }

                    function a() {
                        n();
                        var t = i.Symbol.iterator;
                        t || (t = i.Symbol.iterator = i.Symbol("iterator")), "function" != typeof Array.prototype[t] && e(Array.prototype, t, {
                            configurable: !0,
                            writable: !0,
                            value: function() {
                                return s(this)
                            }
                        }), a = function() {}
                    }

                    function s(t) {
                        var e = 0;
                        return function(t) {
                            return a(), (t = {
                                next: t
                            })[i.Symbol.iterator] = function() {
                                return this
                            }, t
                        }(function() {
                            return e < t.length ? {
                                done: !1,
                                value: t[e++]
                            } : {
                                done: !0
                            }
                        })
                    }

                    function c(t) {
                        a(), n(), a();
                        var e = t[Symbol.iterator];
                        return e ? e.call(t) : s(t)
                    }

                    function h(t) {
                        if (!(t instanceof Array)) {
                            t = c(t);
                            for (var e, i = []; !(e = t.next()).done;) i.push(e.value);
                            t = i
                        }
                        return t
                    }
                    var u = window.Element.prototype,
                        l = u.matches || u.matchesSelector || u.webkitMatchesSelector || u.mozMatchesSelector || u.msMatchesSelector || u.oMatchesSelector;

                    function f(t, e) {
                        if (t && 1 == t.nodeType && e) {
                            if ("string" == typeof e || 1 == e.nodeType) return t == e || d(t, e);
                            if ("length" in e)
                                for (var i, n = 0; i = e[n]; n++)
                                    if (t == i || d(t, i)) return !0
                        }
                        return !1
                    }

                    function d(t, e) {
                        if ("string" != typeof e) return !1;
                        if (l) return l.call(t, e);
                        e = t.parentNode.querySelectorAll(e);
                        for (var i, n = 0; i = e[n]; n++)
                            if (i == t) return !0;
                        return !1
                    }

                    function p(t, e, i) {
                        function n(t) {
                            var n;
                            if (o.composed && "function" == typeof t.composedPath)
                                for (var r, a = t.composedPath(), s = 0; r = a[s]; s++) 1 == r.nodeType && f(r, e) && (n = r);
                            else t: {
                                if ((n = t.target) && 1 == n.nodeType && e)
                                    for (n = [n].concat(function(t) {
                                            for (var e = []; t && t.parentNode && 1 == t.parentNode.nodeType;) t = t.parentNode, e.push(t);
                                            return e
                                        }(n)), a = 0; s = n[a]; a++)
                                        if (f(s, e)) {
                                            n = s;
                                            break t
                                        }
                                n = void 0
                            }
                            n && i.call(n, t, n)
                        }
                        var r = document,
                            o = void 0 === (o = {
                                composed: !0,
                                B: !0
                            }) ? {} : o;
                        return r.addEventListener(t, n, o.B), {
                            j: function() {
                                r.removeEventListener(t, n, o.B)
                            }
                        }
                    }
                    var v = /:(80|443)$/,
                        g = document.createElement("a"),
                        b = {};

                    function m(t) {
                        if (t = t && "." != t ? t : location.href, b[t]) return b[t];
                        if (g.href = t, "." == t.charAt(0) || "/" == t.charAt(0)) return m(g.href);
                        var e = "0" == (e = "80" == g.port || "443" == g.port ? "" : g.port) ? "" : e,
                            i = g.host.replace(v, "");
                        return b[t] = {
                            hash: g.hash,
                            host: i,
                            hostname: g.hostname,
                            href: g.href,
                            origin: g.origin ? g.origin : g.protocol + "//" + i,
                            pathname: "/" == g.pathname.charAt(0) ? g.pathname : "/" + g.pathname,
                            port: e,
                            protocol: g.protocol,
                            search: g.search
                        }
                    }
                    var y = [];

                    function w(t, e) {
                        var i = this;
                        this.context = t, this.v = e, this.f = (this.c = /Task$/.test(e)) ? t.get(e) : t[e], this.b = [], this.a = [], this.g = function(t) {
                            for (var e = [], n = 0; n < arguments.length; ++n) e[n - 0] = arguments[n];
                            return i.a[i.a.length - 1].apply(null, [].concat(h(e)))
                        }, this.c ? t.set(e, this.g) : t[e] = this.g
                    }

                    function T(t, e, i) {
                        (t = S(t, e)).b.push(i), k(t)
                    }

                    function I(t, e, i) {
                        -1 < (i = (t = S(t, e)).b.indexOf(i)) && (t.b.splice(i, 1), 0 < t.b.length ? k(t) : t.j())
                    }

                    function k(t) {
                        t.a = [];
                        for (var e, i = 0; e = t.b[i]; i++) {
                            var n = t.a[i - 1] || t.f.bind(t.context);
                            t.a.push(e(n))
                        }
                    }

                    function S(t, e) {
                        var i = y.filter(function(i) {
                            return i.context == t && i.v == e
                        })[0];
                        return i || (i = new w(t, e), y.push(i)), i
                    }

                    function E(t, e, i, n, r, o) {
                        if ("function" == typeof n) {
                            var a = i.get("buildHitTask");
                            return {
                                buildHitTask: function(i) {
                                    i.set(t, null, !0), i.set(e, null, !0), n(i, r, o), a(i)
                                }
                            }
                        }
                        return j({}, t, e)
                    }

                    function O(t, e) {
                        var i = function(t) {
                                var e = {};
                                if (!t || 1 != t.nodeType) return e;
                                if (!(t = t.attributes).length) return {};
                                for (var i, n = 0; i = t[n]; n++) e[i.name] = i.value;
                                return e
                            }(t),
                            n = {};
                        return Object.keys(i).forEach(function(t) {
                            if (!t.indexOf(e) && t != e + "on") {
                                var r = i[t];
                                "true" == r && (r = !0), "false" == r && (r = !1), t = function(t) {
                                    return t.replace(/[\-\_]+(\w?)/g, function(t, e) {
                                        return e.toUpperCase()
                                    })
                                }(t.slice(e.length)), n[t] = r
                            }
                        }), n
                    }
                    w.prototype.j = function() {
                        var t = y.indexOf(this); - 1 < t && (y.splice(t, 1), this.c ? this.context.set(this.v, this.f) : this.context[this.v] = this.f)
                    };
                    var x = {};
                    var j = Object.assign || function(t, e) {
                        for (var i = [], n = 1; n < arguments.length; ++n) i[n - 1] = arguments[n];
                        n = 0;
                        for (var r = i.length; n < r; n++) {
                            var o, a = Object(i[n]);
                            for (o in a) Object.prototype.hasOwnProperty.call(a, o) && (t[o] = a[o])
                        }
                        return t
                    };
                    var L = function t(e) {
                        return e ? (e ^ 16 * Math.random() >> e / 4).toString(16) : "10000000-1000-4000-8000-100000000000".replace(/[018]/g, t)
                    };

                    function A(t, e) {
                        var i = window.GoogleAnalyticsObject || "ga";
                        window[i] = window[i] || function(t) {
                            for (var e = [], n = 0; n < arguments.length; ++n) e[n - 0] = arguments[n];
                            (window[i].q = window[i].q || []).push(e)
                        }, window.gaDevIds = window.gaDevIds || [], 0 > window.gaDevIds.indexOf("i5iSjo") && window.gaDevIds.push("i5iSjo"), window[i]("provide", t, e), window.gaplugins = window.gaplugins || {}, window.gaplugins[t.charAt(0).toUpperCase() + t.slice(1)] = e
                    }
                    var _ = {
                            C: 1,
                            D: 2,
                            L: 3,
                            M: 4,
                            N: 5,
                            G: 6,
                            H: 7,
                            O: 8,
                            I: 9,
                            F: 10
                        },
                        M = Object.keys(_).length;

                    function C(t, e) {
                        t.set("&_av", "2.4.1");
                        var i = t.get("&_au");
                        if ((i = parseInt(i || "0", 16).toString(2)).length < M)
                            for (var n = M - i.length; n;) i = "0" + i, n--;
                        e = M - e, i = i.substr(0, e) + 1 + i.substr(e + 1), t.set("&_au", parseInt(i || "0", 2).toString(16))
                    }

                    function D(t, e) {
                        C(t, _.C), this.a = j({}, e), this.g = t, this.b = this.a.stripQuery && this.a.queryDimensionIndex ? "dimension" + this.a.queryDimensionIndex : null, this.f = this.f.bind(this), this.c = this.c.bind(this), T(t, "get", this.f), T(t, "buildHitTask", this.c)
                    }

                    function P(t, e) {
                        var i = m(e.page || e.location),
                            n = i.pathname;
                        if (t.a.indexFilename) {
                            var r = n.split("/");
                            t.a.indexFilename == r[r.length - 1] && (r[r.length - 1] = "", n = r.join("/"))
                        }
                        return "remove" == t.a.trailingSlash ? n = n.replace(/\/+$/, "") : "add" == t.a.trailingSlash && (/\.\w+$/.test(n) || "/" == n.substr(-1) || (n += "/")), n = {
                            page: n + (t.a.stripQuery ? H(t, i.search) : i.search)
                        }, e.location && (n.location = e.location), t.b && (n[t.b] = i.search.slice(1) || "(not set)"), "function" == typeof t.a.urlFieldsFilter ? (i = {
                            page: (e = t.a.urlFieldsFilter(n, m)).page,
                            location: e.location
                        }, t.b && (i[t.b] = e[t.b]), i) : n
                    }

                    function H(t, e) {
                        if (Array.isArray(t.a.queryParamsWhitelist)) {
                            var i = [];
                            return e.slice(1).split("&").forEach(function(e) {
                                var n = c(e.split("="));
                                e = n.next().value, n = n.next().value, -1 < t.a.queryParamsWhitelist.indexOf(e) && n && i.push([e, n])
                            }), i.length ? "?" + i.map(function(t) {
                                return t.join("=")
                            }).join("&") : ""
                        }
                        return ""
                    }

                    function q() {
                        this.a = {}
                    }
                    D.prototype.f = function(t) {
                        var e = this;
                        return function(i) {
                            if ("page" == i || i == e.b) {
                                var n = {
                                    location: t("location"),
                                    page: t("page")
                                };
                                return P(e, n)[i]
                            }
                            return t(i)
                        }
                    }, D.prototype.c = function(t) {
                        var e = this;
                        return function(i) {
                            var n = P(e, {
                                location: i.get("location"),
                                page: i.get("page")
                            });
                            i.set(n, null, !0), t(i)
                        }
                    }, D.prototype.remove = function() {
                        I(this.g, "get", this.f), I(this.g, "buildHitTask", this.c)
                    }, A("cleanUrlTracker", D), q.prototype.J = function(t, e) {
                        for (var i = [], n = 1; n < arguments.length; ++n) i[n - 1] = arguments[n];
                        (this.a[t] = this.a[t] || []).forEach(function(t) {
                            return t.apply(null, [].concat(h(i)))
                        })
                    };
                    var U, F = {},
                        N = !1;

                    function R(t, e) {
                        e = void 0 === e ? {} : e, this.a = {}, this.b = t, this.m = e, this.i = null
                    }

                    function V(t, e, i) {
                        return t = ["autotrack", t, e].join(":"), F[t] || (F[t] = new R(t, i), N || (window.addEventListener("storage", Z), N = !0)), F[t]
                    }

                    function K() {
                        if (null != U) return U;
                        try {
                            window.localStorage.setItem("autotrack", "autotrack"), window.localStorage.removeItem("autotrack"), U = !0
                        } catch (t) {
                            U = !1
                        }
                        return U
                    }

                    function Y(t) {
                        if (t.i = {}, K()) try {
                            window.localStorage.removeItem(t.b)
                        } catch (e) {}
                    }

                    function Z(t) {
                        var e = F[t.key];
                        if (e) {
                            var i = j({}, e.m, B(t.oldValue));
                            t = j({}, e.m, B(t.newValue)), e.i = t, e.J("externalSet", t, i)
                        }
                    }

                    function B(t) {
                        var e = {};
                        if (t) try {
                            e = JSON.parse(t)
                        } catch (i) {}
                        return e
                    }! function(t, e) {
                        function i() {}
                        for (var n in i.prototype = e.prototype, t.P = e.prototype, t.prototype = new i, t.prototype.constructor = t, e)
                            if (Object.defineProperties) {
                                var r = Object.getOwnPropertyDescriptor(e, n);
                                r && Object.defineProperty(t, n, r)
                            } else t[n] = e[n]
                    }(R, q), R.prototype.get = function() {
                        if (this.i) return this.i;
                        if (K()) try {
                            this.i = B(window.localStorage.getItem(this.b))
                        } catch (t) {}
                        return this.i = j({}, this.m, this.i)
                    }, R.prototype.set = function(t) {
                        if (this.i = j({}, this.m, this.i, t), K()) try {
                            var e = JSON.stringify(this.i);
                            window.localStorage.setItem(this.b, e)
                        } catch (i) {}
                    }, R.prototype.j = function() {
                        delete F[this.b], Object.keys(F).length || (window.removeEventListener("storage", Z), N = !1)
                    };
                    var J = {};

                    function W(t, e, i) {
                        this.f = t, this.timeout = e || $, this.timeZone = i, this.b = this.b.bind(this), T(t, "sendHitTask", this.b);
                        try {
                            this.c = new Intl.DateTimeFormat("en-US", {
                                timeZone: this.timeZone
                            })
                        } catch (n) {}
                        this.a = V(t.get("trackingId"), "session", {
                            hitTime: 0,
                            isExpired: !1
                        }), this.a.get().id || this.a.set({
                            id: L()
                        })
                    }

                    function G(t, e, i) {
                        var n = t.get("trackingId");
                        return J[n] ? J[n] : J[n] = new W(t, e, i)
                    }

                    function Q(t) {
                        return t.a.get().id
                    }
                    W.prototype.isExpired = function(t) {
                        if ((t = void 0 === t ? Q(this) : t) != Q(this)) return !0;
                        if ((t = this.a.get()).isExpired) return !0;
                        var e = t.hitTime;
                        return !!(e && (t = new Date, e = new Date(e), t - e > 6e4 * this.timeout || this.c && this.c.format(t) != this.c.format(e)))
                    }, W.prototype.b = function(t) {
                        var e = this;
                        return function(i) {
                            t(i);
                            var n = i.get("sessionControl");
                            i = "start" == n || e.isExpired();
                            n = "end" == n;
                            var r = e.a.get();
                            r.hitTime = +new Date, i && (r.isExpired = !1, r.id = L()), n && (r.isExpired = !0), e.a.set(r)
                        }
                    }, W.prototype.j = function() {
                        I(this.f, "sendHitTask", this.b), this.a.j(), delete J[this.f.get("trackingId")]
                    };
                    var $ = 30;

                    function z(t, e) {
                        C(t, _.F), window.addEventListener && (this.b = j({
                            increaseThreshold: 20,
                            sessionTimeout: $,
                            fieldsObj: {}
                        }, e), this.f = t, this.c = tt(this), this.g = function(t) {
                            var e;
                            return function(i) {
                                for (var n = [], r = 0; r < arguments.length; ++r) n[r - 0] = arguments[r];
                                clearTimeout(e), e = setTimeout(function() {
                                    return t.apply(null, [].concat(h(n)))
                                }, 500)
                            }
                        }(this.g.bind(this)), this.l = this.l.bind(this), this.a = V(t.get("trackingId"), "plugins/max-scroll-tracker"), this.h = G(t, this.b.sessionTimeout, this.b.timeZone), T(t, "set", this.l), X(this))
                    }

                    function X(t) {
                        100 > (t.a.get()[t.c] || 0) && window.addEventListener("scroll", t.g)
                    }

                    function tt(t) {
                        return (t = m(t.f.get("page") || t.f.get("location"))).pathname + t.search
                    }

                    function et(t, e) {
                        var i = this;
                        C(t, _.G), window.addEventListener && (this.a = j({
                            events: ["click"],
                            linkSelector: "a, area",
                            shouldTrackOutboundLink: this.shouldTrackOutboundLink,
                            fieldsObj: {},
                            attributePrefix: "ga-"
                        }, e), this.c = t, this.f = this.f.bind(this), this.b = {}, this.a.events.forEach(function(t) {
                            i.b[t] = p(t, i.a.linkSelector, i.f)
                        }))
                    }
                    z.prototype.g = function() {
                        var t = document.documentElement,
                            e = document.body;
                        t = Math.min(100, Math.max(0, Math.round(window.pageYOffset / (Math.max(t.offsetHeight, t.scrollHeight, e.offsetHeight, e.scrollHeight) - window.innerHeight) * 100)));
                        if ((e = Q(this.h)) != this.a.get().sessionId && (Y(this.a), this.a.set({
                                sessionId: e
                            })), this.h.isExpired(this.a.get().sessionId)) Y(this.a);
                        else if (t > (e = this.a.get()[this.c] || 0) && (100 != t && 100 != e || window.removeEventListener("scroll", this.g), e = t - e, 100 == t || e >= this.b.increaseThreshold)) {
                            var i = {};
                            this.a.set((i[this.c] = t, i.sessionId = Q(this.h), i)), t = {
                                transport: "beacon",
                                eventCategory: "Max Scroll",
                                eventAction: "increase",
                                eventValue: e,
                                eventLabel: String(t),
                                nonInteraction: !0
                            }, this.b.maxScrollMetricIndex && (t["metric" + this.b.maxScrollMetricIndex] = e), this.f.send("event", E(t, this.b.fieldsObj, this.f, this.b.hitFilter))
                        }
                    }, z.prototype.l = function(t) {
                        var e = this;
                        return function(i, n) {
                            t(i, n);
                            var r = {};
                            ("object" == typeof i && null !== i ? i : (r[i] = n, r)).page && (i = e.c, e.c = tt(e), e.c != i && X(e))
                        }
                    }, z.prototype.remove = function() {
                        this.h.j(), window.removeEventListener("scroll", this.g), I(this.f, "set", this.l)
                    }, A("maxScrollTracker", z), et.prototype.f = function(t, e) {
                        var i = this;
                        if (this.a.shouldTrackOutboundLink(e, m)) {
                            var n = e.getAttribute("href") || e.getAttribute("xlink:href"),
                                r = m(n),
                                o = E(r = {
                                    transport: "beacon",
                                    eventCategory: "Outbound Link",
                                    eventAction: t.type,
                                    eventLabel: r.href
                                }, j({}, this.a.fieldsObj, O(e, this.a.attributePrefix)), this.c, this.a.hitFilter, e, t);
                            if (navigator.sendBeacon || "click" != t.type || "_blank" == e.target || t.metaKey || t.ctrlKey || t.shiftKey || t.altKey || 1 < t.which) this.c.send("event", o);
                            else {
                                window.addEventListener("click", function e() {
                                    if (window.removeEventListener("click", e), !t.defaultPrevented) {
                                        t.preventDefault();
                                        var r = o.hitCallback;
                                        o.hitCallback = function(t) {
                                            function e() {
                                                i || (i = !0, t())
                                            }
                                            var i = !1;
                                            return setTimeout(e, 2e3), e
                                        }(function() {
                                            "function" == typeof r && r(), location.href = n
                                        })
                                    }
                                    i.c.send("event", o)
                                })
                            }
                        }
                    }, et.prototype.shouldTrackOutboundLink = function(t, e) {
                        return (e = e(t = t.getAttribute("href") || t.getAttribute("xlink:href"))).hostname != location.hostname && "http" == e.protocol.slice(0, 4)
                    }, et.prototype.remove = function() {
                        var t = this;
                        Object.keys(this.b).forEach(function(e) {
                            t.b[e].j()
                        })
                    }, A("outboundLinkTracker", et);
                    var it = L();

                    function nt(t, e) {
                        var i = this;
                        C(t, _.H), document.visibilityState && (this.a = j({
                            sessionTimeout: $,
                            visibleThreshold: 5e3,
                            sendInitialPageview: !1,
                            fieldsObj: {}
                        }, e), this.b = t, this.h = document.visibilityState, this.s = null, this.w = !1, this.l = this.l.bind(this), this.f = this.f.bind(this), this.o = this.o.bind(this), this.u = this.u.bind(this), this.c = V(t.get("trackingId"), "plugins/page-visibility-tracker"), function(t, e) {
                            (t.a.externalSet = t.a.externalSet || []).push(e)
                        }(this.c, this.u), this.g = G(t, this.a.sessionTimeout, this.a.timeZone), T(t, "set", this.l), window.addEventListener("unload", this.o), document.addEventListener("visibilitychange", this.f), function(t, e) {
                            function i() {
                                clearTimeout(r.timeout), r.send && I(t, "send", r.send), delete x[n], r.A.forEach(function(t) {
                                    return t()
                                })
                            }
                            var n = t.get("trackingId"),
                                r = x[n] = x[n] || {};
                            clearTimeout(r.timeout), r.timeout = setTimeout(i, 0), r.A = r.A || [], r.A.push(e), r.send || (r.send = function(t) {
                                return function(e) {
                                    for (var n = [], r = 0; r < arguments.length; ++r) n[r - 0] = arguments[r];
                                    i(), t.apply(null, [].concat(h(n)))
                                }
                            }, T(t, "send", r.send))
                        }(this.b, function() {
                            if ("visible" == document.visibilityState) i.a.sendInitialPageview && (ot(i, {
                                K: !0
                            }), i.w = !0), i.c.set({
                                time: +new Date,
                                state: "visible",
                                pageId: it,
                                sessionId: Q(i.g)
                            });
                            else if (i.a.sendInitialPageview && i.a.pageLoadsMetricIndex) {
                                var t = ((t = {}).transport = "beacon", t.eventCategory = "Page Visibility", t.eventAction = "page load", t.eventLabel = "(not set)", t["metric" + i.a.pageLoadsMetricIndex] = 1, t.nonInteraction = !0, t);
                                i.b.send("event", E(t, i.a.fieldsObj, i.b, i.a.hitFilter))
                            }
                        }))
                    }

                    function rt(t, e, i) {
                        var n = ((n = {
                            hitTime: i = (i || {}).hitTime
                        }) || {}).hitTime;
                        (e = e.time ? (n || +new Date) - e.time : 0) && e >= t.a.visibleThreshold && (n = {
                            transport: "beacon",
                            nonInteraction: !0,
                            eventCategory: "Page Visibility",
                            eventAction: "track",
                            eventValue: e = Math.round(e / 1e3),
                            eventLabel: "(not set)"
                        }, i && (n.queueTime = +new Date - i), t.a.visibleMetricIndex && (n["metric" + t.a.visibleMetricIndex] = e), t.b.send("event", E(n, t.a.fieldsObj, t.b, t.a.hitFilter)))
                    }

                    function ot(t, e) {
                        e = (i = e || {}).hitTime;
                        var i = i.K,
                            n = {
                                transport: "beacon"
                            };
                        e && (n.queueTime = +new Date - e), i && t.a.pageLoadsMetricIndex && (n["metric" + t.a.pageLoadsMetricIndex] = 1), t.b.send("pageview", E(n, t.a.fieldsObj, t.b, t.a.hitFilter))
                    }

                    function at(t, e) {
                        C(t, _.I), history.pushState && window.addEventListener && (this.a = j({
                            shouldTrackUrlChange: this.shouldTrackUrlChange,
                            trackReplaceState: !1,
                            fieldsObj: {},
                            hitFilter: null
                        }, e), this.g = t, this.h = location.pathname + location.search, this.c = this.c.bind(this), this.f = this.f.bind(this), this.b = this.b.bind(this), T(history, "pushState", this.c), T(history, "replaceState", this.f), window.addEventListener("popstate", this.b))
                    }

                    function st(t, e) {
                        setTimeout(function() {
                            var i = t.h,
                                n = location.pathname + location.search;
                            i != n && t.a.shouldTrackUrlChange.call(t, n, i) && (t.h = n, t.g.set({
                                page: n,
                                title: document.title
                            }), (e || t.a.trackReplaceState) && t.g.send("pageview", E({
                                transport: "beacon"
                            }, t.a.fieldsObj, t.g, t.a.hitFilter)))
                        }, 0)
                    }

                    function ct(t, e) {
                        var i = this;
                        if (C(t, _.D), window.addEventListener) {
                            this.a = j({
                                events: ["click"],
                                fieldsObj: {},
                                attributePrefix: "ga-"
                            }, e), this.f = t, this.c = this.c.bind(this);
                            var n = "[" + this.a.attributePrefix + "on]";
                            this.b = {}, this.a.events.forEach(function(t) {
                                i.b[t] = p(t, n, i.c)
                            })
                        }
                    }
                    nt.prototype.f = function() {
                        var t = this;
                        if ("visible" == document.visibilityState || "hidden" == document.visibilityState) {
                            var e = function(t) {
                                    var e = t.c.get();
                                    return "visible" == t.h && "hidden" == e.state && e.pageId != it && (e.state = "visible", e.pageId = it, t.c.set(e)), e
                                }(this),
                                i = {
                                    time: +new Date,
                                    state: document.visibilityState,
                                    pageId: it,
                                    sessionId: Q(this.g)
                                };
                            "visible" == document.visibilityState && this.a.sendInitialPageview && !this.w && (ot(this), this.w = !0), "hidden" == document.visibilityState && this.s && clearTimeout(this.s), this.g.isExpired(e.sessionId) ? (Y(this.c), "hidden" == this.h && "visible" == document.visibilityState && (clearTimeout(this.s), this.s = setTimeout(function() {
                                t.c.set(i), ot(t, {
                                    hitTime: i.time
                                })
                            }, this.a.visibleThreshold))) : (e.pageId == it && "visible" == e.state && rt(this, e), this.c.set(i)), this.h = document.visibilityState
                        }
                    }, nt.prototype.l = function(t) {
                        var e = this;
                        return function(i, n) {
                            var r = {};
                            (r = "object" == typeof i && null !== i ? i : (r[i] = n, r)).page && r.page !== e.b.get("page") && "visible" == e.h && e.f(), t(i, n)
                        }
                    }, nt.prototype.u = function(t, e) {
                        t.time != e.time && (e.pageId != it || "visible" != e.state || this.g.isExpired(e.sessionId) || rt(this, e, {
                            hitTime: t.time
                        }))
                    }, nt.prototype.o = function() {
                        "hidden" != this.h && this.f()
                    }, nt.prototype.remove = function() {
                        this.c.j(), this.g.j(), I(this.b, "set", this.l), window.removeEventListener("unload", this.o), document.removeEventListener("visibilitychange", this.f)
                    }, A("pageVisibilityTracker", nt), at.prototype.c = function(t) {
                        var e = this;
                        return function(i) {
                            for (var n = [], r = 0; r < arguments.length; ++r) n[r - 0] = arguments[r];
                            t.apply(null, [].concat(h(n))), st(e, !0)
                        }
                    }, at.prototype.f = function(t) {
                        var e = this;
                        return function(i) {
                            for (var n = [], r = 0; r < arguments.length; ++r) n[r - 0] = arguments[r];
                            t.apply(null, [].concat(h(n))), st(e, !1)
                        }
                    }, at.prototype.b = function() {
                        st(this, !0)
                    }, at.prototype.shouldTrackUrlChange = function(t, e) {
                        return !(!t || !e)
                    }, at.prototype.remove = function() {
                        I(history, "pushState", this.c), I(history, "replaceState", this.f), window.removeEventListener("popstate", this.b)
                    }, A("urlChangeTracker", at), ct.prototype.c = function(t, e) {
                        var i = this.a.attributePrefix;
                        if (!(0 > e.getAttribute(i + "on").split(/\s*,\s*/).indexOf(t.type))) {
                            i = O(e, i);
                            var n = j({}, this.a.fieldsObj, i);
                            this.f.send(i.hitType || "event", E({
                                transport: "beacon"
                            }, n, this.f, this.a.hitFilter, e, t))
                        }
                    }, ct.prototype.remove = function() {
                        var t = this;
                        Object.keys(this.b).forEach(function(e) {
                            t.b[e].j()
                        })
                    }, A("eventTracker", ct)
                }()
            }).call(this, i(35))
        },
        556: function(t, e, i) {
            "use strict";
            i.r(e), i.d(e, "init", function() {
                return l
            }), i.d(e, "trackError", function() {
                return f
            });
            var n = i(57),
                r = (i(473), {
                    TRACKING_VERSION: "dimension1",
                    CLIENT_ID: "dimension2",
                    WINDOW_ID: "dimension3",
                    HIT_ID: "dimension4",
                    HIT_TIME: "dimension5",
                    HIT_TYPE: "dimension6",
                    HIT_SOURCE: "dimension7",
                    VISIBILITY_STATE: "dimension8",
                    URL_QUERY_PARAMS: "dimension9"
                }),
                o = "metric1",
                a = "metric2",
                s = "metric3",
                c = "metric4",
                h = "metric5",
                u = "metric6",
                l = function() {
                    window.ga = window.ga || function() {
                        for (var t = arguments.length, e = new Array(t), i = 0; i < t; i++) e[i] = arguments[i];
                        return (ga.q = ga.q || []).push(e)
                    }, d(), p(), v(), g(), b(), m(), y()
                },
                f = function() {
                    var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                        e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                    ga("send", "event", Object.assign({
                        eventCategory: "Error",
                        eventAction: t.name || "(no error name)",
                        eventLabel: "".concat(t.message, "\n").concat(t.stack || "(no stack trace)"),
                        nonInteraction: !0
                    }, e))
                },
                d = function() {
                    ga("create", "UA-46270871-8", "auto"), ga("require", "linkid"), ga("set", "transport", "beacon")
                },
                p = function() {
                    var t = window.__e && window.__e.q || [],
                        e = function(t) {
                            var e = t.error || {
                                message: "".concat(t.message, " (").concat(t.lineno, ":").concat(t.colno, ")")
                            };
                            f(e, {
                                eventCategory: "Uncaught Error"
                            })
                        },
                        i = !0,
                        n = !1,
                        r = void 0;
                    try {
                        for (var o, a = t[Symbol.iterator](); !(i = (o = a.next()).done); i = !0) {
                            e(o.value)
                        }
                    } catch (s) {
                        n = !0, r = s
                    } finally {
                        try {
                            i || null == a.return || a.return()
                        } finally {
                            if (n) throw r
                        }
                    }
                    window.addEventListener("error", e)
                },
                v = function() {
                    Object.keys(r).forEach(function(t) {
                        ga("set", r[t], "(not set)")
                    }), ga(function(t) {
                        var e;
                        t.set((e = {}, Object(n.a)(e, r.TRACKING_VERSION, "1"), Object(n.a)(e, r.CLIENT_ID, t.get("clientId")), Object(n.a)(e, r.WINDOW_ID, T()), e))
                    }), ga(function(t) {
                        var e = t.get("buildHitTask");
                        t.set("buildHitTask", function(t) {
                            var i = t.get("queueTime") || 0;
                            t.set(r.HIT_TIME, String(new Date - i), !0), t.set(r.HIT_ID, T(), !0), t.set(r.HIT_TYPE, t.get("hitType"), !0), t.set(r.VISIBILITY_STATE, document.visibilityState, !0), e(t)
                        })
                    })
                },
                g = function() {
                    ga("set", "anonymizeIp", !0)
                },
                b = function() {
                    ga("require", "cleanUrlTracker", {
                        stripQuery: !0,
                        queryDimensionIndex: w(r.URL_QUERY_PARAMS),
                        trailingSlash: "remove"
                    }), ga("require", "maxScrollTracker", {
                        sessionTimeout: 30,
                        timeZone: "Europe/London",
                        maxScrollMetricIndex: w(h)
                    }), ga("require", "outboundLinkTracker", {
                        events: ["click", "contextmenu"]
                    }), ga("require", "pageVisibilityTracker", {
                        sendInitialPageview: !0,
                        pageLoadsMetricIndex: w(u),
                        visibleMetricIndex: w(c),
                        timeZone: "Europe/London",
                        fieldsObj: Object(n.a)({}, r.HIT_SOURCE, "pageVisibilityTracker")
                    }), ga("require", "urlChangeTracker", {
                        fieldsObj: Object(n.a)({}, r.HIT_SOURCE, "urlChangeTracker")
                    }), ga("require", "eventTracker", {
                        attributePrefix: "data-"
                    })
                },
                m = function() {
                    ga("send", "pageview", Object(n.a)({}, r.HIT_SOURCE, "pageload"))
                },
                y = function t() {
                    if (window.performance && window.performance.timing)
                        if ("complete" === document.readyState) {
                            var e, i = performance.timing,
                                r = i.navigationStart,
                                c = Math.round(i.responseEnd - r),
                                h = Math.round(i.domContentLoadedEventStart - r),
                                u = Math.round(i.loadEventStart - r);
                            if (function() {
                                    for (var t = arguments.length, e = new Array(t), i = 0; i < t; i++) e[i] = arguments[i];
                                    return e.every(function(t) {
                                        return t > 0 && t < 6e6
                                    })
                                }(c, h, u)) ga("send", "event", (e = {
                                eventCategory: "Navigation Timing",
                                eventAction: "track",
                                nonInteraction: !0
                            }, Object(n.a)(e, o, c), Object(n.a)(e, a, h), Object(n.a)(e, s, u), e))
                        } else window.addEventListener("load", t)
                },
                w = function(t) {
                    return Number(/\d+$/.exec(t)[0])
                },
                T = function t(e) {
                    return e ? (e ^ 16 * Math.random() >> e / 4).toString(16) : ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, t)
                }
        }
    }
]);
//# sourceMappingURL=15.a5086e58.chunk.js.map