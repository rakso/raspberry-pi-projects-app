import classnames from 'classnames'
import PropTypes from 'prop-types'
import React from 'react'
import {
    translate
} from 'react-i18next'

import HtmlHead from 'components/shared/HtmlHead/HtmlHead'
import Routes from 'components/Routes'

export const App = ({
    i18n
}) => {
    const classNames = classnames('c-i18n-root', {
        'c-i18n-root--rtl': i18n.dir() === 'rtl',
    })
    return ( <
        div className = {
            classNames
        }
        dir = {
            i18n.dir()
        } >
        <
        HtmlHead / >
        <
        Routes / >
        <
        /div>
    )
}

App.propTypes = {
    i18n: PropTypes.object.isRequired,
}

export default translate('translations')(App)