import Loadable from 'react-loadable'
import Loader from 'components/shared/Loader/Loader'
import NoMatch from 'components/shared/NoMatch/NoMatch'

// The `/* webpackChunkName: 'CodeClubProjectListContainer' */` comment gives the
// bundle for that chunk a file name that includes `CodeClubProjectListContainer`.
// See https://webpack.js.org/api/module-methods/#import-

const AuthenticationContainerLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'ProjectPrintContainer' */
            'containers/AuthenticationContainer/AuthenticationContainer'
        ),
    error: NoMatch,
    loading: Loader,
})

const SilentRenewContainerLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'SilentRenewContainerLoader' */
            'containers/AuthenticationContainer/SilentRenewContainer'
        ),
    error: NoMatch,
    loading: Loader,
})

const CodeClubLoader = Loadable({
    loader: () =>
        import ( /* webpackChunkName: 'CodeClub' */ 'components/CodeClub/CodeClub'),
    error: NoMatch,
    loading: Loader,
})

const CoderDojoLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'CoderDojo' */
            'components/CoderDojo/CoderDojo'
        ),
    error: NoMatch,
    loading: Loader,
})

const CodeClubHeaderLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'CodeClub' */
            'components/shared/CodeClubHeader/CodeClubHeader'
        ),
    error: NoMatch,
    loading: Loader,
})

const CoderDojoHeaderLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'CoderDojo' */
            'components/shared/CoderDojoHeader/CoderDojoHeader'
        ),
    error: NoMatch,
    loading: Loader,
})

const HomepageLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'Homepage' */
            'containers/HomepageContainer/HomepageContainer'
        ),
    error: NoMatch,
    loading: Loader,
})

const JamHeaderLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'Jam' */
            'components/shared/JamHeader/JamHeader'
        ),
    error: NoMatch,
    loading: Loader,
})

const PathwayContainerLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'PathwayContainer' */
            'containers/PathwayContainer/PathwayContainer'
        ),
    error: NoMatch,
    loading: Loader,
})

const PathwayHeaderLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'Pathway' */
            'components/shared/PathwayHeader/PathwayHeader'
        ),
    error: NoMatch,
    loading: Loader,
})

const ProjectContainerLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'ProjectContainer' */
            'containers/ProjectContainer/ProjectContainer'
        ),
    error: NoMatch,
    loading: Loader,
})

const ProgressListContainerLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'ProjectListContainer' */
            'containers/ProgressListContainer/ProgressListContainer'
        ),
    error: NoMatch,
    loading: Loader,
})

const ProjectListContainerLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'ProjectListContainer' */
            'containers/ProjectListContainer/ProjectListContainer'
        ),
    error: NoMatch,
    loading: Loader,
})

const ProjectPrintContainerLoader = Loadable({
    loader: () =>
        import (
            /* webpackChunkName: 'ProjectPrintContainer' */
            'containers/ProjectPrintContainer/ProjectPrintContainer'
        ),
    error: NoMatch,
    loading: Loader,
})

export {
    AuthenticationContainerLoader,
    CodeClubLoader,
    CoderDojoLoader,
    CodeClubHeaderLoader,
    CoderDojoHeaderLoader,
    HomepageLoader,
    JamHeaderLoader,
    PathwayContainerLoader,
    PathwayHeaderLoader,
    ProgressListContainerLoader,
    ProjectContainerLoader,
    ProjectListContainerLoader,
    ProjectPrintContainerLoader,
    SilentRenewContainerLoader,
}