(window.webpackJsonp = window.webpackJsonp || []).push([
    [6], {
        410: function(e, t, a) {
            "use strict";
            a.r(t);
            var r = a(81),
                c = a(82),
                n = a(84),
                o = a(83),
                i = a(85),
                s = a(1),
                l = a.n(s),
                u = a(86),
                p = a(137),
                f = a(64),
                d = a(21),
                m = a(14),
                j = a(462),
                h = a.n(j),
                b = a(465),
                k = a(11),
                v = a(442),
                g = Object(u.b)(function(e) {
                    return {
                        slug: e.project.slug
                    }
                })(function(e) {
                    var t = e.slug,
                        a = e.text,
                        r = e.userID;
                    return l.a.createElement(v.a, {
                        projectSlug: t,
                        text: a,
                        userID: r
                    })
                }),
                E = a(16),
                _ = a(416),
                O = a(57),
                y = a(2),
                N = a.n(y),
                x = (a(527), function(e) {
                    function t(e) {
                        var a;
                        return Object(r.a)(this, t), (a = Object(n.a)(this, Object(o.a)(t).call(this, e))).feedbackClickHandler = function() {
                            return a.setState({
                                feedbackVisibleElement: "thanks"
                            })
                        }, a.state = {
                            feedbackVisibleElement: "prompt"
                        }, a
                    }
                    return Object(i.a)(t, e), Object(c.a)(t, [{
                        key: "feedbackClassNames",
                        value: function(e) {
                            var t;
                            return N()((t = {}, Object(O.a)(t, "c-project-feedback__".concat(e), !0), Object(O.a)(t, "c-project-feedback__".concat(e, "--hidden"), e !== this.state.feedbackVisibleElement), t))
                        }
                    }, {
                        key: "render",
                        value: function() {
                            var e = this.props,
                                t = e.locale,
                                a = e.projectSlug,
                                r = e.t;
                            return l.a.createElement("div", {
                                className: "c-project-feedback"
                            }, l.a.createElement("h3", {
                                className: "c-project-feedback__title"
                            }, r("project.completion-cta.feedback")), l.a.createElement("div", {
                                className: this.feedbackClassNames("thanks")
                            }, r("project.footer.feedback.thank-you.text")), l.a.createElement("div", {
                                className: "c-project-feedback__options js-project-feedback__options u-clearfix ".concat(this.feedbackClassNames("prompt"))
                            }, l.a.createElement("div", {
                                className: "c-project-feedback__option"
                            }, l.a.createElement(_.a, {
                                onClick: this.feedbackClickHandler,
                                "data-on": "click",
                                "data-event-category": a,
                                "data-event-action": "like",
                                "data-event-label": t,
                                to: "#"
                            }, l.a.createElement("div", {
                                className: "c-project-feedback__option-image c-project-feedback__option-image--0"
                            }), r("project.feedback.like"))), l.a.createElement("div", {
                                className: "c-project-feedback__option"
                            }, l.a.createElement(_.a, {
                                onClick: this.feedbackClickHandler,
                                "data-on": "click",
                                "data-event-category": a,
                                "data-event-action": "ok",
                                "data-event-label": t,
                                to: "#"
                            }, l.a.createElement("div", {
                                className: "c-project-feedback__option-image c-project-feedback__option-image--1"
                            }), r("project.feedback.ok"))), l.a.createElement("div", {
                                className: "c-project-feedback__option"
                            }, l.a.createElement(_.a, {
                                onClick: this.feedbackClickHandler,
                                "data-on": "click",
                                "data-event-category": a,
                                "data-event-action": "dislike",
                                "data-event-label": t,
                                to: "#"
                            }, l.a.createElement("div", {
                                className: "c-project-feedback__option-image c-project-feedback__option-image--2"
                            }), r("project.feedback.dislike")))))
                        }
                    }]), t
                }(s.Component)),
                C = Object(E.c)("translations")(x),
                S = (a(528), function(e) {
                    function t() {
                        return Object(r.a)(this, t), Object(n.a)(this, Object(o.a)(t).apply(this, arguments))
                    }
                    return Object(i.a)(t, e), Object(c.a)(t, [{
                        key: "render",
                        value: function() {
                            var e = this.props,
                                t = e.locale,
                                a = e.projectSlug,
                                r = e.t;
                            return l.a.createElement("div", {
                                className: "c-project-completion-cta"
                            }, l.a.createElement(C, {
                                locale: t,
                                projectSlug: a
                            }), l.a.createElement("h1", {
                                className: "c-project-completion-cta__heading"
                            }, r("project.completion-cta.find-another-project")), l.a.createElement(_.a, {
                                className: "c-project-completion-cta__link  c-button u-m-x4",
                                to: "/".concat(t, "/projects")
                            }, r("project.completion-cta.browse-all-projects")))
                        }
                    }]), t
                }(s.Component)),
                T = Object(E.c)("translations")(S),
                w = (a(529), function(e) {
                    function t(e) {
                        var a;
                        return Object(r.a)(this, t), (a = Object(n.a)(this, Object(o.a)(t).call(this, e))).state = {
                            userID: h.a.v1()
                        }, a
                    }
                    return Object(i.a)(t, e), Object(c.a)(t, [{
                        key: "componentDidMount",
                        value: function() {
                            b.scroller.scrollTo("c-site-header", {
                                duration: k.a.stepScrollDuration,
                                smooth: "easeOutQuad"
                            })
                        }
                    }, {
                        key: "render",
                        value: function() {
                            return l.a.createElement(l.a.Fragment, null, l.a.createElement("main", {
                                className: "c-project__layout c-project__layout--project-complete u-clearfix",
                                id: "c-project__layout"
                            }, l.a.createElement("section", {
                                className: "c-project__content c-project__content--project-complete"
                            }, l.a.createElement("div", {
                                className: "c-project-steps c-project-steps--project-complete"
                            }, l.a.createElement(T, {
                                locale: this.props.locale,
                                projectSlug: this.props.projectSlug
                            })))), l.a.createElement(g, {
                                text: "project.footer.feedback.text",
                                userID: this.state.userID
                            }))
                        }
                    }]), t
                }(s.Component)),
                P = (a(530), function(e, t, a) {
                    var r = "c-project-navigation__link",
                        c = [r];
                    return 0 === t.position && c.push("".concat(r, "--is-first")), e - 1 === t.position && c.push("".concat(r, "--is-current")), t.position < e - 1 && c.push("".concat(r, "--is-done")), t.position === a - 1 && c.push("".concat(r, "--is-last")), c.join(" ")
                }),
                L = function(e) {
                    var t = e.steps,
                        a = e.currentLocale,
                        r = e.currentStepPosition,
                        c = e.slug,
                        n = e.projectTitle;
                    return l.a.createElement("menu", {
                        className: "c-project__menu"
                    }, l.a.createElement("div", {
                        className: "c-project-navigation"
                    }, l.a.createElement("input", {
                        className: "c-project-navigation__toggle-checkbox u-hidden",
                        id: "c-project-navigation__toggle-checkbox",
                        type: "checkbox"
                    }), l.a.createElement("label", {
                        className: "c-project-navigation__toggle-label",
                        htmlFor: "c-project-navigation__toggle-checkbox"
                    }, "Contents", l.a.createElement("span", {
                        className: "c-project-navigation__toggle-icon"
                    })), l.a.createElement("ul", {
                        className: "c-project-navigation__list"
                    }, t.map(function(e, o) {
                        return l.a.createElement("li", {
                            className: "c-project-navigation__item",
                            key: o
                        }, l.a.createElement(_.a, {
                            className: P(r, e, t.length),
                            to: "/".concat(a, "/projects/").concat(c).concat((i = e.position, 0 === i ? "" : "/".concat(i + 1))),
                            "data-on": "click",
                            "data-event-category": n,
                            "data-event-action": "Clicked Navigation Link",
                            role: "link",
                            tabIndex: 0
                        }, e.title));
                        var i
                    }))))
                },
                F = Object(u.b)(function(e) {
                    var t = e.project;
                    return {
                        project: t.project,
                        currentLocale: t.currentLocale,
                        currentStepPosition: t.currentStepPosition,
                        slug: t.slug
                    }
                })(function(e) {
                    var t = e.currentLocale,
                        a = e.currentStepPosition,
                        r = e.project,
                        c = e.slug;
                    return l.a.createElement(L, {
                        steps: r.steps,
                        currentLocale: t,
                        currentStepPosition: a,
                        slug: c,
                        projectTitle: r.title
                    })
                }),
                D = a(436),
                I = a.n(D),
                M = a(437),
                A = (a(531), a(532), a(558)),
                U = a(557),
                H = (a(533), function(e) {
                    var t = e.firstStep,
                        a = e.nextLinkPath,
                        r = e.nextStepTitle,
                        c = e.previousLinkPath,
                        n = e.projectTitle;
                    return l.a.createElement("nav", {
                        className: "c-project-step-navigation"
                    }, l.a.createElement(_.a, {
                        className: N()({
                            "c-button": !0,
                            "c-button--secondary": !0,
                            "c-project-step-navigation__link": !0,
                            "c-project-step-navigation__link--previous": !0,
                            "u-hidden": t
                        }),
                        to: c,
                        "data-on": "click",
                        "data-event-category": n,
                        "data-event-action": "Clicked Step Navigation"
                    }), l.a.createElement(_.a, {
                        className: "c-button c-project-step-navigation__link--next",
                        to: a,
                        "data-on": "click",
                        "data-event-category": n,
                        "data-event-action": "Clicked Step Navigation"
                    }, r))
                }),
                R = a(424),
                V = function(e, t, a, r) {
                    return "".concat(Object(R.d)(e, r)).concat(G(t, a))
                },
                G = function(e, t) {
                    return t.steps.length === e ? "/complete" : "/".concat(e + 1)
                },
                B = function(e, t, a) {
                    return "".concat(Object(R.d)(e, a)).concat(J(t))
                },
                J = function(e) {
                    return 2 === e ? "" : "/".concat(e - 1)
                },
                q = Object(u.b)(function(e) {
                    var t = e.project;
                    return {
                        currentLocale: t.currentLocale,
                        currentStepPosition: t.currentStepPosition,
                        nextStepTitle: t.nextStepTitle,
                        project: t.project,
                        slug: t.slug
                    }
                })(function(e) {
                    var t = e.currentLocale,
                        a = e.currentStepPosition,
                        r = e.nextStepTitle,
                        c = e.project,
                        n = e.slug;
                    return l.a.createElement(H, {
                        firstStep: 1 === a,
                        nextLinkPath: V(t, a, c, n),
                        nextStepTitle: r,
                        previousLinkPath: B(t, a, n),
                        projectTitle: c.title
                    })
                }),
                z = a(456),
                K = a(67),
                Q = a(87),
                W = a(534),
                $ = a.n(W),
                X = function(e) {
                    function t() {
                        var e, a;
                        Object(r.a)(this, t);
                        for (var c = arguments.length, i = new Array(c), s = 0; s < c; s++) i[s] = arguments[s];
                        return (a = Object(n.a)(this, (e = Object(o.a)(t)).call.apply(e, [this].concat(i)))).addCheckboxEventListener = function(e) {
                            e.addEventListener("change", function(t) {
                                var r = t.target.checked,
                                    c = parseInt(e.dataset.taskIndex, 10);
                                a.updateTasksProgress(c, r)
                            }), e.dataset.onChangeEventListenerAdded = !0
                        }, a.childFactoryCreator = function(e) {
                            return function(t) {
                                return l.a.cloneElement(t, {
                                    classNames: Object(R.g)(e)
                                })
                            }
                        }, a.cssTransitionConfig = function() {
                            return {
                                enter: k.a.transitionEnterTimeout,
                                exit: k.a.transitionExitTimeout
                            }
                        }, a.loadTasksProgress = Object(M.a)(I.a.mark(function e() {
                            var t;
                            return I.a.wrap(function(e) {
                                for (;;) switch (e.prev = e.next) {
                                    case 0:
                                        if (0 !== a.tasksCount()) {
                                            e.next = 2;
                                            break
                                        }
                                        return e.abrupt("return", null);
                                    case 2:
                                        return e.next = 4, $.a.getItem(a.tasksProgressKey());
                                    case 4:
                                        if (!(t = e.sent)) {
                                            e.next = 7;
                                            break
                                        }
                                        return e.abrupt("return", JSON.parse(t));
                                    case 7:
                                        return e.abrupt("return", a.initTasksProgress());
                                    case 8:
                                    case "end":
                                        return e.stop()
                                }
                            }, e)
                        })), a.tasksCount = function() {
                            return (a.props.step.content.match(/"(c-project-task)+[ "]/g) || []).length
                        }, a.initTasksProgress = function() {
                            var e = new Array(a.tasksCount());
                            return e.fill(!1), a.saveTasksProgress(e), e
                        }, a.tasksProgressKey = function() {
                            var e = a.props,
                                t = e.slug,
                                r = e.step;
                            return "tasks-progress-".concat(t, "-step-").concat(r.position)
                        }, a.saveTasksProgress = function(e) {
                            $.a.setItem(a.tasksProgressKey(), JSON.stringify(e))
                        }, a.updateTasksProgress = function() {
                            var e = Object(M.a)(I.a.mark(function e(t, r) {
                                var c;
                                return I.a.wrap(function(e) {
                                    for (;;) switch (e.prev = e.next) {
                                        case 0:
                                            return e.next = 2, a.loadTasksProgress();
                                        case 2:
                                            (c = e.sent)[t] = r, a.saveTasksProgress(c);
                                        case 5:
                                        case "end":
                                            return e.stop()
                                    }
                                }, e)
                            }));
                            return function(t, a) {
                                return e.apply(this, arguments)
                            }
                        }(), a.updateTasksCheckboxes = Object(M.a)(I.a.mark(function e() {
                            var t, r, c, n;
                            return I.a.wrap(function(e) {
                                for (;;) switch (e.prev = e.next) {
                                    case 0:
                                        if (0 !== a.tasksCount()) {
                                            e.next = 2;
                                            break
                                        }
                                        return e.abrupt("return");
                                    case 2:
                                        return e.next = 4, a.loadTasksProgress();
                                    case 4:
                                        for (t = e.sent, r = document.querySelectorAll(".c-project-task__checkbox"), n = 0; n < r.length; n += 1)(c = r[n]).dataset.taskIndex = n, t[n] && (c.checked = !0), a.addCheckboxEventListener(c);
                                    case 7:
                                    case "end":
                                        return e.stop()
                                }
                            }, e)
                        })), a.renderContent = function(e) {
                            var t = Object(Q.c)(e),
                                r = N()("c-project-steps__wrapper", {
                                    "c-project-steps__wrapper--challenge": t
                                }),
                                c = t ? l.a.createElement("span", {
                                    className: "c-project-steps__challenge-label"
                                }, a.props.t("project.steps.challenge")) : null;
                            return l.a.createElement("div", {
                                className: r
                            }, c, l.a.createElement(K.p, null, l.a.createElement("div", {
                                className: "c-project-steps__content",
                                dangerouslySetInnerHTML: {
                                    __html: e.content
                                }
                            })), l.a.createElement(q, null))
                        }, a
                    }
                    return Object(i.a)(t, e), Object(c.a)(t, [{
                        key: "componentDidMount",
                        value: function() {
                            var e = Object(M.a)(I.a.mark(function e() {
                                return I.a.wrap(function(e) {
                                    for (;;) switch (e.prev = e.next) {
                                        case 0:
                                            return e.next = 2, this.loadTasksProgress();
                                        case 2:
                                            this.updateTasksCheckboxes();
                                        case 3:
                                        case "end":
                                            return e.stop()
                                    }
                                }, e, this)
                            }));
                            return function() {
                                return e.apply(this, arguments)
                            }
                        }()
                    }, {
                        key: "componentDidUpdate",
                        value: function() {
                            var e = Object(M.a)(I.a.mark(function e() {
                                return I.a.wrap(function(e) {
                                    for (;;) switch (e.prev = e.next) {
                                        case 0:
                                            return e.next = 2, this.loadTasksProgress();
                                        case 2:
                                            this.updateTasksCheckboxes();
                                        case 3:
                                        case "end":
                                            return e.stop()
                                    }
                                }, e, this)
                            }));
                            return function() {
                                return e.apply(this, arguments)
                            }
                        }()
                    }, {
                        key: "render",
                        value: function() {
                            return l.a.createElement("section", {
                                className: "c-project__content"
                            }, this.props.isScratchProject ? l.a.createElement(z.a, null) : "", l.a.createElement("div", {
                                className: "c-project-steps"
                            }, l.a.createElement(A.a, {
                                childFactory: this.childFactoryCreator(this.props.slideDirection)
                            }, l.a.createElement(U.a, {
                                key: this.props.step.position,
                                classNames: Object(R.g)(this.props.slideDirection),
                                timeout: this.cssTransitionConfig()
                            }, this.renderContent(this.props.step)))))
                        }
                    }]), t
                }(s.Component),
                Y = Object(E.c)("translations")(X),
                Z = a(133),
                ee = a(418),
                te = function(e) {
                    function t() {
                        var e, a;
                        Object(r.a)(this, t);
                        for (var c = arguments.length, i = new Array(c), s = 0; s < c; s++) i[s] = arguments[s];
                        return (a = Object(n.a)(this, (e = Object(o.a)(t)).call.apply(e, [this].concat(i)))).isScratchProject = function() {
                            return -1 !== a.props.project.software.indexOf("scratch")
                        }, a
                    }
                    return Object(i.a)(t, e), Object(c.a)(t, [{
                        key: "componentDidMount",
                        value: function() {
                            setTimeout(function() {
                                Object(Q.e)()
                            }, 0), this.trackLastStep()
                        }
                    }, {
                        key: "componentDidUpdate",
                        value: function() {
                            Object(Q.e)(), this.trackLastStep(), this.scrollToHeader()
                        }
                    }, {
                        key: "scrollToHeader",
                        value: function() {
                            b.scroller.scrollTo("c-project__layout", {
                                duration: k.a.stepScrollDuration,
                                smooth: "easeOutQuad"
                            })
                        }
                    }, {
                        key: "trackLastStep",
                        value: function() {
                            this.props.currentStepPosition === this.props.project.steps.length && window.ga("send", {
                                hitType: "event",
                                eventCategory: this.props.project.title,
                                eventAction: "Reached final step"
                            })
                        }
                    }, {
                        key: "render",
                        value: function() {
                            var e = this.props,
                                t = e.currentStepPosition,
                                a = e.project,
                                r = e.slideDirection,
                                c = e.slug,
                                n = e.currentLocale,
                                o = "/".concat(n, "/projects/").concat(c, "/1"),
                                i = -1 !== window.location.href.indexOf(o),
                                s = a.steps[t - 1];
                            return !s && i && (s = a.steps[0]), s ? l.a.createElement(l.a.Fragment, null, l.a.createElement(Z.a, {
                                titlePrefix: "".concat(a.title, " - ").concat(s.title)
                            }), l.a.createElement(Y, {
                                isScratchProject: this.isScratchProject(),
                                slideDirection: r,
                                slug: c,
                                step: s
                            })) : l.a.createElement(ee.a, {
                                to: o
                            })
                        }
                    }]), t
                }(s.Component),
                ae = Object(u.b)(function(e) {
                    var t = e.project;
                    return {
                        currentStepPosition: t.currentStepPosition,
                        project: t.project,
                        slideDirection: t.slideDirection,
                        slug: t.slug,
                        currentLocale: t.currentLocale
                    }
                })(te),
                re = (a(535), function(e) {
                    function t(e) {
                        var a;
                        return Object(r.a)(this, t), (a = Object(n.a)(this, Object(o.a)(t).call(this, e))).userID = h.a.v1(), a
                    }
                    return Object(i.a)(t, e), Object(c.a)(t, [{
                        key: "stepsOrLoader",
                        value: function() {
                            return this.props.project.steps ? l.a.createElement("div", null, l.a.createElement(F, null), l.a.createElement(ae, null)) : l.a.createElement(d.a, {
                                delay: 200
                            })
                        }
                    }, {
                        key: "render",
                        value: function() {
                            return l.a.createElement(l.a.Fragment, null, l.a.createElement("main", {
                                className: "c-project__layout u-clearfix",
                                id: "c-project__layout"
                            }, this.stepsOrLoader()), l.a.createElement(g, {
                                text: "project.footer.feedback.text",
                                userID: this.userID
                            }))
                        }
                    }]), t
                }(s.Component)),
                ce = a(46),
                ne = (a(471), a(423)),
                oe = a(439),
                ie = function(e) {
                    return Object.entries(e).map(function(e) {
                        var t = Object(ce.a)(e, 2),
                            a = t[0],
                            r = t[1];
                        return l.a.createElement(_.a, {
                            className: "c-project-header__link",
                            key: a,
                            to: r.url
                        }, r.label)
                    })
                },
                se = Object(E.c)("translations")(function(e) {
                    var t = e.authentication,
                        a = e.dispatch,
                        r = e.favourite,
                        c = void 0 !== r && r,
                        n = e.hardware,
                        o = e.imageUrl,
                        i = e.locale,
                        s = e.printUrl,
                        u = e.projectComplete,
                        p = e.projectId,
                        f = e.software,
                        d = e.t,
                        m = e.title,
                        j = Object(ne.a)(i, d, n, "hardware"),
                        h = Object(ne.a)(i, d, f, "software");
                    return l.a.createElement("div", {
                        className: "c-project-header ".concat(u ? "c-project-header--project-complete" : "")
                    }, l.a.createElement("div", {
                        className: "c-project-header__info"
                    }, l.a.createElement("h1", {
                        className: "c-project-header__title o-type-display-large"
                    }, m), l.a.createElement("small", null, ie(j), ie(h), l.a.createElement(_.a, {
                        className: "c-project-header__link c-project-header__link--print",
                        to: s
                    }, l.a.createElement("span", {
                        className: "u-hidden"
                    }, d("project.header.print"))), t.user && l.a.createElement(K.b, {
                        className: "c-project-header__link ".concat(c ? "c-project-header__link--favourite" : ""),
                        onClick: function() {
                            return function(e, t, a, r, c) {
                                var n = a && a.user ? a.user.access_token : null;
                                n && e(t ? Object(oe.c)(r, n, c) : Object(oe.a)(r, n, c))
                            }(a, c, t, i, p)
                        }
                    }, l.a.createElement("span", null, c ? "\u2713 " + d("project.header.favourite.added") : "\uff0b " + d("project.header.favourite.addto"))))), l.a.createElement("figure", {
                        className: "c-project-header__illustration"
                    }, l.a.createElement("div", {
                        className: "c-project-header__decoration"
                    }), l.a.createElement("img", {
                        alt: "",
                        className: "c-project-header__image",
                        src: o
                    })), u ? function(e) {
                        return l.a.createElement("h2", {
                            className: "c-project-header__project-complete-notice"
                        }, l.a.createElement("span", {
                            className: "c-project-header__project-complete-icon"
                        }), e("project.header.congratulations"))
                    }(d) : "")
                }),
                le = Object(u.b)(function(e) {
                    var t = e.project,
                        a = t.project,
                        r = t.currentLocale,
                        c = t.slug;
                    return {
                        authentication: e.authentication,
                        currentLocale: r,
                        dispatch: e.dispatch,
                        favourite: Object(R.a)(e.favourites, e.project.project),
                        project: a,
                        slug: c
                    }
                })(function(e) {
                    var t = e.authentication,
                        a = e.currentLocale,
                        r = e.dispatch,
                        c = e.favourite,
                        n = e.project,
                        o = e.projectComplete,
                        i = e.slug;
                    return l.a.createElement(se, {
                        authentication: t,
                        dispatch: r,
                        favourite: c,
                        hardware: n.hardware,
                        imageUrl: n.heroImage,
                        locale: a,
                        printUrl: Object(R.b)(a, i),
                        projectComplete: o,
                        projectId: n.id,
                        software: n.software,
                        title: n.title
                    })
                }),
                ue = function(e) {
                    var t = e.locale,
                        a = e.project,
                        r = e.projectComplete,
                        c = e.projectSlug;
                    return l.a.createElement("div", {
                        className: "c-project ".concat(Object(R.f)(a.theme))
                    }, l.a.createElement("header", {
                        className: "c-project__header"
                    }, l.a.createElement(le, {
                        projectComplete: r
                    })), r ? l.a.createElement(l.a.Fragment, null, l.a.createElement(Z.a, {
                        titlePrefix: "".concat(a.title, " - Complete")
                    }), l.a.createElement(w, {
                        locale: t,
                        projectSlug: c
                    })) : l.a.createElement(re, {
                        project: a
                    }))
                },
                pe = Object(u.b)(function(e) {
                    var t = e.project,
                        a = t.project;
                    return {
                        currentLocale: t.currentLocale,
                        project: a
                    }
                })(function(e) {
                    var t = e.currentLocale,
                        a = e.project,
                        r = e.projectComplete,
                        c = e.projectSlug;
                    return l.a.createElement(ue, {
                        locale: t,
                        project: a,
                        projectComplete: r,
                        projectSlug: c
                    })
                }),
                fe = a(134),
                de = (a(536), a(198)),
                me = Object(E.c)("translations")(function(e) {
                    var t = e.project,
                        a = e.t;
                    return l.a.createElement("div", null, l.a.createElement(Z.a, {
                        titlePrefix: "".concat(t.title)
                    }), l.a.createElement("div", {
                        className: "c-project ".concat(Object(R.f)(t.theme))
                    }, l.a.createElement("header", {
                        className: "c-project__header"
                    }, l.a.createElement("div", {
                        className: "c-project-header"
                    }, l.a.createElement("div", {
                        className: "c-project-header__info"
                    }, l.a.createElement("h1", {
                        className: "c-project-header__title o-type-display-large"
                    }, t.title)), l.a.createElement("figure", {
                        className: "c-project-header__illustration"
                    }, l.a.createElement("div", {
                        className: "c-project-header__decoration"
                    }), l.a.createElement("img", {
                        alt: "",
                        className: "c-project-header__image",
                        src: t.heroImage
                    })))), l.a.createElement("div", {
                        className: "o-container u-mv-x3"
                    }, l.a.createElement("div", {
                        className: "c-project-steps c-project-steps__not-translated"
                    }, l.a.createElement("div", {
                        className: "c-project-steps__wrapper"
                    }, l.a.createElement("h2", null, a("project.not-translated.not-available"), " \u2014", " ", l.a.createElement("a", {
                        href: "https://www.raspberrypi.org/translate/"
                    }, a("project.not-translated.help-us"), "!")), l.a.createElement("div", {
                        className: "u-mv-x3"
                    }, a("project.not-translated.alternatively"), "!"), l.a.createElement("div", {
                        className: "u-pb-x6"
                    }, l.a.createElement(de.a, {
                        filter: function(e) {
                            return function(e, t) {
                                var a = {};
                                return t.availableLocales.forEach(function(t) {
                                    return a[t] = e[t]
                                }), a
                            }(e, t)
                        }
                    })))))))
                }),
                je = a(434),
                he = a(432);
            a.d(t, "ProjectContainer", function() {
                return be
            });
            var be = function(e) {
                function t() {
                    return Object(r.a)(this, t), Object(n.a)(this, Object(o.a)(t).apply(this, arguments))
                }
                return Object(i.a)(t, e), Object(c.a)(t, [{
                    key: "changeProject",
                    value: function(e, t) {
                        var a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : 1,
                            r = this.props,
                            c = r.dispatch,
                            n = r.match,
                            o = r.authentication;
                        o.user && c(Object(oe.b)(n.params.locale, o.user.access_token)), c(Object(je.a)({
                            locale: e,
                            slug: t,
                            position: a
                        }))
                    }
                }, {
                    key: "UNSAFE_componentWillMount",
                    value: function() {
                        var e = this,
                            t = this.props,
                            a = t.match,
                            r = t.projectFetched,
                            c = t.slug,
                            n = t.project;
                        f.a.on("languageChanged", function(t) {
                            e.changeProject(t, a.params.slug, a.params.position)
                        }), 1 === r && a.params.slug === c && a.params.locale === n.locale || this.changeProject(a.params.locale, a.params.slug, a.params.position)
                    }
                }, {
                    key: "UNSAFE_componentWillReceiveProps",
                    value: function(e) {
                        if (Object(R.e)(this.props, e)) {
                            var t = this.props,
                                a = t.dispatch,
                                r = t.project,
                                c = t.slug,
                                n = t.authentication,
                                o = t.progress,
                                i = e.match.params.position,
                                s = n && n.user ? n.user.access_token : null;
                            a(Object(je.c)(i)), s && (o.ids.includes(r.id) ? a(Object(he.c)(r.locale, s, r.id, c, i)) : a(Object(he.a)(r.locale, s, r.id, c, i)))
                        }
                    }
                }, {
                    key: "correctComponent",
                    value: function() {
                        var e = this.props,
                            t = e.error,
                            a = e.match,
                            r = e.projectFetched,
                            c = e.project,
                            n = a.params,
                            o = n.locale,
                            i = n.slug;
                        return -1 === r ? l.a.createElement(m.a, {
                            error: t
                        }) : 1 === r && c.availableLocales ? l.a.createElement(me, {
                            project: c
                        }) : 1 === r || c.title ? l.a.createElement(l.a.Fragment, null, l.a.createElement(p.Helmet, null, l.a.createElement("link", {
                            rel: "canonical",
                            href: "https://projects.raspberrypi.org/".concat(o, "/projects/").concat(i)
                        })), l.a.createElement(pe, {
                            projectComplete: Object(R.c)(a.url),
                            projectSlug: i
                        })) : l.a.createElement(d.a, null)
                    }
                }, {
                    key: "render",
                    value: function() {
                        var e = this.props.match.params.locale;
                        return l.a.createElement("div", null, l.a.createElement(fe.a, {
                            locale: e
                        }), this.correctComponent())
                    }
                }]), t
            }(s.Component);
            t.default = Object(u.b)(function(e) {
                var t = e.project,
                    a = t.error,
                    r = t.projectFetched,
                    c = t.slug,
                    n = t.project,
                    o = e.authentication;
                return {
                    error: a,
                    projectFetched: r,
                    slug: c,
                    project: n,
                    progress: e.progress,
                    favourites: e.favourites,
                    authentication: o,
                    projects: e.projects.entities
                }
            })(be)
        },
        422: function(e, t, a) {
            "use strict";
            a.d(t, "a", function() {
                return i
            });
            var r = a(57),
                c = a(8),
                n = a(135),
                o = a(6),
                i = function(e) {
                    var t = e.method,
                        a = void 0 === t ? "GET" : t,
                        i = e.endpoint,
                        s = e.types,
                        l = e.data,
                        u = void 0 === l ? {} : l,
                        p = Object(n.a)(e, ["method", "endpoint", "types", "data"]);
                    return Object(r.a)({}, o.a, Object(c.a)({
                        method: a,
                        endpoint: i,
                        types: s,
                        data: u
                    }, p))
                }
        },
        423: function(e, t, a) {
            "use strict";
            a.d(t, "a", function() {
                return i
            }), a.d(t, "b", function() {
                return l
            });
            var r = a(46),
                c = function(e, t, a) {
                    return e(["filter.".concat(a, ".options.").concat(t)])
                },
                n = function(e, t, a) {
                    return "/".concat(e, "/projects?").concat(a, "[]=").concat(t)
                },
                o = function(e) {
                    return e && "object" === typeof e
                },
                i = function(e, t, a, r) {
                    var i = {};
                    return o(a) ? (a.map(function(a) {
                        return i[a] = {
                            url: n(e, a, r),
                            label: c(t, a, r)
                        }
                    }), i) : i
                },
                s = function(e, t, a, r) {
                    return o(a) ? function(e, t, a, r) {
                        var n = [];
                        return o(a) ? (a.map(function(e) {
                            return n.push(c(t, e, r))
                        }), n) : n
                    }(0, t, a, r).join(", ") : null
                },
                l = function(e, t, a) {
                    var c = [];
                    return o(a) ? (Object.entries(a).map(function(e) {
                        var a = Object(r.a)(e, 2),
                            n = a[0],
                            i = a[1];
                        return o(i) && i.length > 0 ? c.push(s(0, t, i, n)) : null
                    }), c.join(", ")) : null
                }
        },
        424: function(e, t, a) {
            "use strict";
            a.d(t, "a", function() {
                return r
            }), a.d(t, "b", function() {
                return n
            }), a.d(t, "c", function() {
                return o
            }), a.d(t, "d", function() {
                return i
            }), a.d(t, "e", function() {
                return s
            }), a.d(t, "f", function() {
                return l
            }), a.d(t, "g", function() {
                return u
            });
            var r = function(e, t) {
                    return !(!t || !t.id) && e.ids.includes(t.id.toString())
                },
                c = function(e) {
                    return e && e.match && e.match.params
                },
                n = function(e, t) {
                    return "".concat(i(e, t), "/print")
                },
                o = function(e) {
                    var t = e.match(/..\/projects\/.+\/complete\/?$/i);
                    return Boolean(t)
                },
                i = function(e, t) {
                    return "/".concat(e, "/projects/").concat(t)
                },
                s = function(e, t) {
                    return !(!c(e) || !c(t)) && e.match.params.position !== t.match.params.position
                },
                l = function(e) {
                    return "undefined" === typeof e ? "" : "t-".concat(e)
                },
                u = function(e) {
                    return "c-project-steps__wrapper--animate-".concat(e)
                }
        },
        431: function(e, t, a) {
            "use strict";
            var r = a(16),
                c = a(1),
                n = a.n(c),
                o = a(11);
            t.a = Object(r.c)("translations")(function(e) {
                var t = e.projectRepoUrl,
                    a = e.t;
                return n.a.createElement("p", null, a("project.footer.license.published"), " ", n.a.createElement("a", {
                    className: "c-project-footer__link c-project-footer__link--rpi",
                    href: o.a.raspberryPiUrl
                }, "Raspberry Pi Foundation"), t ? n.a.createElement(n.a.Fragment, null, " ", a("project.footer.license.under"), " ", n.a.createElement("a", {
                    className: "c-project-footer__link c-project-footer__link--creative-commons",
                    href: o.a.creativeCommonsUrl
                }, "Creative Commons license"), ".", n.a.createElement("br", null), n.a.createElement("a", {
                    className: "c-project-footer__link c-project-footer__link--github",
                    href: t
                }, a("project.footer.license.link.text"))) : null)
            })
        },
        432: function(e, t, a) {
            "use strict";
            a.d(t, "b", function() {
                return n
            }), a.d(t, "a", function() {
                return o
            }), a.d(t, "c", function() {
                return i
            });
            var r = a(6),
                c = a(422),
                n = function(e, t) {
                    return Object(c.a)({
                        endpoint: "/".concat(e, "/progress"),
                        types: [r.d, r.j, r.g],
                        currentLocale: e,
                        authToken: t
                    })
                },
                o = function(e, t, a, n, o) {
                    return Object(c.a)({
                        endpoint: "/".concat(e, "/progress"),
                        types: [r.p, r.s, r.n],
                        method: "POST",
                        data: {
                            projectId: a,
                            id: n,
                            step: o
                        },
                        currentLocale: e,
                        authToken: t
                    })
                },
                i = function(e, t, a, n, o) {
                    return Object(c.a)({
                        endpoint: "/".concat(e, "/progress/").concat(n),
                        types: [r.p, r.s, r.n],
                        method: "PUT",
                        data: {
                            projectId: a,
                            id: n,
                            step: o
                        },
                        currentLocale: e,
                        authToken: t
                    })
                }
        },
        434: function(e, t, a) {
            "use strict";
            a.d(t, "b", function() {
                return n
            }), a.d(t, "a", function() {
                return o
            }), a.d(t, "c", function() {
                return i
            });
            var r = a(6),
                c = a(422),
                n = function(e, t) {
                    return Object(c.a)({
                        endpoint: "/".concat(e, "/projects").concat(t),
                        types: [r.e, r.l, r.g],
                        currentLocale: e
                    })
                },
                o = function(e) {
                    var t = e.locale,
                        a = e.slug,
                        n = e.position;
                    return Object(c.a)({
                        endpoint: "/".concat(t, "/projects/").concat(a),
                        types: [r.f, r.k, r.g],
                        currentLocale: t,
                        currentStepPosition: n,
                        slug: a
                    })
                },
                i = function(e) {
                    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null;
                    return {
                        type: r.t,
                        currentStepPosition: e,
                        authToken: t
                    }
                }
        },
        439: function(e, t, a) {
            "use strict";
            a.d(t, "b", function() {
                return n
            }), a.d(t, "a", function() {
                return o
            }), a.d(t, "c", function() {
                return i
            });
            var r = a(6),
                c = a(422),
                n = function(e, t) {
                    return Object(c.a)({
                        endpoint: "/".concat(e, "/favourites"),
                        types: [r.b, r.h, r.g],
                        mapIds: !0,
                        currentLocale: e,
                        authToken: t
                    })
                },
                o = function(e, t, a) {
                    return Object(c.a)({
                        endpoint: "/".concat(e, "/favourites"),
                        types: [r.o, r.r, r.n],
                        method: "POST",
                        data: {
                            id: a
                        },
                        currentLocale: e,
                        authToken: t
                    })
                },
                i = function(e, t, a) {
                    return Object(c.a)({
                        endpoint: "/".concat(e, "/favourites/").concat(a),
                        types: [r.m, r.q, r.n],
                        method: "DELETE",
                        data: {
                            id: a
                        },
                        currentLocale: e,
                        authToken: t
                    })
                }
        },
        442: function(e, t, a) {
            "use strict";
            var r = a(436),
                c = a.n(r),
                n = a(437),
                o = a(57),
                i = a(81),
                s = a(82),
                l = a(84),
                u = a(83),
                p = a(85),
                f = a(1),
                d = a.n(f),
                m = a(2),
                j = a.n(m),
                h = a(11),
                b = new Headers({
                    "Content-Type": "application/vnd.api+json"
                }),
                k = function(e) {
                    var t = "".concat("https://learning-admin.raspberrypi.org", "/api/v1/feedbacks");
                    return Promise.resolve(fetch(t, {
                        method: "POST",
                        headers: b,
                        body: JSON.stringify({
                            data: {
                                type: "api/v1/feedbacks",
                                attributes: e
                            }
                        })
                    }).then(function(e) {
                        if (e.status === h.a.createdStatus) return e.json();
                        var t = new Error(e.statusText);
                        throw t.response = e, t
                    }).catch(function(e) {
                        return console.error("Request failed:", e)
                    }))
                },
                v = a(16),
                g = (a(447), a(431)),
                E = function(e) {
                    function t(e) {
                        var a;
                        return Object(i.a)(this, t), (a = Object(l.a)(this, Object(u.a)(t).call(this, e))).feedbackAttributes = function() {
                            var e = {
                                message: a.feedbackText(),
                                userID: a.props.userID
                            };
                            return a.isProject() && (e.project_slug = a.props.projectSlug, e.step_number = a.formStep), e
                        }, a.feedbackFormSubmitHandler = function(e) {
                            e.preventDefault(), a.setState({
                                feedbackFormDisabled: !0
                            }, function() {
                                return a.postFeedback()
                            })
                        }, a.feedbackIntro = function() {
                            return "URL: ".concat(window.location.pathname, "\n")
                        }, a.feedbackPromptClickHandler = function() {
                            return a.setState({
                                feedbackVisibleElement: "form"
                            })
                        }, a.feedbackText = function() {
                            return a.isProject() ? "".concat(a.feedbackIntro(), "\n\n").concat(a.state.feedbackText) : a.state.feedbackText
                        }, a.isProject = function() {
                            return "string" === typeof a.props.projectSlug
                        }, a.updateFeedbackText = function(e) {
                            a.setState({
                                feedbackText: e.target.value
                            })
                        }, a.state = {
                            feedbackFormDisabled: !1,
                            feedbackText: "",
                            feedbackVisibleElement: "prompt"
                        }, a.formStep = 5, a
                    }
                    return Object(p.a)(t, e), Object(s.a)(t, [{
                        key: "feedbackClassNames",
                        value: function(e) {
                            var t;
                            return j()((t = {}, Object(o.a)(t, "c-project-footer__feedback-".concat(e), !0), Object(o.a)(t, "c-project-footer__feedback-".concat(e, "--hidden"), e !== this.state.feedbackVisibleElement), t))
                        }
                    }, {
                        key: "canSubmitFeedback",
                        value: function() {
                            return !this.state.feedbackFormDisabled && Boolean(this.state.feedbackText)
                        }
                    }, {
                        key: "postFeedback",
                        value: function() {
                            var e = Object(n.a)(c.a.mark(function e() {
                                return c.a.wrap(function(e) {
                                    for (;;) switch (e.prev = e.next) {
                                        case 0:
                                            return e.next = 2, k(this.feedbackAttributes());
                                        case 2:
                                            this.setState({
                                                feedbackVisibleElement: "thanks"
                                            });
                                        case 3:
                                        case "end":
                                            return e.stop()
                                    }
                                }, e, this)
                            }));
                            return function() {
                                return e.apply(this, arguments)
                            }
                        }()
                    }, {
                        key: "projectRepoUrl",
                        value: function() {
                            return this.props.projectSlug ? "https://github.com/RaspberryPiLearning/".concat(this.props.projectSlug) : null
                        }
                    }, {
                        key: "render",
                        value: function() {
                            var e = this.props.t;
                            return d.a.createElement("footer", {
                                className: "c-project-footer"
                            }, d.a.createElement("p", null, e(this.props.text)), d.a.createElement("button", {
                                className: this.feedbackClassNames("prompt"),
                                onClick: this.feedbackPromptClickHandler
                            }, e("project.footer.feedback.button.text"), " "), d.a.createElement("form", {
                                className: this.feedbackClassNames("form"),
                                onSubmit: this.feedbackFormSubmitHandler
                            }, d.a.createElement("textarea", {
                                className: "c-project-footer__feedback-textarea",
                                cols: "40",
                                disabled: this.state.feedbackFormDisabled,
                                onChange: this.updateFeedbackText,
                                placeholder: "".concat(e("project.footer.feedback.placeholder.text")),
                                rows: "6",
                                value: this.state.feedbackText
                            }), d.a.createElement("button", {
                                className: "c-project-footer__feedback-submit",
                                disabled: !this.canSubmitFeedback(),
                                type: "submit"
                            }, this.state.feedbackFormDisabled ? e("project.footer.feedback.button.disabled.text") : e("project.footer.feedback.button.text"))), d.a.createElement("div", {
                                className: this.feedbackClassNames("thanks")
                            }, e("project.footer.feedback.thank-you.text")), d.a.createElement(g.a, {
                                projectRepoUrl: this.projectRepoUrl()
                            }), d.a.createElement("div", {
                                className: "c-project-footer-policy"
                            }, d.a.createElement("p", null, d.a.createElement("a", {
                                className: "c-project-footer__link c-project-footer__link-option",
                                href: "https://www.raspberrypi.org/cookies/"
                            }, e("project.footer.license.cookies"))), d.a.createElement("p", null, d.a.createElement("a", {
                                className: "c-project-footer__link",
                                href: "https://www.raspberrypi.org/privacy/"
                            }, e("project.footer.license.privacy")))))
                        }
                    }]), t
                }(f.Component);
            t.a = Object(v.c)("translations")(E)
        },
        447: function(e, t, a) {},
        456: function(e, t, a) {
            "use strict";
            var r = a(1),
                c = a.n(r);
            a(457);
            t.a = function() {
                return c.a.createElement("svg", {
                    className: "c-scratchblock-svg-filters"
                }, c.a.createElement("defs", null, c.a.createElement("filter", {
                    id: "bevelFilter",
                    x0: "-50%",
                    y0: "-50%",
                    width: "200%",
                    height: "200%"
                }, c.a.createElement("feGaussianBlur", {
                    result: "blur-1",
                    in: "SourceAlpha",
                    stdDeviation: "1 1"
                }), c.a.createElement("feFlood", {
                    result: "flood-2",
                    in: "undefined",
                    floodColor: "#fff",
                    floodOpacity: "0.15"
                }), c.a.createElement("feOffset", {
                    result: "offset-3",
                    in: "blur-1",
                    dx: "1",
                    dy: "1"
                }), c.a.createElement("feComposite", {
                    result: "comp-4",
                    operator: "arithmetic",
                    in: "SourceAlpha",
                    in2: "offset-3",
                    k2: "1",
                    k3: "-1"
                }), c.a.createElement("feComposite", {
                    result: "comp-5",
                    operator: "in",
                    in: "flood-2",
                    in2: "comp-4"
                }), c.a.createElement("feFlood", {
                    result: "flood-6",
                    in: "undefined",
                    floodColor: "#000",
                    floodOpacity: "0.7"
                }), c.a.createElement("feOffset", {
                    result: "offset-7",
                    in: "blur-1",
                    dx: "-1",
                    dy: "-1"
                }), c.a.createElement("feComposite", {
                    result: "comp-8",
                    operator: "arithmetic",
                    in: "SourceAlpha",
                    in2: "offset-7",
                    k2: "1",
                    k3: "-1"
                }), c.a.createElement("feComposite", {
                    result: "comp-9",
                    operator: "in",
                    in: "flood-6",
                    in2: "comp-8"
                }), c.a.createElement("feMerge", {
                    result: "merge-10"
                }, c.a.createElement("feMergeNode", { in: "SourceGraphic"
                }), c.a.createElement("feMergeNode", { in: "comp-5"
                }), c.a.createElement("feMergeNode", { in: "comp-9"
                }))), c.a.createElement("filter", {
                    id: "inputBevelFilter",
                    x0: "-50%",
                    y0: "-50%",
                    width: "200%",
                    height: "200%"
                }, c.a.createElement("feGaussianBlur", {
                    result: "blur-1",
                    in: "SourceAlpha",
                    stdDeviation: "1 1"
                }), c.a.createElement("feFlood", {
                    result: "flood-2",
                    in: "undefined",
                    floodColor: "#fff",
                    floodOpacity: "0.15"
                }), c.a.createElement("feOffset", {
                    result: "offset-3",
                    in: "blur-1",
                    dx: "-1",
                    dy: "-1"
                }), c.a.createElement("feComposite", {
                    result: "comp-4",
                    operator: "arithmetic",
                    in: "SourceAlpha",
                    in2: "offset-3",
                    k2: "1",
                    k3: "-1"
                }), c.a.createElement("feComposite", {
                    result: "comp-5",
                    operator: "in",
                    in: "flood-2",
                    in2: "comp-4"
                }), c.a.createElement("feFlood", {
                    result: "flood-6",
                    in: "undefined",
                    floodColor: "#000",
                    floodOpacity: "0.7"
                }), c.a.createElement("feOffset", {
                    result: "offset-7",
                    in: "blur-1",
                    dx: "1",
                    dy: "1"
                }), c.a.createElement("feComposite", {
                    result: "comp-8",
                    operator: "arithmetic",
                    in: "SourceAlpha",
                    in2: "offset-7",
                    k2: "1",
                    k3: "-1"
                }), c.a.createElement("feComposite", {
                    result: "comp-9",
                    operator: "in",
                    in: "flood-6",
                    in2: "comp-8"
                }), c.a.createElement("feMerge", {
                    result: "merge-10"
                }, c.a.createElement("feMergeNode", { in: "SourceGraphic"
                }), c.a.createElement("feMergeNode", { in: "comp-5"
                }), c.a.createElement("feMergeNode", { in: "comp-9"
                }))), c.a.createElement("filter", {
                    id: "inputDarkFilter",
                    x0: "-50%",
                    y0: "-50%",
                    width: "200%",
                    height: "200%"
                }, c.a.createElement("feFlood", {
                    result: "flood-1",
                    in: "undefined",
                    floodColor: "#000",
                    floodOpacity: "0.2"
                }), c.a.createElement("feComposite", {
                    result: "comp-2",
                    operator: "in",
                    in: "flood-1",
                    in2: "SourceAlpha"
                }), c.a.createElement("feMerge", {
                    result: "merge-3"
                }, c.a.createElement("feMergeNode", { in: "SourceGraphic"
                }), c.a.createElement("feMergeNode", { in: "comp-2"
                }))), c.a.createElement("path", {
                    d: "M1.504 21L0 19.493 4.567 0h1.948l-.5 2.418s1.002-.502 3.006 0c2.006.503 3.008 2.01 6.517 2.01 3.508 0 4.463-.545 4.463-.545l-.823 9.892s-2.137 1.005-5.144.696c-3.007-.307-3.007-2.007-6.014-2.51-3.008-.502-4.512.503-4.512.503L1.504 21z",
                    fill: "#3f8d15",
                    id: "greenFlag"
                }), c.a.createElement("path", {
                    d: "M6.724 0C3.01 0 0 2.91 0 6.5c0 2.316 1.253 4.35 3.14 5.5H5.17v-1.256C3.364 10.126 2.07 8.46 2.07 6.5 2.07 4.015 4.152 2 6.723 2c1.14 0 2.184.396 2.993 1.053L8.31 4.13c-.45.344-.398.826.11 1.08L15 8.5 13.858.992c-.083-.547-.514-.714-.963-.37l-1.532 1.172A6.825 6.825 0 0 0 6.723 0z",
                    fill: "#fff",
                    id: "turnRight"
                }), c.a.createElement("path", {
                    d: "M3.637 1.794A6.825 6.825 0 0 1 8.277 0C11.99 0 15 2.91 15 6.5c0 2.316-1.253 4.35-3.14 5.5H9.83v-1.256c1.808-.618 3.103-2.285 3.103-4.244 0-2.485-2.083-4.5-4.654-4.5-1.14 0-2.184.396-2.993 1.053L6.69 4.13c.45.344.398.826-.11 1.08L0 8.5 1.142.992c.083-.547.514-.714.963-.37l1.532 1.172z",
                    fill: "#fff",
                    id: "turnLeft"
                }), c.a.createElement("g", {
                    id: "loopArrow"
                }, c.a.createElement("path", {
                    d: "M8 0l2 -2l0 -3l3 0l-4 -5l-4 5l3 0l0 3l-8 0l0 2",
                    fill: "#000",
                    opacity: "0.3"
                }), c.a.createElement("path", {
                    d: "M8 0l2 -2l0 -3l3 0l-4 -5l-4 5l3 0l0 3l-8 0l0 2",
                    fill: "#fff",
                    opacity: "0.9",
                    transform: "translate(-1 -1)"
                }))))
            }
        },
        457: function(e, t, a) {},
        471: function(e, t, a) {},
        527: function(e, t, a) {},
        528: function(e, t, a) {},
        529: function(e, t, a) {},
        530: function(e, t, a) {},
        531: function(e, t, a) {},
        533: function(e, t, a) {},
        535: function(e, t, a) {},
        536: function(e, t, a) {}
    }
]);
//# sourceMappingURL=ProjectContainer.27781258.chunk.js.map