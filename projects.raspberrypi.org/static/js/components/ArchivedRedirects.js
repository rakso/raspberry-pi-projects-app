import React from 'react'
import {
    Route
} from 'react-router-dom'

export const archivedProjects = [
    'microbit-game-controller',
    'n-days-of-christmas',
    'sweet-shop-reaction-game',
]

const loadArchivedRoutes = () => {
    return archivedProjects.map((project, idx) => {
        return ( <
            Route exact path = {
                `/:locale/projects/${project}`
            }
            key = {
                idx
            }
            component = {
                () =>
                (window.location = `https://github.com/raspberrypilearning/${project}`)
            }
            />
        )
    })
}

export default loadArchivedRoutes