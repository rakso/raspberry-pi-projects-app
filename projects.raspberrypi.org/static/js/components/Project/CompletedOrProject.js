import PropTypes from 'prop-types'
import React from 'react'

import CompletedProject from './CompletedProject/CompletedProject'
import Project from './Project/Project'
import ProjectHeaderContainer from 'components/Project/shared/ProjectHeader/ProjectHeaderContainer'
import HtmlHead from 'components/shared/HtmlHead/HtmlHead'
import {
    themeClassName
} from 'helpers/Project'

const CompletedOrProject = ({
    locale,
    project,
    projectComplete,
    projectSlug,
}) => ( <
    div className = {
        `c-project ${themeClassName(project.theme)}`
    } >
    <
    header className = "c-project__header" >
    <
    ProjectHeaderContainer projectComplete = {
        projectComplete
    }
    /> <
    /header> {
        projectComplete ? ( <
            React.Fragment >
            <
            HtmlHead titlePrefix = {
                `${project.title} - Complete`
            }
            /> <
            CompletedProject locale = {
                locale
            }
            projectSlug = {
                projectSlug
            }
            /> <
            /React.Fragment>
        ) : ( <
            Project project = {
                project
            }
            />
        )
    } <
    /div>
)

CompletedOrProject.propTypes = {
    locale: PropTypes.string.isRequired,
    project: PropTypes.object.isRequired,
    projectComplete: PropTypes.bool.isRequired,
    projectSlug: PropTypes.string.isRequired,
}

export default CompletedOrProject