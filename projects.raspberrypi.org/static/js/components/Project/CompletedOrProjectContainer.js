import PropTypes from 'prop-types'
import React from 'react'
import {
    connect
} from 'react-redux'

import CompletedOrProject from './CompletedOrProject'

const CompletedOrProjectContainer = ({
    currentLocale,
    project,
    projectComplete,
    projectSlug,
}) => {
    return ( <
        CompletedOrProject locale = {
            currentLocale
        }
        project = {
            project
        }
        projectComplete = {
            projectComplete
        }
        projectSlug = {
            projectSlug
        }
        />
    )
}

CompletedOrProjectContainer.propTypes = {
    currentLocale: PropTypes.string.isRequired,
    project: PropTypes.object.isRequired,
    projectComplete: PropTypes.bool.isRequired,
    projectSlug: PropTypes.string.isRequired,
}

const mapStateToProps = state => {
    const {
        project,
        currentLocale
    } = state.project
    return {
        currentLocale,
        project,
    }
}

export default connect(mapStateToProps)(CompletedOrProjectContainer)