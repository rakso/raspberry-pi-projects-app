import PropTypes from 'prop-types'
import React, {
    Component
} from 'react'
import uuid from 'uuid'
import {
    scroller
} from 'react-scroll'

import config from 'helpers/config'
import FooterContainer from 'components/shared/Footer/FooterContainer'
import CompletionCta from 'components/Project/CompletedProject/CompletionCta/CompletionCta'

import './CompletedProject.css'

class CompletedProject extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userID: uuid.v1(),
        }
    }

    componentDidMount() {
        scroller.scrollTo('c-site-header', {
            duration: config.stepScrollDuration,
            smooth: 'easeOutQuad',
        })
    }

    render() {
        return ( <
            React.Fragment >
            <
            main className = "c-project__layout c-project__layout--project-complete u-clearfix"
            id = "c-project__layout" >
            <
            section className = "c-project__content c-project__content--project-complete" >
            <
            div className = "c-project-steps c-project-steps--project-complete" >
            <
            CompletionCta locale = {
                this.props.locale
            }
            projectSlug = {
                this.props.projectSlug
            }
            /> <
            /div> <
            /section> <
            /main>

            <
            FooterContainer text = "project.footer.feedback.text"
            userID = {
                this.state.userID
            }
            /> <
            /React.Fragment>
        )
    }
}

CompletedProject.propTypes = {
    locale: PropTypes.string.isRequired,
    projectSlug: PropTypes.string.isRequired,
}

export default CompletedProject