import PropTypes from 'prop-types'
import React, {
    Component
} from 'react'
import {
    translate
} from 'react-i18next'
import {
    Link
} from 'react-router-dom'
import Feedback from 'components/Project/Feedback/Feedback'

import './CompletionCta.css'

export class CompletionCta extends Component {
    render() {
        const {
            locale,
            projectSlug,
            t
        } = this.props
        return ( <
            div className = "c-project-completion-cta" >
            <
            Feedback locale = {
                locale
            }
            projectSlug = {
                projectSlug
            }
            /> <
            h1 className = "c-project-completion-cta__heading" > {
                t('project.completion-cta.find-another-project')
            } <
            /h1>

            <
            Link className = "c-project-completion-cta__link  c-button u-m-x4"
            to = {
                `/${locale}/projects`
            } >
            {
                t('project.completion-cta.browse-all-projects')
            } <
            /Link> <
            /div>
        )
    }
}

CompletionCta.propTypes = {
    locale: PropTypes.string.isRequired,
    projectSlug: PropTypes.string.isRequired,
    t: PropTypes.func.isRequired,
}

export default translate('translations')(CompletionCta)