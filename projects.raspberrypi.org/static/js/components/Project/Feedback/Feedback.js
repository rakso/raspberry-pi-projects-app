import PropTypes from 'prop-types'
import React, {
    Component
} from 'react'
import {
    translate
} from 'react-i18next'
import {
    Link
} from 'react-router-dom'
import classNames from 'classnames'

import './Feedback.css'

export class Feedback extends Component {
    constructor(props) {
        super(props)
        this.state = {
            feedbackVisibleElement: 'prompt',
        }
    }

    feedbackClickHandler = () =>
        this.setState({
            feedbackVisibleElement: 'thanks'
        })

    feedbackClassNames(element) {
        return classNames({
            [`c-project-feedback__${element}`]: true,
            [`c-project-feedback__${element}--hidden`]: element !== this.state.feedbackVisibleElement,
        })
    }
    render() {
        const {
            locale,
            projectSlug,
            t
        } = this.props
        return ( <
            div className = "c-project-feedback" >
            <
            h3 className = "c-project-feedback__title" > {
                t('project.completion-cta.feedback')
            } <
            /h3>

            <
            div className = {
                this.feedbackClassNames('thanks')
            } > {
                t('project.footer.feedback.thank-you.text')
            } <
            /div>

            <
            div className = {
                `c-project-feedback__options js-project-feedback__options u-clearfix ${this.feedbackClassNames(
            'prompt',
          )}`
            } >
            <
            div className = "c-project-feedback__option" >
            <
            Link onClick = {
                this.feedbackClickHandler
            }
            data - on = "click"
            data - event - category = {
                projectSlug
            }
            data - event - action = "like"
            data - event - label = {
                locale
            }
            to = "#" >
            <
            div className = "c-project-feedback__option-image c-project-feedback__option-image--0" / > {
                t('project.feedback.like')
            } <
            /Link> <
            /div>

            <
            div className = "c-project-feedback__option" >
            <
            Link onClick = {
                this.feedbackClickHandler
            }
            data - on = "click"
            data - event - category = {
                projectSlug
            }
            data - event - action = "ok"
            data - event - label = {
                locale
            }
            to = "#" >
            <
            div className = "c-project-feedback__option-image c-project-feedback__option-image--1" / > {
                t('project.feedback.ok')
            } <
            /Link> <
            /div>

            <
            div className = "c-project-feedback__option" >
            <
            Link onClick = {
                this.feedbackClickHandler
            }
            data - on = "click"
            data - event - category = {
                projectSlug
            }
            data - event - action = "dislike"
            data - event - label = {
                locale
            }
            to = "#" >
            <
            div className = "c-project-feedback__option-image c-project-feedback__option-image--2" / > {
                t('project.feedback.dislike')
            } <
            /Link> <
            /div> <
            /div> <
            /div>
        )
    }
}

Feedback.propTypes = {
    locale: PropTypes.string.isRequired,
    projectSlug: PropTypes.string.isRequired,
    t: PropTypes.func.isRequired,
}

export default translate('translations')(Feedback)