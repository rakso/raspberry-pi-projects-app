import PropTypes from 'prop-types'
import React from 'react'
import {
    Link
} from 'react-router-dom'

import './Nav.css'

const navItemClassNames = (currentStepPosition, step, numSteps) => {
    let classBase = 'c-project-navigation__link'
    let classes = [classBase]

    if (step.position === 0) {
        classes.push(`${classBase}--is-first`)
    }

    if (currentStepPosition - 1 === step.position) {
        classes.push(`${classBase}--is-current`)
    }

    if (step.position < currentStepPosition - 1) {
        classes.push(`${classBase}--is-done`)
    }

    if (step.position === numSteps - 1) {
        classes.push(`${classBase}--is-last`)
    }

    return classes.join(' ')
}

const linkToPosition = position => (position === 0 ? '' : `/${position + 1}`)

const Nav = ({
    steps,
    currentLocale,
    currentStepPosition,
    slug,
    projectTitle,
}) => ( <
    menu className = "c-project__menu" >
    <
    div className = "c-project-navigation" >
    <
    input className = "c-project-navigation__toggle-checkbox u-hidden"
    id = "c-project-navigation__toggle-checkbox"
    type = "checkbox" /
    >

    <
    label className = "c-project-navigation__toggle-label"
    htmlFor = "c-project-navigation__toggle-checkbox" >
    Contents <
    span className = "c-project-navigation__toggle-icon" / >
    <
    /label>

    <
    ul className = "c-project-navigation__list" > {
        steps.map((step, index) => {
            return ( <
                li className = "c-project-navigation__item"
                key = {
                    index
                } >
                <
                Link className = {
                    navItemClassNames(
                        currentStepPosition,
                        step,
                        steps.length,
                    )
                }
                to = {
                    `/${currentLocale}/projects/${slug}${linkToPosition(
                  step.position,
                )}`
                }
                data - on = "click"
                data - event - category = {
                    projectTitle
                }
                data - event - action = "Clicked Navigation Link"
                role = "link"
                tabIndex = {
                    0
                } >
                {
                    step.title
                } <
                /Link> <
                /li>
            )
        })
    } <
    /ul> <
    /div> <
    /menu>
)

Nav.propTypes = {
    steps: PropTypes.array.isRequired,
    currentLocale: PropTypes.string.isRequired,
    currentStepPosition: PropTypes.number.isRequired,
    projectTitle: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
}

export default Nav