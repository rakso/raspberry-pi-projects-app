import PropTypes from 'prop-types'
import React from 'react'
import {
    connect
} from 'react-redux'

import Nav from './Nav'

const NavContainer = ({
    currentLocale,
    currentStepPosition,
    project,
    slug,
}) => {
    return ( <
        Nav steps = {
            project.steps
        }
        currentLocale = {
            currentLocale
        }
        currentStepPosition = {
            currentStepPosition
        }
        slug = {
            slug
        }
        projectTitle = {
            project.title
        }
        />
    )
}

NavContainer.propTypes = {
    project: PropTypes.object.isRequired,
    currentLocale: PropTypes.string.isRequired,
    currentStepPosition: PropTypes.number.isRequired,
    slug: PropTypes.string.isRequired,
}

const mapStateToProps = state => {
    const {
        project,
        currentLocale,
        currentStepPosition,
        slug
    } = state.project
    return {
        project,
        currentLocale,
        currentStepPosition,
        slug,
    }
}

export default connect(mapStateToProps)(NavContainer)