import PropTypes from 'prop-types'
import React, {
    Component
} from 'react'
import uuid from 'uuid'

import FooterContainer from 'components/shared/Footer/FooterContainer'
import NavContainer from 'components/Project/Project/Nav/NavContainer'
import StepContainer from 'components/Project/Project/Step/StepContainer'
import Loader from 'components/shared/Loader/Loader'

import './Project.css'

class Project extends Component {
    constructor(props) {
        super(props)
        this.userID = uuid.v1()
    }

    stepsOrLoader() {
        const {
            project
        } = this.props
        if (project.steps) {
            return ( <
                div >
                <
                NavContainer / >
                <
                StepContainer / >
                <
                /div>
            )
        }
        return <Loader delay = {
            200
        }
        />
    }

    render() {
        return ( <
            React.Fragment >
            <
            main className = "c-project__layout u-clearfix"
            id = "c-project__layout" > {
                this.stepsOrLoader()
            } <
            /main> <
            FooterContainer text = "project.footer.feedback.text"
            userID = {
                this.userID
            }
            /> <
            /React.Fragment>
        )
    }
}

Project.propTypes = {
    project: PropTypes.object.isRequired,
}

export default Project