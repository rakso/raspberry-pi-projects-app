import './Content.css'
import 'swiper/dist/css/swiper.min.css'

import {
    CSSTransition,
    TransitionGroup
} from 'react-transition-group'
import React, {
    Component
} from 'react'

import NavigationContainer from 'components/Project/Project/Step/Navigation/NavigationContainer'
import PropTypes from 'prop-types'
import ScratchBlockSvgFilters from 'components/Project/Project/ScratchBlockSvgFilters/ScratchBlockSvgFilters'
import {
    Wysiwyg
} from 'raspberry-pi-bits'
import classnames from 'classnames'
import config from 'helpers/config'
import {
    isChallenge
} from 'helpers/Step'
import {
    transitionName
} from 'helpers/Project'
import {
    translate
} from 'react-i18next'
import localForage from 'localforage'

export class Content extends Component {
    async componentDidMount() {
        await this.loadTasksProgress()
        this.updateTasksCheckboxes()
    }

    async componentDidUpdate() {
        await this.loadTasksProgress()
        this.updateTasksCheckboxes()
    }

    addCheckboxEventListener = checkbox => {
        checkbox.addEventListener('change', event => {
            const checked = event.target.checked
            const taskIndex = parseInt(checkbox.dataset.taskIndex, 10)

            this.updateTasksProgress(taskIndex, checked)
        })

        checkbox.dataset.onChangeEventListenerAdded = true
    }

    childFactoryCreator = slideDirection => child =>
        React.cloneElement(child, {
            classNames: transitionName(slideDirection),
        })

    cssTransitionConfig = () => ({
        enter: config.transitionEnterTimeout,
        exit: config.transitionExitTimeout,
    })

    loadTasksProgress = async () => {
        if (this.tasksCount() === 0) {
            return null
        }
        let progress = await localForage.getItem(this.tasksProgressKey())

        if (progress) {
            return JSON.parse(progress)
        }

        return this.initTasksProgress()
    }

    tasksCount = () => {
        let content = this.props.step.content
        return (content.match(/"(c-project-task)+[ "]/g) || []).length
    }

    initTasksProgress = () => {
        let progress = new Array(this.tasksCount())
        progress.fill(false)

        this.saveTasksProgress(progress)

        return progress
    }

    tasksProgressKey = () => {
        const {
            slug,
            step
        } = this.props
        return `tasks-progress-${slug}-step-${step.position}`
    }

    saveTasksProgress = progress => {
        localForage.setItem(this.tasksProgressKey(), JSON.stringify(progress))
    }

    updateTasksProgress = async (index, value) => {
        let progress = await this.loadTasksProgress()
        progress[index] = value
        this.saveTasksProgress(progress)
    }

    updateTasksCheckboxes = async () => {
        if (this.tasksCount() === 0) {
            return
        }
        const progress = await this.loadTasksProgress()

        let checkboxes = document.querySelectorAll('.c-project-task__checkbox')
        let checkbox

        for (let index = 0; index < checkboxes.length; index += 1) {
            checkbox = checkboxes[index]
            checkbox.dataset.taskIndex = index

            if (progress[index]) {
                checkbox.checked = true
            }

            this.addCheckboxEventListener(checkbox)
        }
    }

    renderContent = step => {
        const stepIsChallenge = isChallenge(step)
        const classNames = classnames('c-project-steps__wrapper', {
            'c-project-steps__wrapper--challenge': stepIsChallenge,
        })
        const challengeLabel = stepIsChallenge ? ( <
            span className = "c-project-steps__challenge-label" > {
                this.props.t('project.steps.challenge')
            } <
            /span>
        ) : null

        return ( <
            div className = {
                classNames
            } > {
                challengeLabel
            } <
            Wysiwyg >
            <
            div className = "c-project-steps__content"
            dangerouslySetInnerHTML = {
                {
                    __html: step.content,
                }
            }
            /> <
            /Wysiwyg> <
            NavigationContainer / >
            <
            /div>
        )
    }

    render() {
        return ( <
            section className = "c-project__content" > {
                this.props.isScratchProject ? < ScratchBlockSvgFilters / > : ''
            } <
            div className = "c-project-steps" >
            <
            TransitionGroup childFactory = {
                this.childFactoryCreator(this.props.slideDirection)
            } >
            <
            CSSTransition key = {
                this.props.step.position
            }
            classNames = {
                transitionName(this.props.slideDirection)
            }
            timeout = {
                this.cssTransitionConfig()
            } >
            {
                this.renderContent(this.props.step)
            } <
            /CSSTransition> <
            /TransitionGroup> <
            /div> <
            /section>
        )
    }
}

Content.propTypes = {
    isScratchProject: PropTypes.bool.isRequired,
    slideDirection: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
    step: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired,
}

export default translate('translations')(Content)