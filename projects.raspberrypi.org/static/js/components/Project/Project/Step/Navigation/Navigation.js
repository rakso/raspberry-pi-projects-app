import PropTypes from 'prop-types'
import React from 'react'
import {
    Link
} from 'react-router-dom'
import classnames from 'classnames'

import './Navigation.css'

const Navigation = ({
    firstStep,
    nextLinkPath,
    nextStepTitle,
    previousLinkPath,
    projectTitle,
}) => ( <
    nav className = "c-project-step-navigation" >
    <
    Link className = {
        classnames({
            'c-button': true,
            'c-button--secondary': true,
            'c-project-step-navigation__link': true,
            'c-project-step-navigation__link--previous': true,
            'u-hidden': firstStep,
        })
    }
    to = {
        previousLinkPath
    }
    data - on = "click"
    data - event - category = {
        projectTitle
    }
    data - event - action = "Clicked Step Navigation" /
    >

    <
    Link className = "c-button c-project-step-navigation__link--next"
    to = {
        nextLinkPath
    }
    data - on = "click"
    data - event - category = {
        projectTitle
    }
    data - event - action = "Clicked Step Navigation" >
    {
        nextStepTitle
    } <
    /Link> <
    /nav>
)

Navigation.propTypes = {
    firstStep: PropTypes.bool.isRequired,
    nextLinkPath: PropTypes.string.isRequired,
    nextStepTitle: PropTypes.string.isRequired,
    previousLinkPath: PropTypes.string.isRequired,
    projectTitle: PropTypes.string.isRequired,
}

export default Navigation