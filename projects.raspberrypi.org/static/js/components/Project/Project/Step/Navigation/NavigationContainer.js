import PropTypes from 'prop-types'
import React from 'react'
import {
    connect
} from 'react-redux'

import Navigation from './Navigation'

import {
    projectPath
} from 'helpers/Project'

const nextLinkPath = (locale, position, project, slug) =>
    `${projectPath(locale, slug)}${nextLinkPosition(position, project)}`

const nextLinkPosition = (position, project) =>
    project.steps.length === position ? '/complete' : `/${position + 1}`

const previousLinkPath = (locale, position, slug) =>
    `${projectPath(locale, slug)}${previousLinkPosition(position)}`

const previousLinkPosition = position => {
    const secondStepPosition = 2
    return position === secondStepPosition ? '' : `/${position - 1}`
}

export const NavigationContainer = ({
    currentLocale,
    currentStepPosition,
    nextStepTitle,
    project,
    slug,
}) => ( <
    Navigation firstStep = {
        currentStepPosition === 1
    }
    nextLinkPath = {
        nextLinkPath(
            currentLocale,
            currentStepPosition,
            project,
            slug,
        )
    }
    nextStepTitle = {
        nextStepTitle
    }
    previousLinkPath = {
        previousLinkPath(
            currentLocale,
            currentStepPosition,
            slug,
        )
    }
    projectTitle = {
        project.title
    }
    />
)

NavigationContainer.propTypes = {
    currentLocale: PropTypes.string.isRequired,
    currentStepPosition: PropTypes.number.isRequired,
    nextStepTitle: PropTypes.string.isRequired,
    project: PropTypes.object.isRequired,
    slug: PropTypes.string.isRequired,
}

const mapStateToProps = state => {
    const {
        currentLocale,
        currentStepPosition,
        nextStepTitle,
        project,
        slug,
    } = state.project
    return {
        currentLocale,
        currentStepPosition,
        nextStepTitle,
        project,
        slug,
    }
}

export default connect(mapStateToProps)(NavigationContainer)