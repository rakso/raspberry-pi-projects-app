import React, {
    Component
} from 'react'

import Content from './Content/Content'
import HtmlHead from 'components/shared/HtmlHead/HtmlHead'
import PropTypes from 'prop-types'
import config from 'helpers/config'
import {
    connect
} from 'react-redux'
import {
    scroller
} from 'react-scroll'
import {
    stepInit
} from 'helpers/Step'
import {
    Redirect
} from 'react-router-dom'

export class StepContainer extends Component {
    componentDidMount() {
        setTimeout(() => {
            // This is a workaround, if the locale has no corresponding folder in /public/locales
            // then the dom isn't loaded at this point and the init fails.
            // this waits until the dom is rendered before trying to initialize events
            stepInit()
        }, 0)

        this.trackLastStep()
    }

    componentDidUpdate() {
        stepInit()
        this.trackLastStep()
        this.scrollToHeader()
    }

    isScratchProject = () => this.props.project.software.indexOf('scratch') !== -1

    scrollToHeader() {
        scroller.scrollTo('c-project__layout', {
            duration: config.stepScrollDuration,
            smooth: 'easeOutQuad',
        })
    }

    trackLastStep() {
        if (this.props.currentStepPosition !== this.props.project.steps.length) {
            return
        }

        window.ga('send', {
            hitType: 'event',
            eventCategory: this.props.project.title,
            eventAction: 'Reached final step',
        })
    }

    render() {
        const {
            currentStepPosition,
            project,
            slideDirection,
            slug,
            currentLocale,
        } = this.props
        const firstStepLocation = `/${currentLocale}/projects/${slug}/1`
        const matchFirstStepLocation =
            window.location.href.indexOf(firstStepLocation) !== -1
        let step = project.steps[currentStepPosition - 1]
        if (!step && matchFirstStepLocation) {
            // If on first step, just use that
            step = project.steps[0]
        }
        if (!step) {
            // Most likely URL tampering
            return <Redirect to = {
                firstStepLocation
            }
            />
        }
        return ( <
            React.Fragment >
            <
            HtmlHead titlePrefix = {
                `${project.title} - ${step.title}`
            }
            /> <
            Content isScratchProject = {
                this.isScratchProject()
            }
            slideDirection = {
                slideDirection
            }
            slug = {
                slug
            }
            step = {
                step
            }
            /> <
            /React.Fragment>
        )
    }
}

StepContainer.propTypes = {
    currentStepPosition: PropTypes.number.isRequired,
    project: PropTypes.object.isRequired,
    slideDirection: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
    currentLocale: PropTypes.string.isRequired,
}

const mapStateToProps = state => {
    const {
        currentStepPosition,
        project,
        slideDirection,
        slug,
        currentLocale,
    } = state.project
    return {
        currentStepPosition,
        project,
        slideDirection,
        slug,
        currentLocale,
    }
}

export default connect(mapStateToProps)(StepContainer)