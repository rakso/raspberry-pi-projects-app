import PropTypes from 'prop-types'
import 'components/Project/shared/ProjectHeader/ProjectHeader.css'
import './ProjectNotTranslated.css'

import React from 'react'

import LanguageSelector from 'components/shared/SiteHeader/LanguageSelector/LanguageSelector.js'
import {
    themeClassName
} from 'helpers/Project'
import {
    translate
} from 'react-i18next'

import HtmlHead from 'components/shared/HtmlHead/HtmlHead'

const languageDropdownFilter = (locales, project) => {
    let options = {}
    project.availableLocales.forEach(x => (options[x] = locales[x]))
    return options
}

const ProjectNotTranslated = ({
    project,
    t
}) => ( <
    div >
    <
    HtmlHead titlePrefix = {
        `${project.title}`
    }
    /> <
    div className = {
        `c-project ${themeClassName(project.theme)}`
    } >
    <
    header className = "c-project__header" >
    <
    div className = "c-project-header" >
    <
    div className = "c-project-header__info" >
    <
    h1 className = "c-project-header__title o-type-display-large" > {
        project.title
    } <
    /h1> <
    /div>

    <
    figure className = "c-project-header__illustration" >
    <
    div className = "c-project-header__decoration" / >
    <
    img alt = ""
    className = "c-project-header__image"
    src = {
        project.heroImage
    }
    /> <
    /figure> <
    /div> <
    /header> <
    div className = "o-container u-mv-x3" >
    <
    div className = "c-project-steps c-project-steps__not-translated" >
    <
    div className = "c-project-steps__wrapper" >
    <
    h2 > {
        t('project.not-translated.not-available')
    }— {
        ' '
    } <
    a href = "https://www.raspberrypi.org/translate/" > {
        t('project.not-translated.help-us')
    }!
    <
    /a> <
    /h2> <
    div className = "u-mv-x3" > {
        t('project.not-translated.alternatively')
    }!
    <
    /div> <
    div className = "u-pb-x6" >
    <
    LanguageSelector filter = {
        locales => {
            return languageDropdownFilter(locales, project)
        }
    }
    /> <
    /div> <
    /div> <
    /div> <
    /div> <
    /div> <
    /div>
)

ProjectNotTranslated.propTypes = {
    project: PropTypes.object.isRequired,
    t: PropTypes.func.isRequired,
}

export default translate('translations')(ProjectNotTranslated)