import './ProjectHeader.css'

import {
    Button
} from 'raspberry-pi-bits'

import {
    Link
} from 'react-router-dom'
import PropTypes from 'prop-types'
import React from 'react'
import {
    tagsLinks
} from 'helpers/tags'
import {
    translate
} from 'react-i18next'

import {
    addFavourite,
    removeFavourite
} from 'redux/actions/FavouriteActions'

const linksList = links =>
    Object.entries(links).map(([key, link]) => ( <
        Link className = "c-project-header__link"
        key = {
            key
        }
        to = {
            link.url
        } > {
            link.label
        } <
        /Link>
    ))

const projectCompletedNotice = t => ( <
    h2 className = "c-project-header__project-complete-notice" >
    <
    span className = "c-project-header__project-complete-icon" / > {
        t('project.header.congratulations')
    } <
    /h2>
)

const dispatchFavourite = (
    dispatch,
    favourite,
    authentication,
    locale,
    projectId,
) => {
    const authToken =
        authentication && authentication.user ?
        authentication.user.access_token :
        null
    if (authToken) {
        if (favourite) {
            dispatch(removeFavourite(locale, authToken, projectId))
        } else {
            dispatch(addFavourite(locale, authToken, projectId))
        }
    }
}

export const ProjectHeader = ({
    authentication,
    dispatch,
    favourite = false,
    hardware,
    imageUrl,
    locale,
    printUrl,
    projectComplete,
    projectId,
    software,
    t,
    title,
}) => {
    const hardwareLinks = tagsLinks(locale, t, hardware, 'hardware')
    const softwareLinks = tagsLinks(locale, t, software, 'software')
    const toggleFavourite = () =>
        dispatchFavourite(dispatch, favourite, authentication, locale, projectId)

    return ( <
        div className = {
            `c-project-header ${
        projectComplete ? 'c-project-header--project-complete' : ''
      }`
        } >
        <
        div className = "c-project-header__info" >
        <
        h1 className = "c-project-header__title o-type-display-large" > {
            title
        } <
        /h1> <
        small > {
            linksList(hardwareLinks)
        } {
            linksList(softwareLinks)
        } <
        Link className = "c-project-header__link c-project-header__link--print"
        to = {
            printUrl
        } >
        <
        span className = "u-hidden" > {
            t('project.header.print')
        } < /span> <
        /Link> {
            authentication.user && ( <
                Button className = {
                    `c-project-header__link ${
                favourite ? 'c-project-header__link--favourite' : ''
              }`
                }
                onClick = {
                    toggleFavourite
                } >
                <
                span > {
                    favourite ?
                    '✓ ' + t('project.header.favourite.added') :
                        '＋ ' + t('project.header.favourite.addto')
                } <
                /span> <
                /Button>
            )
        } <
        /small> <
        /div>

        <
        figure className = "c-project-header__illustration" >
        <
        div className = "c-project-header__decoration" / >
        <
        img alt = ""
        className = {
            `c-project-header__image`
        }
        src = {
            imageUrl
        }
        /> <
        /figure>

        {
            projectComplete ? projectCompletedNotice(t) : ''
        } <
        /div>
    )
}

ProjectHeader.propTypes = {
    authentication: PropTypes.object,
    dispatch: PropTypes.func.isRequired,
    favourite: PropTypes.bool,
    hardware: PropTypes.array,
    imageUrl: PropTypes.string,
    locale: PropTypes.string.isRequired,
    printUrl: PropTypes.string.isRequired,
    projectComplete: PropTypes.bool.isRequired,
    projectId: PropTypes.number,
    software: PropTypes.array,
    t: PropTypes.func.isRequired,
    title: PropTypes.string,
}

export default translate('translations')(ProjectHeader)