import ProjectHeader from './ProjectHeader'
import PropTypes from 'prop-types'
import React from 'react'
import {
    connect
} from 'react-redux'
import {
    isFavourite,
    printUrl
} from 'helpers/Project'

const ProjectHeaderContainer = ({
    authentication,
    currentLocale,
    dispatch,
    favourite,
    project,
    projectComplete,
    slug,
}) => ( <
    ProjectHeader authentication = {
        authentication
    }
    dispatch = {
        dispatch
    }
    favourite = {
        favourite
    }
    hardware = {
        project.hardware
    }
    imageUrl = {
        project.heroImage
    }
    locale = {
        currentLocale
    }
    printUrl = {
        printUrl(currentLocale, slug)
    }
    projectComplete = {
        projectComplete
    }
    projectId = {
        project.id
    }
    software = {
        project.software
    }
    title = {
        project.title
    }
    />
)

ProjectHeaderContainer.propTypes = {
    authentication: PropTypes.object,
    currentLocale: PropTypes.string.isRequired,
    dispatch: PropTypes.func.isRequired,
    favourite: PropTypes.bool.isRequired,
    project: PropTypes.object.isRequired,
    projectComplete: PropTypes.bool.isRequired,
    slug: PropTypes.string.isRequired,
}

const mapStateToProps = state => {
    const {
        project,
        currentLocale,
        slug
    } = state.project
    const {
        authentication,
        dispatch
    } = state
    return {
        authentication,
        currentLocale,
        dispatch,
        favourite: isFavourite(state.favourites, state.project.project),
        project,
        slug,
    }
}

export default connect(mapStateToProps)(ProjectHeaderContainer)