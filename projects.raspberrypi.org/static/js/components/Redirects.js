import React from 'react'
import {
    Route
} from 'react-router-dom'

export const redirectedProjects = [
    // {
    //   from: '/en/projects/astro-pi-mission-zero',
    //   to: 'https://astro-pi.org/',
    // },
]

const loadRedirectedRoutes = () => {
    return redirectedProjects.map(redirect => {
        return ( <
            Route exact path = {
                redirect.from
            }
            key = {
                redirect.from
            }
            component = {
                () => (window.location = redirect.to)
            }
            />
        )
    })
}

export default loadRedirectedRoutes