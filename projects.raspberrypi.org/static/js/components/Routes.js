/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "_" }] */
import PropTypes from 'prop-types'
import React from 'react'
import {
    useCookies
} from 'react-cookie'
import {
    Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom'
import {
    translate
} from 'react-i18next'

import {
    AuthenticationContainerLoader,
    SilentRenewContainerLoader,
    CodeClubLoader,
    CoderDojoLoader,
    HomepageLoader,
    PathwayContainerLoader,
    ProgressListContainerLoader,
    ProjectContainerLoader,
    ProjectListContainerLoader,
    ProjectPrintContainerLoader,
} from 'AsyncLoaders'

import setUpHistory from 'helpers/history'
import config from 'helpers/config'

import NoMatch from 'components/shared/NoMatch/NoMatch'
import SiteHeader from 'components/shared/SiteHeader/SiteHeader'
import loadArchivedRoutes from 'components/ArchivedRedirects'
import loadLegacyRoutes from 'components/LegacyRedirects'
import loadRedirectedRoutes from 'components/Redirects'

const NoMatchHandler = translate('translations')(({
    t
}) => ( <
    div >
    <
    SiteHeader locale = "en" / >
    <
    NoMatch error = {
        {
            status: config.notFoundStatus,
            message: t('no-match.title.text'),
        }
    }
    /> <
    /div>
))

const HomePageRenderHandler = props => {
    if (!config.availableHomePageLanguages.includes(props.match.params.locale)) {
        return ( <
            Redirect to = {
                {
                    pathname: `/${props.match.params.locale}/projects`,
                }
            }
            />
        )
    }
    return <HomepageLoader { ...props
    }
    />
}

const TestLoginHandler = (setCookie, removeCookie, enable) => {
    if (enable) {
        setCookie('enableAccountsLogin', true, {
            path: '/'
        })
    } else {
        removeCookie('enableAccountsLogin', {
            path: '/'
        })
    }
    return <Redirect to = {
        {
            pathname: `/`
        }
    }
    />
}

HomePageRenderHandler.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            locale: PropTypes.string,
        }),
    }),
}

const Routes = ({
        i18n
    }) => {
        const [_cookies, setCookie, removeCookie] = useCookies([
            'enableAccountsLogin',
        ])

        return ( <
            Router history = {
                setUpHistory(i18n)
            } >
            <
            Switch >
            <
            Route exact path = "/rpi/silent_renew"
            component = {
                SilentRenewContainerLoader
            }
            /> {
                loadLegacyRoutes()
            } {
                loadArchivedRoutes()
            } {
                loadRedirectedRoutes()
            } <
            Route exact path = "/"
            render = {
                () => < Redirect to = "/en/" / >
            }
            /> <
            Route exact path = {
                ['/:locale/enable-accounts', '/enable-accounts']
            }
            render = {
                () => TestLoginHandler(setCookie, removeCookie, true)
            }
            /> <
            Route exact path = {
                ['/:locale/disable-accounts', '/disable-accounts']
            }
            render = {
                () => TestLoginHandler(setCookie, removeCookie, false)
            }
            /> <
            Route exact path = "/:locale"
            render = {
                props => HomePageRenderHandler(props)
            }
            /> <
            Route exact path = "/:locale/codeclub/"
            component = {
                CodeClubLoader
            }
            /> <
            Route exact path = "/:locale/coderdojo/"
            component = {
                CoderDojoLoader
            }
            /> <
            Route exact path = "/:locale/jam"
            render = {
                ({
                    match: {
                        params: {
                            locale
                        },
                    },
                }) => < Redirect to = {
                    `/${locale}/jam/jam`
                }
                />} /
                >
                <
                Route
                exact
                path = "/:locale/projects"
                component = {
                    ProjectListContainerLoader
                }
                /> <
                Route
                exact
                path = "/:locale/projects/:progress(progress|finished)"
                component = {
                    ProgressListContainerLoader
                }
                /> <
                Route
                exact
                path = "/:locale/projects/:slug/print"
                component = {
                    ProjectPrintContainerLoader
                }
                /> <
                Route
                exact
                path = "/:locale/projects/:slug/complete"
                component = {
                    ProjectContainerLoader
                }
                /> <
                Route
                exact
                path = "/:locale/projects/:slug/:position?"
                component = {
                    ProjectContainerLoader
                }
                /> <
                Route
                exact
                path = "/:locale/:pathwayType/:slug"
                component = {
                    PathwayContainerLoader
                }
                /> <
                Route
                exact
                path = "/rpi/callback"
                component = {
                    AuthenticationContainerLoader
                }
                /> <
                Route component = {
                    NoMatchHandler
                }
                /> <
                /Switch> <
                /Router>
            )
        }

        Routes.propTypes = {
            i18n: PropTypes.object.isRequired,
        }
        export default translate('translations')(Routes)
        export {
            HomePageRenderHandler
        }