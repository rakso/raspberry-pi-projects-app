import React, {
    Component
} from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import {
    postFeedbackToAdmin
} from 'helpers/Feedback'
import {
    translate
} from 'react-i18next'
import './Footer.css'
import LicenseInfo from '../LicenseInfo/LicenseInfo'

export class Footer extends Component {
    constructor(props) {
        super(props)

        this.state = {
            feedbackFormDisabled: false,
            feedbackText: '',
            feedbackVisibleElement: 'prompt',
        }

        this.formStep = 5
    }

    /* eslint-disable camelcase */
    feedbackAttributes = () => {
        let attributes = {
            message: this.feedbackText(),
            userID: this.props.userID,
        }

        if (this.isProject()) {
            attributes.project_slug = this.props.projectSlug
            attributes.step_number = this.formStep
        }

        return attributes
    }
    /* eslint-enable camelcase */

    feedbackClassNames(element) {
        return classNames({
            [`c-project-footer__feedback-${element}`]: true,
            [`c-project-footer__feedback-${element}--hidden`]: element !== this.state.feedbackVisibleElement,
        })
    }

    canSubmitFeedback() {
        return !this.state.feedbackFormDisabled && Boolean(this.state.feedbackText)
    }

    feedbackFormSubmitHandler = event => {
        event.preventDefault()

        this.setState({
                feedbackFormDisabled: true,
            },
            () => this.postFeedback(),
        )
    }

    feedbackIntro = () => `URL: ${window.location.pathname}\n`

    feedbackPromptClickHandler = () =>
        this.setState({
            feedbackVisibleElement: 'form',
        })

    feedbackText = () => {
        if (this.isProject()) {
            return `${this.feedbackIntro()}\n\n${this.state.feedbackText}`
        }

        return this.state.feedbackText
    }

    isProject = () => typeof this.props.projectSlug === 'string'

    async postFeedback() {
        await postFeedbackToAdmin(this.feedbackAttributes())

        this.setState({
            feedbackVisibleElement: 'thanks',
        })
    }

    projectRepoUrl() {
        if (this.props.projectSlug) {
            return `https://github.com/RaspberryPiLearning/${this.props.projectSlug}`
        }

        return null
    }

    updateFeedbackText = event => {
        this.setState({
            feedbackText: event.target.value,
        })
    }

    render() {
        const t = this.props.t
        return ( <
            footer className = "c-project-footer" >
            <
            p > {
                t(this.props.text)
            } < /p>

            <
            button className = {
                this.feedbackClassNames('prompt')
            }
            onClick = {
                this.feedbackPromptClickHandler
            } >
            {
                t('project.footer.feedback.button.text')
            } {
                ' '
            } <
            /button>

            <
            form className = {
                this.feedbackClassNames('form')
            }
            onSubmit = {
                this.feedbackFormSubmitHandler
            } >
            <
            textarea className = "c-project-footer__feedback-textarea"
            cols = "40"
            disabled = {
                this.state.feedbackFormDisabled
            }
            onChange = {
                this.updateFeedbackText
            }
            placeholder = {
                `${t('project.footer.feedback.placeholder.text')}`
            }
            rows = "6"
            value = {
                this.state.feedbackText
            }
            /> <
            button className = "c-project-footer__feedback-submit"
            disabled = {!this.canSubmitFeedback()
            }
            type = "submit" >
            {
                this.state.feedbackFormDisabled ?
                t('project.footer.feedback.button.disabled.text') :
                    t('project.footer.feedback.button.text')
            } <
            /button> <
            /form>

            <
            div className = {
                this.feedbackClassNames('thanks')
            } > {
                t('project.footer.feedback.thank-you.text')
            } <
            /div>

            <
            LicenseInfo projectRepoUrl = {
                this.projectRepoUrl()
            }
            /> <
            div className = "c-project-footer-policy" >
            <
            p >
            <
            a className = "c-project-footer__link c-project-footer__link-option"
            href = "https://www.raspberrypi.org/cookies/" >
            {
                t('project.footer.license.cookies')
            } <
            /a> <
            /p> <
            p >
            <
            a className = "c-project-footer__link"
            href = "https://www.raspberrypi.org/privacy/" >
            {
                t('project.footer.license.privacy')
            } <
            /a> <
            /p> <
            /div> <
            /footer>
        )
    }
}

Footer.propTypes = {
    projectSlug: PropTypes.string,
    t: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    userID: PropTypes.string,
}

export default translate('translations')(Footer)