import PropTypes from 'prop-types'
import React from 'react'
import {
    connect
} from 'react-redux'

import Footer from './Footer'

const FooterContainer = ({
    slug,
    text,
    userID
}) => {
    return <Footer projectSlug = {
        slug
    }
    text = {
        text
    }
    userID = {
        userID
    }
    />
}

FooterContainer.propTypes = {
    slug: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    userID: PropTypes.string,
}

const mapStateToProps = state => {
    const {
        slug
    } = state.project
    return {
        slug,
    }
}

export default connect(mapStateToProps)(FooterContainer)