import PropTypes from 'prop-types'
import React from 'react'
import {
    Helmet
} from 'react-helmet'

import {
    htmlTitle
} from 'helpers/htmlTitle'

const HtmlHead = ({
    titlePrefix
}) => ( <
    Helmet >
    <
    title > {
        htmlTitle(titlePrefix)
    } < /title> <
    /Helmet>
)

HtmlHead.propTypes = {
    titlePrefix: PropTypes.string,
}

export default HtmlHead