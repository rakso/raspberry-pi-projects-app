import './SiteHeader.css'

import LanguageSelector from './LanguageSelector/LanguageSelector'
import UserStatus from './UserStatus/UserStatus'

import {
    useCookies
} from 'react-cookie'
import {
    Link
} from 'react-router-dom'
import PropTypes from 'prop-types'
import React from 'react'
import logomark from './images/logomark.svg'
import {
    translate
} from 'react-i18next'

export const SiteHeader = ({
    locale,
    t
}) => {
    const [cookies] = useCookies(['enableAccountsLogin'])

    return ( <
        header className = "c-site-header"
        id = "c-site-header"
        role = "banner" >
        <
        div className = "c-site-header__container" >
        <
        a className = "c-site-header__rpf-link"
        href = "https://raspberrypi.org" >
        <
        img alt = ""
        className = "c-site-header__rpf-logomark"
        src = {
            logomark
        }
        /> <
        /a> {
            locale !== 'en' && ( <
                Link className = "c-site-header__home-link"
                to = {
                    `/${locale}`
                } > {
                    t('navigation.projects')
                } <
                /Link>
            )
        } {
            locale === 'en' && ( <
                React.Fragment >
                <
                Link className = "c-site-header__home-link"
                to = {
                    `/${locale}`
                } > {
                    t('navigation.home')
                } <
                /Link> <
                Link className = "c-site-header__nav-link"
                to = {
                    `/${locale}/projects`
                } >
                {
                    t('navigation.projects')
                } <
                /Link> <
                Link className = "c-site-header__nav-link"
                to = {
                    `/${locale}/codeclub`
                } >
                {
                    t('navigation.codeclub')
                } <
                /Link> <
                Link className = "c-site-header__nav-link"
                to = {
                    `/${locale}/coderdojo`
                } >
                {
                    t('navigation.coderdojo')
                } <
                /Link> <
                Link className = "c-site-header__nav-link"
                to = {
                    `/${locale}/jam`
                } > {
                    t('navigation.jam')
                } <
                /Link> <
                /React.Fragment>
            )
        }

        {
            process.env.REACT_APP_AUTHENTICATION === 'true' &&
                cookies.enableAccountsLogin && < UserStatus / >
        }

        <
        div className = "c-site-header__language-selector" >
        <
        LanguageSelector / >
        <
        /div> <
        /div> <
        /header>
    )
}

SiteHeader.propTypes = {
    locale: PropTypes.string.isRequired,
    t: PropTypes.func.isRequired,
}

export default translate('translations')(SiteHeader)