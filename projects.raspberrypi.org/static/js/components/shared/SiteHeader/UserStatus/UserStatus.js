import React from 'react'
import PropTypes from 'prop-types'
import {
    connect
} from 'react-redux'
import {
    translate
} from 'react-i18next'

import {
    userManager
} from 'helpers/authentication'

const redirectToLogIn = e => {
    e.preventDefault()
    userManager.signinRedirect()
}

const redirectToLogOut = e => {
    e.preventDefault()
    userManager.removeUser()
}

const UserStatus = ({
    authentication,
    t
}) => ( <
    div > {!authentication.user && ( <
            a className = "c-site-header__home-link"
            href = "/login"
            onClick = {
                redirectToLogIn
            } >
            {
                t('navigation.login')
            } <
            /a>
        )
    }

    {
        authentication.user && ( <
            div >
            <
            span className = "c-site-header__home-link" > {
                authentication.user.profile.name
            } <
            /span> <
            a className = "c-site-header__home-link"
            href = "/logout"
            onClick = {
                redirectToLogOut
            } >
            {
                t('navigation.logout')
            } <
            /a> <
            /div>
        )
    } <
    /div>
)

UserStatus.propTypes = {
    authentication: PropTypes.shape({
        user: PropTypes.shape({
            profile: PropTypes.shape({
                name: PropTypes.string.isRequired,
            }),
        }),
    }),
    t: PropTypes.func.isRequired,
}

const mapStateToProps = state => {
    const {
        authentication
    } = state
    return {
        authentication,
    }
}

export {
    UserStatus
}
export default translate('translations')(connect(mapStateToProps)(UserStatus))