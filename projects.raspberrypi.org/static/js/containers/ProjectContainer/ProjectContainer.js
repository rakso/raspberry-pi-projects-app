import PropTypes from 'prop-types'
import React, {
    Component
} from 'react'
import {
    connect
} from 'react-redux'
import {
    Helmet
} from 'react-helmet'
import i18next from 'i18next'

import Loader from 'components/shared/Loader/Loader'
import NoMatch from 'components/shared/NoMatch/NoMatch'
import CompletedOrProjectContainer from 'components/Project/CompletedOrProjectContainer'
import SiteHeader from 'components/shared/SiteHeader/SiteHeader'
import ProjectNotTranslated from 'components/Project/ProjectNotTranslated/ProjectNotTranslated'

import {
    projectComplete,
    shouldSetProjectStep
} from 'helpers/Project'
import {
    fetchProject,
    setProjectStep
} from 'redux/actions/ProjectActions'
import {
    fetchFavourites
} from 'redux/actions/FavouriteActions'
import {
    createProgress,
    updateProgress
} from 'redux/actions/ProgressActions'

export class ProjectContainer extends Component {
    changeProject(locale, slug, position = 1) {
        const {
            dispatch,
            match,
            authentication
        } = this.props
        if (authentication.user) {
            dispatch(
                fetchFavourites(match.params.locale, authentication.user.access_token),
            )
        }
        dispatch(
            fetchProject({
                locale: locale,
                slug: slug,
                position: position,
            }),
        )
    }

    UNSAFE_componentWillMount() {
        const {
            match,
            projectFetched,
            slug,
            project
        } = this.props
        i18next.on('languageChanged', locale => {
            this.changeProject(locale, match.params.slug, match.params.position)
        })
        if (
            projectFetched !== 1 ||
            match.params.slug !== slug ||
            match.params.locale !== project.locale
        ) {
            this.changeProject(
                match.params.locale,
                match.params.slug,
                match.params.position,
            )
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (shouldSetProjectStep(this.props, nextProps)) {
            const {
                dispatch,
                project,
                slug,
                authentication,
                progress
            } = this.props
            const {
                match: {
                    params: {
                        position: nextPosition
                    },
                },
            } = nextProps
            const authToken =
                authentication && authentication.user ?
                authentication.user.access_token :
                null
            dispatch(setProjectStep(nextPosition))
            if (authToken) {
                if (progress.ids.includes(project.id)) {
                    dispatch(
                        updateProgress(
                            project.locale,
                            authToken,
                            project.id,
                            slug,
                            nextPosition,
                        ),
                    )
                } else {
                    dispatch(
                        createProgress(
                            project.locale,
                            authToken,
                            project.id,
                            slug,
                            nextPosition,
                        ),
                    )
                }
            }
        }
    }

    correctComponent() {
        const {
            error,
            match,
            projectFetched,
            project
        } = this.props
        const {
            locale,
            slug
        } = match.params
        let component
        if (projectFetched === -1) {
            component = < NoMatch error = {
                error
            }
            />
        } else if (projectFetched === 1 && project.availableLocales) {
            component = < ProjectNotTranslated project = {
                project
            }
            />
        } else if (projectFetched === 1 || project.title) {
            component = ( <
                React.Fragment >
                <
                Helmet >
                <
                link rel = "canonical"
                href = {
                    `https://projects.raspberrypi.org/${locale}/projects/${slug}`
                }
                /> <
                /Helmet> <
                CompletedOrProjectContainer projectComplete = {
                    projectComplete(match.url)
                }
                projectSlug = {
                    slug
                }
                /> <
                /React.Fragment>
            )
        } else {
            component = < Loader / >
        }
        return component
    }

    render() {
        const locale = this.props.match.params.locale
        return ( <
            div >
            <
            SiteHeader locale = {
                locale
            }
            /> {
                this.correctComponent()
            } <
            /div>
        )
    }
}

ProjectContainer.propTypes = {
    dispatch: PropTypes.func.isRequired,
    error: PropTypes.object,
    match: PropTypes.object.isRequired,
    projectFetched: PropTypes.number.isRequired,
    slug: PropTypes.string,
    projects: PropTypes.object,
    project: PropTypes.object,
    favourites: PropTypes.object,
    progress: PropTypes.object,
    authentication: PropTypes.object,
}

const mapStateToProps = state => {
    const {
        error,
        projectFetched,
        slug,
        project
    } = state.project
    const {
        authentication
    } = state
    return {
        error,
        projectFetched,
        slug,
        project,
        progress: state.progress,
        favourites: state.favourites,
        authentication,
        projects: state.projects.entities,
    }
}

export default connect(mapStateToProps)(ProjectContainer)