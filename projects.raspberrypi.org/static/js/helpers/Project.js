const isFavourite = (favourites, project) => {
    if (project && project.id) {
        return favourites.ids.includes(project.id.toString())
    }
    return false
}

const matchParamsPresent = props => props && props.match && props.match.params

const printUrl = (locale, slug) => `${projectPath(locale, slug)}/print`

const projectComplete = url => {
    const match = url.match(/..\/projects\/.+\/complete\/?$/i)
    return Boolean(match)
}

const projectPath = (locale, slug) => `/${locale}/projects/${slug}`

const shouldSetProjectStep = (props, nextProps) => {
    if (!(matchParamsPresent(props) && matchParamsPresent(nextProps))) {
        return false
    }

    const {
        match: {
            params: {
                position
            },
        },
    } = props
    const {
        match: {
            params: {
                position: nextPosition
            },
        },
    } = nextProps
    return position !== nextPosition
}

const themeClassName = theme => {
    if (typeof theme === 'undefined') {
        return ''
    }
    return `t-${theme}`
}
const transitionName = slideDirection => {
    return `c-project-steps__wrapper--animate-${slideDirection}`
}

export {
    isFavourite,
    printUrl,
    projectComplete,
    projectPath,
    shouldSetProjectStep,
    themeClassName,
    transitionName,
}