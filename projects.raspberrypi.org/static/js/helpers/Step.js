/* global Prism, scratchblocks */

import config from 'helpers/config'

const Swiper = require('swiper/dist/js/swiper')

const carouselInit = panelContent => {
    const carousel = panelContent.querySelector('.c-project-panel__swiper')

    if (carousel !== null) {
        // eslint-disable-next-line no-new
        new Swiper(carousel, {
            a11y: true,
            containerModifierClass: 'c-project-panel__swiper--',
            navigation: {
                disabledClass: 'c-project-panel__swiper-button--disabled',
                nextEl: carousel.querySelector('.c-project-panel__swiper-button--next'),
                prevEl: carousel.querySelector('.c-project-panel__swiper-button--prev'),
            },
            pagination: {
                clickable: true,
                bulletActiveClass: 'c-project-panel__swiper-bullet--active',
                bulletClass: 'c-project-panel__swiper-bullet',
                el: carousel.querySelector('.c-project-panel__swiper-pagination'),
                hideOnClick: false,
                type: 'bullets',
            },
            setWrapperSize: true,
            slideClass: 'c-project-panel__swiper-slide',
            spaceBetween: 80,
            wrapperClass: 'c-project-panel__swiper-wrapper',
        })

        panelContent.classList.remove('js-project-panel--initialise-swiper')
        panelContent.classList.add('js-project-panel--swiper-initialised')
    }
}

const isChallenge = step =>
    typeof step.challenge !== 'undefined' && step.challenge

const isHint = panelContent => {
    return (
        panelContent.classList.contains('js-project-panel--initialise-swiper') ||
        panelContent.classList.contains('js-project-panel--swiper-initialised')
    )
}

const projectPanelToggleClickHandler = event => {
    const clickedElement = event.target

    if (!clickedElement.classList.contains('js-project-panel__toggle')) {
        return
    }

    clickedElement.classList.toggle('c-project-panel__heading--has-close-icon')
    const panelContent = clickedElement.nextElementSibling

    panelContent.classList.toggle('u-hidden')

    if (isHint(panelContent)) {
        carouselInit(panelContent)
    }

    trackHintIngredient(event, isHint(panelContent))
}

const projectPanelToggleInit = () => {
    const projectContent = document.querySelector('.c-project__content')

    if (projectContent === null) {
        return
    }

    projectContent.addEventListener('click', projectPanelToggleClickHandler)
}

const initScratchblocksHighlighting = () => {
    const selectors = {
        '.language-blocks': 'scratch2',
        '.language-blocks3': 'scratch3',
    }

    Object.entries(selectors).forEach(entry => {
        const [selector, style] = entry

        scratchblocks.renderMatching(selector, {
            languages: config.scratchSupportedLanguages,
            style: style,
        })
    })
}

const initPrismSyntaxHighlighting = () => {
    const selector = [
        'pre code:not([class*="language-blocks"])',
        'code[class*="language-"]:not([class*="language-blocks"])',
    ].join(', ')
    const elements = document.querySelectorAll(selector)

    if (elements.length > 0) {
        for (const element of elements) {
            Prism.highlightElement(element)
        }
    }
}

const removeScratchPreviewIframes = () => {
    const iframes = document.querySelectorAll('.scratch-preview iframe')
    for (let iframe of iframes) {
        iframe.parentNode.removeChild(iframe)
    }
}

const stepInit = () => {
    initScratchblocksHighlighting()
    initPrismSyntaxHighlighting()
    projectPanelToggleInit()
}

const stepPositon = position => (position ? parseInt(position, 10) : 1)

const stepSlideDirection = (currentStepPosition, nextStepPosition) =>
    currentStepPosition > nextStepPosition ? 'left' : 'right'

const trackHintIngredient = (event, hint) => {
    const title = event.target.textContent.trim()
    window.ga('send', {
        hitType: 'event',
        eventCategory: `Project ${hint ? 'hint' : 'ingredient'}`,
        eventAction: `Clicked ${hint ? 'hint' : `ingredient ${title}`}`,
        eventLabel: event.target.baseURI,
    })
}

export {
    initPrismSyntaxHighlighting,
    initScratchblocksHighlighting,
    isChallenge,
    isHint,
    removeScratchPreviewIframes,
    stepInit,
    stepPositon,
    stepSlideDirection,
}