/* eslint-disable */
/* istanbul ignore file */
(function() {
    var aa = "function" == typeof Object.defineProperties ? Object.defineProperty : function(a, b, c) {
            if (c.get || c.set) throw new TypeError("ES3 does not support getters and setters.");
            a != Array.prototype && a != Object.prototype && (a[b] = c.value)
        },
        g = "undefined" != typeof window && window === this ? this : "undefined" != typeof global && null != global ? global : this;

    function k() {
        k = function() {};
        g.Symbol || (g.Symbol = ba)
    }
    var ca = 0;

    function ba(a) {
        return "jscomp_symbol_" + (a || "") + ca++
    }

    function l() {
        k();
        var a = g.Symbol.iterator;
        a || (a = g.Symbol.iterator = g.Symbol("iterator"));
        "function" != typeof Array.prototype[a] && aa(Array.prototype, a, {
            configurable: !0,
            writable: !0,
            value: function() {
                return da(this)
            }
        });
        l = function() {}
    }

    function da(a) {
        var b = 0;
        return ea(function() {
            return b < a.length ? {
                done: !1,
                value: a[b++]
            } : {
                done: !0
            }
        })
    }

    function ea(a) {
        l();
        a = {
            next: a
        };
        a[g.Symbol.iterator] = function() {
            return this
        };
        return a
    }

    function fa(a) {
        l();
        k();
        l();
        var b = a[Symbol.iterator];
        return b ? b.call(a) : da(a)
    }

    function m(a) {
        if (!(a instanceof Array)) {
            a = fa(a);
            for (var b, c = []; !(b = a.next()).done;) c.push(b.value);
            a = c
        }
        return a
    }

    function ia(a, b) {
        function c() {}
        c.prototype = b.prototype;
        a.P = b.prototype;
        a.prototype = new c;
        a.prototype.constructor = a;
        for (var d in b)
            if (Object.defineProperties) {
                var e = Object.getOwnPropertyDescriptor(b, d);
                e && Object.defineProperty(a, d, e)
            } else a[d] = b[d]
    }
    var n = window.Element.prototype,
        ja = n.matches || n.matchesSelector || n.webkitMatchesSelector || n.mozMatchesSelector || n.msMatchesSelector || n.oMatchesSelector;

    function ka(a, b) {
        if (a && 1 == a.nodeType && b) {
            if ("string" == typeof b || 1 == b.nodeType) return a == b || la(a, b);
            if ("length" in b)
                for (var c = 0, d; d = b[c]; c++)
                    if (a == d || la(a, d)) return !0
        }
        return !1
    }

    function la(a, b) {
        if ("string" != typeof b) return !1;
        if (ja) return ja.call(a, b);
        b = a.parentNode.querySelectorAll(b);
        for (var c = 0, d; d = b[c]; c++)
            if (d == a) return !0;
        return !1
    }

    function ma(a) {
        for (var b = []; a && a.parentNode && 1 == a.parentNode.nodeType;) a = a.parentNode, b.push(a);
        return b
    }

    function na(a, b, c) {
        function d(a) {
            var d;
            if (h.composed && "function" == typeof a.composedPath)
                for (var e = a.composedPath(), f = 0, F; F = e[f]; f++) 1 == F.nodeType && ka(F, b) && (d = F);
            else a: {
                if ((d = a.target) && 1 == d.nodeType && b)
                    for (d = [d].concat(ma(d)), e = 0; f = d[e]; e++)
                        if (ka(f, b)) {
                            d = f;
                            break a
                        }
                d = void 0
            }
            d && c.call(d, a, d)
        }
        var e = document,
            h = {
                composed: !0,
                B: !0
            },
            h = void 0 === h ? {} : h;
        e.addEventListener(a, d, h.B);
        return {
            j: function() {
                e.removeEventListener(a, d, h.B)
            }
        }
    }

    function oa(a) {
        var b = {};
        if (!a || 1 != a.nodeType) return b;
        a = a.attributes;
        if (!a.length) return {};
        for (var c = 0, d; d = a[c]; c++) b[d.name] = d.value;
        return b
    }
    var pa = /:(80|443)$/,
        p = document.createElement("a"),
        q = {};

    function r(a) {
        a = a && "." != a ? a : location.href;
        if (q[a]) return q[a];
        p.href = a;
        if ("." == a.charAt(0) || "/" == a.charAt(0)) return r(p.href);
        var b = "80" == p.port || "443" == p.port ? "" : p.port,
            b = "0" == b ? "" : b,
            c = p.host.replace(pa, "");
        return q[a] = {
            hash: p.hash,
            host: c,
            hostname: p.hostname,
            href: p.href,
            origin: p.origin ? p.origin : p.protocol + "//" + c,
            pathname: "/" == p.pathname.charAt(0) ? p.pathname : "/" + p.pathname,
            port: b,
            protocol: p.protocol,
            search: p.search
        }
    }
    var t = [];

    function qa(a, b) {
        var c = this;
        this.context = a;
        this.v = b;
        this.f = (this.c = /Task$/.test(b)) ? a.get(b) : a[b];
        this.b = [];
        this.a = [];
        this.g = function(a) {
            for (var b = [], d = 0; d < arguments.length; ++d) b[d - 0] = arguments[d];
            return c.a[c.a.length - 1].apply(null, [].concat(m(b)))
        };
        this.c ? a.set(b, this.g) : a[b] = this.g
    }

    function u(a, b, c) {
        a = ra(a, b);
        a.b.push(c);
        sa(a)
    }

    function v(a, b, c) {
        a = ra(a, b);
        c = a.b.indexOf(c); - 1 < c && (a.b.splice(c, 1), 0 < a.b.length ? sa(a) : a.j())
    }

    function sa(a) {
        a.a = [];
        for (var b, c = 0; b = a.b[c]; c++) {
            var d = a.a[c - 1] || a.f.bind(a.context);
            a.a.push(b(d))
        }
    }
    qa.prototype.j = function() {
        var a = t.indexOf(this); - 1 < a && (t.splice(a, 1), this.c ? this.context.set(this.v, this.f) : this.context[this.v] = this.f)
    };

    function ra(a, b) {
        var c = t.filter(function(c) {
            return c.context == a && c.v == b
        })[0];
        c || (c = new qa(a, b), t.push(c));
        return c
    }

    function w(a, b, c, d, e, h) {
        if ("function" == typeof d) {
            var f = c.get("buildHitTask");
            return {
                buildHitTask: function(c) {
                    c.set(a, null, !0);
                    c.set(b, null, !0);
                    d(c, e, h);
                    f(c)
                }
            }
        }
        return x({}, a, b)
    }

    function ta(a, b) {
        var c = oa(a),
            d = {};
        Object.keys(c).forEach(function(a) {
            if (!a.indexOf(b) && a != b + "on") {
                var e = c[a];
                "true" == e && (e = !0);
                "false" == e && (e = !1);
                a = ua(a.slice(b.length));
                d[a] = e
            }
        });
        return d
    }

    function va(a) {
        var b;
        return function(c) {
            for (var d = [], e = 0; e < arguments.length; ++e) d[e - 0] = arguments[e];
            clearTimeout(b);
            b = setTimeout(function() {
                return a.apply(null, [].concat(m(d)))
            }, 500)
        }
    }

    function wa(a) {
        function b() {
            c || (c = !0, a())
        }
        var c = !1;
        setTimeout(b, 2E3);
        return b
    }
    var y = {};

    function xa(a, b) {
        function c() {
            clearTimeout(e.timeout);
            e.send && v(a, "send", e.send);
            delete y[d];
            e.A.forEach(function(a) {
                return a()
            })
        }
        var d = a.get("trackingId"),
            e = y[d] = y[d] || {};
        clearTimeout(e.timeout);
        e.timeout = setTimeout(c, 0);
        e.A = e.A || [];
        e.A.push(b);
        e.send || (e.send = function(a) {
            return function(b) {
                for (var d = [], e = 0; e < arguments.length; ++e) d[e - 0] = arguments[e];
                c();
                a.apply(null, [].concat(m(d)))
            }
        }, u(a, "send", e.send))
    }
    var x = Object.assign || function(a, b) {
        for (var c = [], d = 1; d < arguments.length; ++d) c[d - 1] = arguments[d];
        for (var d = 0, e = c.length; d < e; d++) {
            var h = Object(c[d]),
                f;
            for (f in h) Object.prototype.hasOwnProperty.call(h, f) && (a[f] = h[f])
        }
        return a
    };

    function ua(a) {
        return a.replace(/[\-\_]+(\w?)/g, function(a, c) {
            return c.toUpperCase()
        })
    }
    var z = function ya(b) {
        return b ? (b ^ 16 * Math.random() >> b / 4).toString(16) : "10000000-1000-4000-8000-100000000000".replace(/[018]/g, ya)
    };

    function A(a, b) {
        var c = window.GoogleAnalyticsObject || "ga";
        window[c] = window[c] || function(a) {
            for (var b = [], d = 0; d < arguments.length; ++d) b[d - 0] = arguments[d];
            (window[c].q = window[c].q || []).push(b)
        };
        window.gaDevIds = window.gaDevIds || [];
        0 > window.gaDevIds.indexOf("i5iSjo") && window.gaDevIds.push("i5iSjo");
        window[c]("provide", a, b);
        window.gaplugins = window.gaplugins || {};
        window.gaplugins[a.charAt(0).toUpperCase() + a.slice(1)] = b
    }
    var B = {
            C: 1,
            D: 2,
            L: 3,
            M: 4,
            N: 5,
            G: 6,
            H: 7,
            O: 8,
            I: 9,
            F: 10
        },
        C = Object.keys(B).length;

    function D(a, b) {
        a.set("\x26_av", "2.4.1");
        var c = a.get("\x26_au"),
            c = parseInt(c || "0", 16).toString(2);
        if (c.length < C)
            for (var d = C - c.length; d;) c = "0" + c, d--;
        b = C - b;
        c = c.substr(0, b) + 1 + c.substr(b + 1);
        a.set("\x26_au", parseInt(c || "0", 2).toString(16))
    }

    function E(a, b) {
        D(a, B.C);
        this.a = x({}, b);
        this.g = a;
        this.b = this.a.stripQuery && this.a.queryDimensionIndex ? "dimension" + this.a.queryDimensionIndex : null;
        this.f = this.f.bind(this);
        this.c = this.c.bind(this);
        u(a, "get", this.f);
        u(a, "buildHitTask", this.c)
    }
    E.prototype.f = function(a) {
        var b = this;
        return function(c) {
            if ("page" == c || c == b.b) {
                var d = {
                    location: a("location"),
                    page: a("page")
                };
                return za(b, d)[c]
            }
            return a(c)
        }
    };
    E.prototype.c = function(a) {
        var b = this;
        return function(c) {
            var d = za(b, {
                location: c.get("location"),
                page: c.get("page")
            });
            c.set(d, null, !0);
            a(c)
        }
    };

    function za(a, b) {
        var c = r(b.page || b.location),
            d = c.pathname;
        if (a.a.indexFilename) {
            var e = d.split("/");
            a.a.indexFilename == e[e.length - 1] && (e[e.length - 1] = "", d = e.join("/"))
        }
        "remove" == a.a.trailingSlash ? d = d.replace(/\/+$/, "") : "add" == a.a.trailingSlash && (/\.\w+$/.test(d) || "/" == d.substr(-1) || (d += "/"));
        d = {
            page: d + (a.a.stripQuery ? Aa(a, c.search) : c.search)
        };
        b.location && (d.location = b.location);
        a.b && (d[a.b] = c.search.slice(1) || "(not set)");
        return "function" == typeof a.a.urlFieldsFilter ? (b = a.a.urlFieldsFilter(d, r), c = {
            page: b.page,
            location: b.location
        }, a.b && (c[a.b] = b[a.b]), c) : d
    }

    function Aa(a, b) {
        if (Array.isArray(a.a.queryParamsWhitelist)) {
            var c = [];
            b.slice(1).split("\x26").forEach(function(b) {
                var d = fa(b.split("\x3d"));
                b = d.next().value;
                d = d.next().value; - 1 < a.a.queryParamsWhitelist.indexOf(b) && d && c.push([b, d])
            });
            return c.length ? "?" + c.map(function(a) {
                return a.join("\x3d")
            }).join("\x26") : ""
        }
        return ""
    }
    E.prototype.remove = function() {
        v(this.g, "get", this.f);
        v(this.g, "buildHitTask", this.c)
    };
    A("cleanUrlTracker", E);

    function Ba() {
        this.a = {}
    }

    function Ca(a, b) {
        (a.a.externalSet = a.a.externalSet || []).push(b)
    }
    Ba.prototype.J = function(a, b) {
        for (var c = [], d = 1; d < arguments.length; ++d) c[d - 1] = arguments[d];
        (this.a[a] = this.a[a] || []).forEach(function(a) {
            return a.apply(null, [].concat(m(c)))
        })
    };
    var G = {},
        H = !1,
        I;

    function J(a, b) {
        b = void 0 === b ? {} : b;
        this.a = {};
        this.b = a;
        this.m = b;
        this.i = null
    }
    ia(J, Ba);

    function K(a, b, c) {
        a = ["autotrack", a, b].join(":");
        G[a] || (G[a] = new J(a, c), H || (window.addEventListener("storage", Da), H = !0));
        return G[a]
    }

    function L() {
        if (null != I) return I;
        try {
            window.localStorage.setItem("autotrack", "autotrack"), window.localStorage.removeItem("autotrack"), I = !0
        } catch (a) {
            I = !1
        }
        return I
    }
    J.prototype.get = function() {
        if (this.i) return this.i;
        if (L()) try {
            this.i = M(window.localStorage.getItem(this.b))
        } catch (a) {}
        return this.i = x({}, this.m, this.i)
    };
    J.prototype.set = function(a) {
        this.i = x({}, this.m, this.i, a);
        if (L()) try {
            var b = JSON.stringify(this.i);
            window.localStorage.setItem(this.b, b)
        } catch (c) {}
    };

    function N(a) {
        a.i = {};
        if (L()) try {
            window.localStorage.removeItem(a.b)
        } catch (b) {}
    }
    J.prototype.j = function() {
        delete G[this.b];
        Object.keys(G).length || (window.removeEventListener("storage", Da), H = !1)
    };

    function Da(a) {
        var b = G[a.key];
        if (b) {
            var c = x({}, b.m, M(a.oldValue));
            a = x({}, b.m, M(a.newValue));
            b.i = a;
            b.J("externalSet", a, c)
        }
    }

    function M(a) {
        var b = {};
        if (a) try {
            b = JSON.parse(a)
        } catch (c) {}
        return b
    }
    var O = {};

    function P(a, b, c) {
        this.f = a;
        this.timeout = b || Q;
        this.timeZone = c;
        this.b = this.b.bind(this);
        u(a, "sendHitTask", this.b);
        try {
            this.c = new Intl.DateTimeFormat("en-US", {
                timeZone: this.timeZone
            })
        } catch (d) {}
        this.a = K(a.get("trackingId"), "session", {
            hitTime: 0,
            isExpired: !1
        });
        this.a.get().id || this.a.set({
            id: z()
        })
    }

    function Ea(a, b, c) {
        var d = a.get("trackingId");
        return O[d] ? O[d] : O[d] = new P(a, b, c)
    }

    function R(a) {
        return a.a.get().id
    }
    P.prototype.isExpired = function(a) {
        a = void 0 === a ? R(this) : a;
        if (a != R(this)) return !0;
        a = this.a.get();
        if (a.isExpired) return !0;
        var b = a.hitTime;
        return b && (a = new Date, b = new Date(b), a - b > 6E4 * this.timeout || this.c && this.c.format(a) != this.c.format(b)) ? !0 : !1
    };
    P.prototype.b = function(a) {
        var b = this;
        return function(c) {
            a(c);
            var d = c.get("sessionControl");
            c = "start" == d || b.isExpired();
            var d = "end" == d,
                e = b.a.get();
            e.hitTime = +new Date;
            c && (e.isExpired = !1, e.id = z());
            d && (e.isExpired = !0);
            b.a.set(e)
        }
    };
    P.prototype.j = function() {
        v(this.f, "sendHitTask", this.b);
        this.a.j();
        delete O[this.f.get("trackingId")]
    };
    var Q = 30;

    function S(a, b) {
        D(a, B.F);
        window.addEventListener && (this.b = x({
            increaseThreshold: 20,
            sessionTimeout: Q,
            fieldsObj: {}
        }, b), this.f = a, this.c = Fa(this), this.g = va(this.g.bind(this)), this.l = this.l.bind(this), this.a = K(a.get("trackingId"), "plugins/max-scroll-tracker"), this.h = Ea(a, this.b.sessionTimeout, this.b.timeZone), u(a, "set", this.l), Ga(this))
    }

    function Ga(a) {
        100 > (a.a.get()[a.c] || 0) && window.addEventListener("scroll", a.g)
    }
    S.prototype.g = function() {
        var a = document.documentElement,
            b = document.body,
            a = Math.min(100, Math.max(0, Math.round(window.pageYOffset / (Math.max(a.offsetHeight, a.scrollHeight, b.offsetHeight, b.scrollHeight) - window.innerHeight) * 100))),
            b = R(this.h);
        b != this.a.get().sessionId && (N(this.a), this.a.set({
            sessionId: b
        }));
        if (this.h.isExpired(this.a.get().sessionId)) N(this.a);
        else if (b = this.a.get()[this.c] || 0, a > b && (100 != a && 100 != b || window.removeEventListener("scroll", this.g), b = a - b, 100 == a || b >= this.b.increaseThreshold)) {
            var c = {};
            this.a.set((c[this.c] = a, c.sessionId = R(this.h), c));
            a = {
                transport: "beacon",
                eventCategory: "Max Scroll",
                eventAction: "increase",
                eventValue: b,
                eventLabel: String(a),
                nonInteraction: !0
            };
            this.b.maxScrollMetricIndex && (a["metric" + this.b.maxScrollMetricIndex] = b);
            this.f.send("event", w(a, this.b.fieldsObj, this.f, this.b.hitFilter))
        }
    };
    S.prototype.l = function(a) {
        var b = this;
        return function(c, d) {
            a(c, d);
            var e = {};
            ("object" == typeof c && null !== c ? c : (e[c] = d, e)).page && (c = b.c, b.c = Fa(b), b.c != c && Ga(b))
        }
    };

    function Fa(a) {
        a = r(a.f.get("page") || a.f.get("location"));
        return a.pathname + a.search
    }
    S.prototype.remove = function() {
        this.h.j();
        window.removeEventListener("scroll", this.g);
        v(this.f, "set", this.l)
    };
    A("maxScrollTracker", S);

    function T(a, b) {
        var c = this;
        D(a, B.G);
        window.addEventListener && (this.a = x({
            events: ["click"],
            linkSelector: "a, area",
            shouldTrackOutboundLink: this.shouldTrackOutboundLink,
            fieldsObj: {},
            attributePrefix: "ga-"
        }, b), this.c = a, this.f = this.f.bind(this), this.b = {}, this.a.events.forEach(function(a) {
            c.b[a] = na(a, c.a.linkSelector, c.f)
        }))
    }
    T.prototype.f = function(a, b) {
        var c = this;
        if (this.a.shouldTrackOutboundLink(b, r)) {
            var d = b.getAttribute("href") || b.getAttribute("xlink:href"),
                e = r(d),
                e = {
                    transport: "beacon",
                    eventCategory: "Outbound Link",
                    eventAction: a.type,
                    eventLabel: e.href
                },
                h = x({}, this.a.fieldsObj, ta(b, this.a.attributePrefix)),
                f = w(e, h, this.c, this.a.hitFilter, b, a);
            if (navigator.sendBeacon || "click" != a.type || "_blank" == b.target || a.metaKey || a.ctrlKey || a.shiftKey || a.altKey || 1 < a.which) this.c.send("event", f);
            else {
                var ha = function() {
                    window.removeEventListener("click",
                        ha);
                    if (!a.defaultPrevented) {
                        a.preventDefault();
                        var b = f.hitCallback;
                        f.hitCallback = wa(function() {
                            "function" == typeof b && b();
                            location.href = d
                        })
                    }
                    c.c.send("event", f)
                };
                window.addEventListener("click", ha)
            }
        }
    };
    T.prototype.shouldTrackOutboundLink = function(a, b) {
        a = a.getAttribute("href") || a.getAttribute("xlink:href");
        b = b(a);
        return b.hostname != location.hostname && "http" == b.protocol.slice(0, 4)
    };
    T.prototype.remove = function() {
        var a = this;
        Object.keys(this.b).forEach(function(b) {
            a.b[b].j()
        })
    };
    A("outboundLinkTracker", T);
    var U = z();

    function V(a, b) {
        var c = this;
        D(a, B.H);
        document.visibilityState && (this.a = x({
                sessionTimeout: Q,
                visibleThreshold: 5E3,
                sendInitialPageview: !1,
                fieldsObj: {}
            }, b), this.b = a, this.h = document.visibilityState, this.s = null, this.w = !1, this.l = this.l.bind(this), this.f = this.f.bind(this), this.o = this.o.bind(this), this.u = this.u.bind(this), this.c = K(a.get("trackingId"), "plugins/page-visibility-tracker"), Ca(this.c, this.u), this.g = Ea(a, this.a.sessionTimeout, this.a.timeZone), u(a, "set", this.l), window.addEventListener("unload", this.o),
            document.addEventListener("visibilitychange", this.f), xa(this.b, function() {
                if ("visible" == document.visibilityState) c.a.sendInitialPageview && (W(c, {
                    K: !0
                }), c.w = !0), c.c.set({
                    time: +new Date,
                    state: "visible",
                    pageId: U,
                    sessionId: R(c.g)
                });
                else if (c.a.sendInitialPageview && c.a.pageLoadsMetricIndex) {
                    var a = {},
                        a = (a.transport = "beacon", a.eventCategory = "Page Visibility", a.eventAction = "page load", a.eventLabel = "(not set)", a["metric" + c.a.pageLoadsMetricIndex] = 1, a.nonInteraction = !0, a);
                    c.b.send("event", w(a, c.a.fieldsObj,
                        c.b, c.a.hitFilter))
                }
            }))
    }
    V.prototype.f = function() {
        var a = this;
        if ("visible" == document.visibilityState || "hidden" == document.visibilityState) {
            var b = Ha(this),
                c = {
                    time: +new Date,
                    state: document.visibilityState,
                    pageId: U,
                    sessionId: R(this.g)
                };
            "visible" == document.visibilityState && this.a.sendInitialPageview && !this.w && (W(this), this.w = !0);
            "hidden" == document.visibilityState && this.s && clearTimeout(this.s);
            this.g.isExpired(b.sessionId) ? (N(this.c), "hidden" == this.h && "visible" == document.visibilityState && (clearTimeout(this.s), this.s = setTimeout(function() {
                a.c.set(c);
                W(a, {
                    hitTime: c.time
                })
            }, this.a.visibleThreshold))) : (b.pageId == U && "visible" == b.state && Ia(this, b), this.c.set(c));
            this.h = document.visibilityState
        }
    };

    function Ha(a) {
        var b = a.c.get();
        "visible" == a.h && "hidden" == b.state && b.pageId != U && (b.state = "visible", b.pageId = U, a.c.set(b));
        return b
    }

    function Ia(a, b, c) {
        c = (c ? c : {}).hitTime;
        var d = {
                hitTime: c
            },
            d = (d ? d : {}).hitTime;
        (b = b.time ? (d || +new Date) - b.time : 0) && b >= a.a.visibleThreshold && (b = Math.round(b / 1E3), d = {
            transport: "beacon",
            nonInteraction: !0,
            eventCategory: "Page Visibility",
            eventAction: "track",
            eventValue: b,
            eventLabel: "(not set)"
        }, c && (d.queueTime = +new Date - c), a.a.visibleMetricIndex && (d["metric" + a.a.visibleMetricIndex] = b), a.b.send("event", w(d, a.a.fieldsObj, a.b, a.a.hitFilter)))
    }

    function W(a, b) {
        var c = b ? b : {};
        b = c.hitTime;
        var c = c.K,
            d = {
                transport: "beacon"
            };
        b && (d.queueTime = +new Date - b);
        c && a.a.pageLoadsMetricIndex && (d["metric" + a.a.pageLoadsMetricIndex] = 1);
        a.b.send("pageview", w(d, a.a.fieldsObj, a.b, a.a.hitFilter))
    }
    V.prototype.l = function(a) {
        var b = this;
        return function(c, d) {
            var e = {},
                e = "object" == typeof c && null !== c ? c : (e[c] = d, e);
            e.page && e.page !== b.b.get("page") && "visible" == b.h && b.f();
            a(c, d)
        }
    };
    V.prototype.u = function(a, b) {
        a.time != b.time && (b.pageId != U || "visible" != b.state || this.g.isExpired(b.sessionId) || Ia(this, b, {
            hitTime: a.time
        }))
    };
    V.prototype.o = function() {
        "hidden" != this.h && this.f()
    };
    V.prototype.remove = function() {
        this.c.j();
        this.g.j();
        v(this.b, "set", this.l);
        window.removeEventListener("unload", this.o);
        document.removeEventListener("visibilitychange", this.f)
    };
    A("pageVisibilityTracker", V);

    function X(a, b) {
        D(a, B.I);
        history.pushState && window.addEventListener && (this.a = x({
            shouldTrackUrlChange: this.shouldTrackUrlChange,
            trackReplaceState: !1,
            fieldsObj: {},
            hitFilter: null
        }, b), this.g = a, this.h = location.pathname + location.search, this.c = this.c.bind(this), this.f = this.f.bind(this), this.b = this.b.bind(this), u(history, "pushState", this.c), u(history, "replaceState", this.f), window.addEventListener("popstate", this.b))
    }
    X.prototype.c = function(a) {
        var b = this;
        return function(c) {
            for (var d = [], e = 0; e < arguments.length; ++e) d[e - 0] = arguments[e];
            a.apply(null, [].concat(m(d)));
            Y(b, !0)
        }
    };
    X.prototype.f = function(a) {
        var b = this;
        return function(c) {
            for (var d = [], e = 0; e < arguments.length; ++e) d[e - 0] = arguments[e];
            a.apply(null, [].concat(m(d)));
            Y(b, !1)
        }
    };
    X.prototype.b = function() {
        Y(this, !0)
    };

    function Y(a, b) {
        setTimeout(function() {
            var c = a.h,
                d = location.pathname + location.search;
            c != d && a.a.shouldTrackUrlChange.call(a, d, c) && (a.h = d, a.g.set({
                page: d,
                title: document.title
            }), (b || a.a.trackReplaceState) && a.g.send("pageview", w({
                transport: "beacon"
            }, a.a.fieldsObj, a.g, a.a.hitFilter)))
        }, 0)
    }
    X.prototype.shouldTrackUrlChange = function(a, b) {
        return !(!a || !b)
    };
    X.prototype.remove = function() {
        v(history, "pushState", this.c);
        v(history, "replaceState", this.f);
        window.removeEventListener("popstate", this.b)
    };
    A("urlChangeTracker", X);

    function Z(a, b) {
        var c = this;
        D(a, B.D);
        if (window.addEventListener) {
            this.a = x({
                events: ["click"],
                fieldsObj: {},
                attributePrefix: "ga-"
            }, b);
            this.f = a;
            this.c = this.c.bind(this);
            var d = "[" + this.a.attributePrefix + "on]";
            this.b = {};
            this.a.events.forEach(function(a) {
                c.b[a] = na(a, d, c.c)
            })
        }
    }
    Z.prototype.c = function(a, b) {
        var c = this.a.attributePrefix;
        if (!(0 > b.getAttribute(c + "on").split(/\s*,\s*/).indexOf(a.type))) {
            var c = ta(b, c),
                d = x({}, this.a.fieldsObj, c);
            this.f.send(c.hitType || "event", w({
                transport: "beacon"
            }, d, this.f, this.a.hitFilter, b, a))
        }
    };
    Z.prototype.remove = function() {
        var a = this;
        Object.keys(this.b).forEach(function(b) {
            a.b[b].j()
        })
    };
    A("eventTracker", Z);
})();