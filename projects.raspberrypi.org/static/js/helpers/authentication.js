import {
    CallbackComponent,
    OidcProvider,
    createUserManager,
    loadUser,
    reducer,
} from 'redux-oidc'

import {
    WebStorageStateStore
} from 'oidc-client'

const host = `${window.location.protocol}//${window.location.hostname}${
  window.location.port ? `:${window.location.port}` : ''
}`

/* eslint-disable camelcase */
const settings = {
    client_id: process.env.REACT_APP_AUTHENTICATION_CLIENT_ID,
    redirect_uri: `${host}/rpi/callback`,
    response_type: 'id_token token',
    scope: 'openid profile force-consent',
    authority: process.env.REACT_APP_AUTHENTICATION_URL,
    silent_redirect_uri: `${host}/rpi/silent_renew`,
    automaticSilentRenew: true,
    filterProtocolClaims: false,
    loadUserInfo: false,
    monitorSession: false,
    userStore: new WebStorageStateStore({
        store: window.localStorage
    }),
    extraQueryParams: {
        brand: 'projects',
    },
}
/* eslint-enable camelcase */

const userManager = createUserManager(settings)

userManager.events.addAccessTokenExpired(() => {
    // If the token has expired, trigger the silent renew process
    userManager.signinSilent()
})

const setupAuthentication = store => {
    loadUser(store, userManager)
}

export {
    CallbackComponent,
    OidcProvider,
    reducer,
    userManager
}

export default setupAuthentication