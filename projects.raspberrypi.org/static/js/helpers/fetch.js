import config from 'helpers/config'

const json = response => {
    return response.json()
}

const status = response => {
    if (
        response.status >= config.okStatus &&
        response.status < config.redirectedStatus
    ) {
        return response
    }

    const err = new Error(response.statusText)
    err.status = response.status
    err.response = response
    throw err
}

export {
    json,
    status
}