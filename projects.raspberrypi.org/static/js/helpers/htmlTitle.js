import config from 'helpers/config'

const htmlTitle = titlePrefix => {
    if (titlePrefix) {
        return `${titlePrefix} | ${config.siteTitle}`
    }
    return `${config.siteTitle}`
}

export {
    htmlTitle
}