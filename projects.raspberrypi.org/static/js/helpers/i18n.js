import i18n from 'i18next'
import Backend from 'i18next-xhr-backend'
import languageDetector from 'i18next-browser-languagedetector'
import {
    reactI18nextModule
} from 'react-i18next'
import customDetector from './customDetector'

const lngDetector = new languageDetector()
lngDetector.addDetector(customDetector)

i18n
    .use(Backend)
    .use(lngDetector)
    .use(reactI18nextModule)
    .init({
        fallbackLng: 'en',

        // have a common namespace used around the full app
        ns: ['translations'],
        defaultNS: 'translations',

        debug: process.env.NODE_ENV !== 'production',

        detection: {
            order: ['customDetector'],
        },

        interpolation: {
            escapeValue: false, // not needed for react!!
        },

        react: {
            wait: true,
        },
    })

export default i18n