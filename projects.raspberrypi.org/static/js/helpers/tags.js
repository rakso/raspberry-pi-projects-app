const tagLabel = (t, tag, type) => t([`filter.${type}.options.${tag}`])

const tagUrl = (locale, tag, type) => `/${locale}/projects?${type}[]=${tag}`

const tagsAreValid = tags => tags && typeof tags === 'object'

const tagsLinks = (locale, t, tags, type) => {
    const links = {}

    if (!tagsAreValid(tags)) {
        return links
    }

    tags.map(tag => {
        return (links[tag] = {
            url: tagUrl(locale, tag, type),
            label: tagLabel(t, tag, type),
        })
    })

    return links
}

const tagsList = (locale, t, tags, type) => {
    const list = []

    if (!tagsAreValid(tags)) {
        return list
    }

    tags.map(tag => {
        return list.push(tagLabel(t, tag, type))
    })

    return list
}

const tagsString = (locale, t, tags, type) => {
    if (!tagsAreValid(tags)) {
        return null
    }

    const list = tagsList(locale, t, tags, type)

    return list.join(', ')
}

const tagsStringCombined = (locale, t, tags) => {
    const strings = []

    if (!tagsAreValid(tags)) {
        return null
    }

    Object.entries(tags).map(([key, data]) => {
        if (tagsAreValid(data) && data.length > 0) {
            return strings.push(tagsString(locale, t, data, key))
        }

        return null
    })

    return strings.join(', ')
}

export {
    tagLabel,
    tagUrl,
    tagsAreValid,
    tagsLinks,
    tagsList,
    tagsString,
    tagsStringCombined,
}