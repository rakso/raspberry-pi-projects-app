import 'react-app-polyfill/ie9' // For IE 9-11 support
import 'core-js/es6/array'
import 'core-js/es6/map'
import 'core-js/es6/set'
import 'core-js/es6/symbol'
import 'core-js/es7/array'
import 'core-js/fn/object/values'
import 'raf/polyfill'
import Raven from 'raven-js'
import React from 'react'
import ReactDOM from 'react-dom'
import {
    createStore,
    applyMiddleware,
    compose
} from 'redux'
import {
    Provider
} from 'react-redux'

import reducers from 'redux/reducers/index'
import App from 'App'
import api from 'redux/middleware/api'

import ErrorBoundary from './ErrorBoundary'

import setupAuthentication, {
    userManager,
    OidcProvider,
} from './helpers/authentication'
import './helpers/i18n'
import 'index.css'

import ('helpers/analytics/analytics').then(analytics => analytics.init())

Raven.config(process.env.REACT_APP_SENTRY_DSN).install()

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose // eslint-disable-line  no-underscore-dangle
const store = createStore(reducers, composeEnhancers(applyMiddleware(api)))

if (process.env.REACT_APP_AUTHENTICATION === 'true') {
    setupAuthentication(store)
}

ReactDOM.render( <
    ErrorBoundary >
    <
    Provider store = {
        store
    } >
    <
    OidcProvider userManager = {
        userManager
    }
    store = {
        store
    } >
    <
    App / >
    <
    /OidcProvider> <
    /Provider> <
    /ErrorBoundary>,
    document.getElementById('root'),
)