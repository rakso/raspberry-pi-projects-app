(window.webpackJsonp = window.webpackJsonp || []).push([
    [10], {
        102: function(e, t, n) {
            "use strict";
            var r, a = n(49);
            t.a = function(e) {
                return r || (r = Object(a.a)()).listen(function(t) {
                    var n = e.language,
                        r = t.pathname.split("/")[1];
                    r !== n && e.changeLanguage(r)
                }), r
            }
        },
        11: function(e, t, n) {
            "use strict";
            t.a = {
                okStatus: 200,
                createdStatus: 201,
                deletedStatus: 204,
                redirectedStatus: 300,
                notFoundStatus: 404,
                internalServerErrorStatus: 500,
                siteTitle: "Raspberry Pi Projects",
                stepScrollDuration: 250,
                transitionEnterTimeout: 325,
                transitionExitTimeout: 325,
                creativeCommonsUrl: "https://creativecommons.org/licenses/by-sa/4.0/",
                raspberryPiUrl: "https://www.raspberrypi.org",
                projectCompleteSlug: "complete",
                projectCompleteTitle: "Project Complete",
                reducers: {
                    project: {
                        initialState: {
                            currentLocale: "en",
                            currentStepPosition: null,
                            error: null,
                            project: {},
                            projectFetched: 0,
                            slideDirection: "left",
                            slug: null
                        }
                    },
                    projects: {
                        initialState: {
                            currentLocale: "en",
                            entities: {},
                            links: {},
                            meta: {},
                            loading: !0,
                            error: null,
                            ids: []
                        }
                    }
                },
                scratchSupportedLanguages: ["ab", "am", "ar", "az", "be", "bg", "ca", "ckb", "cs", "cy", "da", "de", "el", "en", "es", "es_419", "et", "eu", "fa", "fi", "fr", "ga", "gd", "gl", "he", "hr", "hu", "id", "is", "it", "ja", "ja_Hira", "ko", "lt", "lv", "mi", "nb", "nl", "nn", "pl", "pt", "pt_br", "ro", "ru", "sk", "sl", "sr", "sv", "th", "tr", "uk", "vi", "zh_cn", "zh_tw", "zu"],
                availableHomePageLanguages: ["en"]
            }
        },
        133: function(e, t, n) {
            "use strict";
            var r = n(1),
                a = n.n(r),
                o = n(137),
                c = n(197);
            t.a = function(e) {
                var t = e.titlePrefix;
                return a.a.createElement(o.Helmet, null, a.a.createElement("title", null, Object(c.a)(t)))
            }
        },
        134: function(e, t, n) {
            "use strict";
            var r = n(46),
                a = (n(385), n(198)),
                o = n(1),
                c = n.n(o),
                i = n(86),
                s = n(16),
                l = n(45),
                u = function(e) {
                    e.preventDefault(), l.e.signinRedirect()
                },
                p = function(e) {
                    e.preventDefault(), l.e.removeUser()
                },
                d = Object(s.c)("translations")(Object(i.b)(function(e) {
                    return {
                        authentication: e.authentication
                    }
                })(function(e) {
                    var t = e.authentication,
                        n = e.t;
                    return c.a.createElement("div", null, !t.user && c.a.createElement("a", {
                        className: "c-site-header__home-link",
                        href: "/login",
                        onClick: u
                    }, n("navigation.login")), t.user && c.a.createElement("div", null, c.a.createElement("span", {
                        className: "c-site-header__home-link"
                    }, t.user.profile.name), c.a.createElement("a", {
                        className: "c-site-header__home-link",
                        href: "/logout",
                        onClick: p
                    }, n("navigation.logout"))))
                })),
                m = n(417),
                f = n(416),
                g = n(203),
                h = n.n(g);
            t.a = Object(s.c)("translations")(function(e) {
                var t = e.locale,
                    n = e.t,
                    o = Object(m.a)(["enableAccountsLogin"]),
                    i = Object(r.a)(o, 1)[0];
                return c.a.createElement("header", {
                    className: "c-site-header",
                    id: "c-site-header",
                    role: "banner"
                }, c.a.createElement("div", {
                    className: "c-site-header__container"
                }, c.a.createElement("a", {
                    className: "c-site-header__rpf-link",
                    href: "https://raspberrypi.org"
                }, c.a.createElement("img", {
                    alt: "",
                    className: "c-site-header__rpf-logomark",
                    src: h.a
                })), "en" !== t && c.a.createElement(f.a, {
                    className: "c-site-header__home-link",
                    to: "/".concat(t)
                }, n("navigation.projects")), "en" === t && c.a.createElement(c.a.Fragment, null, c.a.createElement(f.a, {
                    className: "c-site-header__home-link",
                    to: "/".concat(t)
                }, n("navigation.home")), c.a.createElement(f.a, {
                    className: "c-site-header__nav-link",
                    to: "/".concat(t, "/projects")
                }, n("navigation.projects")), c.a.createElement(f.a, {
                    className: "c-site-header__nav-link",
                    to: "/".concat(t, "/codeclub")
                }, n("navigation.codeclub")), c.a.createElement(f.a, {
                    className: "c-site-header__nav-link",
                    to: "/".concat(t, "/coderdojo")
                }, n("navigation.coderdojo")), c.a.createElement(f.a, {
                    className: "c-site-header__nav-link",
                    to: "/".concat(t, "/jam")
                }, n("navigation.jam"))), "true" === Object({
                    NODE_ENV: "production",
                    PUBLIC_URL: "",
                    REACT_APP_GA_ID: "UA-46270871-8",
                    REACT_APP_SENTRY_DSN: "https://d40346592202445d84f573aae73cff38@sentry.io/182298",
                    REACT_APP_BING_VERIFICATION: "7C9CCDF0A68F98FA2958BF187FB3734F",
                    REACT_APP_GOOGLE_VERIFICATION: "DbEQRCm57B8ygmlhiOw2FWj31XGVinad0t29v0TyAvY",
                    REACT_APP_YANDEX_VERIFICATION: "187b0cd22bb82d98",
                    REACT_APP_API_URL: "https://learning-admin.raspberrypi.org",
                    REACT_APP_TYPEKIT_ID: "nkn4hxx"
                }).REACT_APP_AUTHENTICATION && i.enableAccountsLogin && c.a.createElement(d, null), c.a.createElement("div", {
                    className: "c-site-header__language-selector"
                }, c.a.createElement(a.a, null))))
            })
        },
        14: function(e, t, n) {
            "use strict";
            var r = n(1),
                a = n.n(r),
                o = n(16),
                c = n(67),
                i = n(11);
            n(384);
            t.a = Object(o.c)("translations")(function(e) {
                var t = e.error,
                    n = e.t;
                if (!t) return a.a.createElement("div", null);
                var r = n("no-match.title.text");
                return t.status !== i.a.notFoundStatus && (r = n("error-boundary.message")), a.a.createElement("div", {
                    className: "c-error c-error--".concat(t.status)
                }, a.a.createElement("div", {
                    className: "c-error__wrapper u-clearfix"
                }, a.a.createElement("div", {
                    className: "c-error__image"
                }, a.a.createElement("span", {
                    className: "c-error__screen"
                }, function() {
                    for (var e, t, n = [], r = Math.ceil(10 * Math.random()) + 5, o = 0; o < r; o += 1) e = Math.floor(172 * Math.random()), t = Math.floor(93 * Math.random()), n.push(a.a.createElement("span", {
                        className: "c-error__dialog",
                        key: o,
                        style: {
                            left: e,
                            top: t
                        }
                    }));
                    return n
                }())), a.a.createElement("div", {
                    className: "c-error__body"
                }, a.a.createElement(c.m, {
                    element: "h1"
                }, r), t.status === i.a.notFoundStatus ? "" : a.a.createElement(c.k, {
                    element: "p"
                }, n("no-match.message.notified")), a.a.createElement(c.k, {
                    element: "p"
                }, n("no-match.message.text")), a.a.createElement(c.b, {
                    to: "/"
                }, n("no-match.button.text")))))
            })
        },
        197: function(e, t, n) {
            "use strict";
            n.d(t, "a", function() {
                return a
            });
            var r = n(11),
                a = function(e) {
                    return e ? "".concat(e, " | ").concat(r.a.siteTitle) : "".concat(r.a.siteTitle)
                }
        },
        198: function(e, t, n) {
            "use strict";
            var r = n(8),
                a = n(1),
                o = n.n(a),
                c = n(2),
                i = n.n(c),
                s = (n(386), function(e) {
                    var t = e.changeHandler,
                        n = e.className,
                        r = e.label,
                        a = e.name,
                        c = e.options,
                        s = e.value,
                        l = e.blankText;
                    return o.a.createElement("select", {
                        "aria-label": r,
                        className: i()(n, "c-dropdown"),
                        name: a,
                        onChange: function(e) {
                            return t(e.target.value)
                        },
                        value: s
                    }, Boolean(l) && o.a.createElement("option", null, "-- ", l, " --"), function(e) {
                        return Object.keys(e).map(function(t) {
                            return o.a.createElement("option", {
                                key: t,
                                value: t
                            }, e[t])
                        })
                    }(c))
                }),
                l = n(102),
                u = n(16);
            t.a = Object(u.c)("translations")(function(e) {
                var t = e.i18n,
                    n = e.t,
                    a = e.filter,
                    c = n("language-selector.options", {
                        returnObjects: !0
                    }),
                    i = a ? a(c) : c;
                return o.a.createElement(s, {
                    changeHandler: function(e) {
                        return function(e, t) {
                            if (!t) return null;
                            var n = window.location.pathname.split("/");
                            return n[1] = t, n = n.join("/"), Object(l.a)(e).push({
                                pathname: n
                            }), null
                        }(t, e)
                    },
                    label: n("language-selector.label"),
                    name: "locale",
                    options: Object(r.a)({}, i),
                    value: t.language,
                    blankText: n("language-selector.label")
                })
            })
        },
        203: function(e, t, n) {
            e.exports = n.p + "static/media/logomark.20300533.svg"
        },
        207: function(e, t, n) {
            e.exports = n(401)
        },
        21: function(e, t, n) {
            "use strict";
            var r = n(1),
                a = n.n(r),
                o = n(81),
                c = n(82),
                i = n(84),
                s = n(83),
                l = n(85),
                u = function(e) {
                    function t() {
                        var e, n;
                        Object(o.a)(this, t);
                        for (var r = arguments.length, a = new Array(r), c = 0; c < r; c++) a[c] = arguments[c];
                        return (n = Object(i.a)(this, (e = Object(s.a)(t)).call.apply(e, [this].concat(a)))).state = {
                            waiting: !0
                        }, n
                    }
                    return Object(l.a)(t, e), Object(c.a)(t, [{
                        key: "componentDidMount",
                        value: function() {
                            var e = this;
                            this.timer = setTimeout(function() {
                                e.setState({
                                    waiting: !1
                                })
                            }, this.props.wait)
                        }
                    }, {
                        key: "componentWillUnmount",
                        value: function() {
                            clearTimeout(this.timer)
                        }
                    }, {
                        key: "render",
                        value: function() {
                            return this.state.waiting ? null : this.props.children
                        }
                    }]), t
                }(r.Component);
            u.defaultProps = {
                wait: 250
            };
            var p = u;
            n(383), t.a = function(e) {
                var t = e.display,
                    n = void 0 === t || t,
                    r = e.delay,
                    o = void 0 === r ? 250 : r;
                return a.a.createElement(p, {
                    wait: o
                }, a.a.createElement("div", {
                    className: "computer display-".concat(n)
                }, a.a.createElement("div", {
                    className: "screen"
                })))
            }
        },
        28: function(e, t, n) {
            "use strict";
            n.d(t, "a", function() {
                return i
            }), n.d(t, "c", function() {
                return l
            }), n.d(t, "e", function() {
                return u
            }), n.d(t, "b", function() {
                return p
            }), n.d(t, "d", function() {
                return d
            }), n.d(t, "f", function() {
                return m
            }), n.d(t, "g", function() {
                return f
            }), n.d(t, "h", function() {
                return g
            }), n.d(t, "i", function() {
                return h
            }), n.d(t, "j", function() {
                return b
            }), n.d(t, "k", function() {
                return E
            }), n.d(t, "l", function() {
                return _
            }), n.d(t, "m", function() {
                return j
            }), n.d(t, "n", function() {
                return s
            });
            var r = n(22),
                a = n.n(r),
                o = n(21),
                c = n(14),
                i = a()({
                    loader: function() {
                        return n.e(8).then(n.bind(null, 405))
                    },
                    error: c.a,
                    loading: o.a
                }),
                s = a()({
                    loader: function() {
                        return n.e(9).then(n.bind(null, 406))
                    },
                    error: c.a,
                    loading: o.a
                }),
                l = a()({
                    loader: function() {
                        return n.e(0).then(n.bind(null, 407))
                    },
                    error: c.a,
                    loading: o.a
                }),
                u = a()({
                    loader: function() {
                        return n.e(1).then(n.bind(null, 408))
                    },
                    error: c.a,
                    loading: o.a
                }),
                p = a()({
                    loader: function() {
                        return n.e(0).then(n.bind(null, 403))
                    },
                    error: c.a,
                    loading: o.a
                }),
                d = a()({
                    loader: function() {
                        return n.e(1).then(n.bind(null, 404))
                    },
                    error: c.a,
                    loading: o.a
                }),
                m = a()({
                    loader: function() {
                        return Promise.all([n.e(14), n.e(2)]).then(n.bind(null, 411))
                    },
                    error: c.a,
                    loading: o.a
                }),
                f = a()({
                    loader: function() {
                        return n.e(3).then(n.bind(null, 409))
                    },
                    error: c.a,
                    loading: o.a
                }),
                g = a()({
                    loader: function() {
                        return n.e(5).then(n.bind(null, 413))
                    },
                    error: c.a,
                    loading: o.a
                }),
                h = a()({
                    loader: function() {
                        return n.e(4).then(n.bind(null, 402))
                    },
                    error: c.a,
                    loading: o.a
                }),
                E = a()({
                    loader: function() {
                        return Promise.all([n.e(13), n.e(6)]).then(n.bind(null, 410))
                    },
                    error: c.a,
                    loading: o.a
                }),
                b = a()({
                    loader: function() {
                        return n.e(7).then(n.bind(null, 415))
                    },
                    error: c.a,
                    loading: o.a
                }),
                _ = a()({
                    loader: function() {
                        return n.e(7).then(n.bind(null, 414))
                    },
                    error: c.a,
                    loading: o.a
                }),
                j = a()({
                    loader: function() {
                        return n.e(8).then(n.bind(null, 412))
                    },
                    error: c.a,
                    loading: o.a
                })
        },
        383: function(e, t, n) {},
        384: function(e, t, n) {},
        385: function(e, t, n) {},
        386: function(e, t, n) {},
        400: function(e, t, n) {},
        401: function(e, t, n) {
            "use strict";
            n.r(t);
            n(208), n(310), n(339), n(343), n(345), n(350), n(354), n(357);
            var r = n(63),
                a = n.n(r),
                o = n(1),
                c = n.n(o),
                i = n(136),
                s = n.n(i),
                l = n(48),
                u = n(86),
                p = n(45),
                d = n(135),
                m = n(57),
                f = n(8),
                g = n(11),
                h = n(6),
                E = function(e, t) {
                    return Object.assign({}, e, t)
                };

            function b(e) {
                var t = function(e, t) {
                    if ("object" !== typeof e || null === e) return e;
                    var n = e[Symbol.toPrimitive];
                    if (void 0 !== n) {
                        var r = n.call(e, t || "default");
                        if ("object" !== typeof r) return r;
                        throw new TypeError("@@toPrimitive must return a primitive value.")
                    }
                    return ("string" === t ? String : Number)(e)
                }(e, "string");
                return "symbol" === typeof t ? t : String(t)
            }
            var _ = g.a.reducers.projects.initialState;
            var j = {
                    title: "",
                    description: " ",
                    projectsIds: []
                },
                v = Object(f.a)({
                    error: null,
                    loading: !0,
                    currentLocale: "en"
                }, j);
            var O = n(64),
                A = n(87),
                y = g.a.reducers.project.initialState;
            var T = g.a.reducers.projects.initialState;
            var P = g.a.reducers.projects.initialState;
            var C = Object(l.c)({
                    favourites: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : _,
                            t = arguments.length > 1 ? arguments[1] : void 0;
                        switch (t.type) {
                            case h.b:
                                return E(e, {
                                    currentLocale: t.currentLocale || e.currentLocale,
                                    loading: !0
                                });
                            case h.h:
                                var n = t.response.project || {},
                                    r = Object.values(n).map(function(e) {
                                        return e.id
                                    });
                                return E(e, Object(f.a)({}, e, {
                                    entities: n,
                                    ids: r,
                                    loading: !1
                                }));
                            case h.g:
                                return E(e, {
                                    entities: {},
                                    links: {},
                                    meta: {},
                                    loading: !1,
                                    error: t.error
                                });
                            case h.o:
                            case h.m:
                                return E(e, Object(f.a)({}, e, {
                                    loading: !0
                                }));
                            case h.r:
                                var a = e.ids.slice();
                                return a.includes(t.response.id) || a.push(t.response.id.toString()), E(e, Object(f.a)({}, e, {
                                    entities: Object(f.a)({}, e.entities, Object(m.a)({}, t.response.id, Object(f.a)({}, e.entities[t.response.id]))),
                                    loading: !1,
                                    ids: a
                                }));
                            case h.q:
                                var o = e.ids.filter(function(e) {
                                        return e !== t.response.id.toString()
                                    }),
                                    c = e.entities,
                                    i = (c[t.response.id], Object(d.a)(c, [t.response.id].map(b)));
                                return E(e, Object(f.a)({}, e, {
                                    entities: i,
                                    loading: !1,
                                    ids: o
                                }));
                            case h.n:
                                return E(e, Object(f.a)({}, e, {
                                    error: t.error,
                                    loading: !1
                                }));
                            default:
                                return e
                        }
                    },
                    pathway: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : v,
                            t = arguments.length > 1 ? arguments[1] : void 0;
                        switch (t.type) {
                            case h.c:
                                return Object.assign({}, e, Object(f.a)({}, j, {
                                    error: null,
                                    loading: !0,
                                    currentLocale: t.currentLocale || e.currentLocale
                                }));
                            case h.i:
                                var n = t.response || {};
                                return Object.assign({}, e, Object(f.a)({}, e, n, {
                                    loading: !1
                                }));
                            case h.g:
                                return Object.assign({}, e, Object(f.a)({}, e, j, {
                                    error: t.error,
                                    loading: !1
                                }));
                            default:
                                return e
                        }
                    },
                    progress: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : P,
                            t = arguments.length > 1 ? arguments[1] : void 0;
                        switch (t.type) {
                            case h.d:
                                return E(e, {
                                    currentLocale: t.currentLocale || e.currentLocale,
                                    loading: !0
                                });
                            case h.j:
                                var n = t.response.project || {},
                                    r = Object.values(n).map(function(e) {
                                        return e.id
                                    });
                                return E(e, Object(f.a)({}, e, {
                                    entities: n,
                                    ids: r,
                                    loading: !1
                                }));
                            case h.g:
                                return E(e, Object(f.a)({}, e, {
                                    entities: {},
                                    error: t.error,
                                    loading: !1
                                }));
                            case h.p:
                                return E(e, Object(f.a)({}, e, {
                                    loading: !0
                                }));
                            case h.s:
                                var a = e.ids.slice();
                                return a.includes(t.response.projectId) || a.push(t.response.projectId.toString()), E(e, Object(f.a)({}, e, {
                                    entities: Object(f.a)({}, e.entities, Object(m.a)({}, t.response.projectId, Object(f.a)({}, e.entities[t.response.projectId], {
                                        currentStep: t.response.step
                                    }))),
                                    loading: !1,
                                    ids: a
                                }));
                            case h.n:
                                return E(e, Object(f.a)({}, e, {
                                    error: t.error,
                                    loading: !1
                                }));
                            default:
                                return e
                        }
                    },
                    project: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : y,
                            t = arguments.length > 1 ? arguments[1] : void 0;
                        switch (t.type) {
                            case h.f:
                                var n = Object(A.f)(t.currentStepPosition);
                                return Object.assign({}, e, {
                                    currentLocale: t.currentLocale || e.currentLocale,
                                    currentStepPosition: n,
                                    error: null,
                                    project: t.cachedProject ? t.cachedProject.content : {},
                                    projectFetched: 0,
                                    slideDirection: "left",
                                    slug: t.slug
                                });
                            case h.k:
                                var r = Object.assign({}, t.response.content, {
                                    id: t.response.id,
                                    locale: t.response.locale
                                }) || {};
                                return Object.assign({}, e, Object(f.a)({}, e, {
                                    nextStepTitle: r.steps[e.currentStepPosition] ? r.steps[e.currentStepPosition].title : O.a.t("project.steps.navigation.whats-next"),
                                    project: r,
                                    projectFetched: 1
                                }));
                            case h.g:
                                var a = Object.assign({}, t.error.body.content, {
                                    id: t.error.body.id,
                                    availableLocales: t.error.body.availableLocales
                                }) || {};
                                return Object.assign({}, e, Object(f.a)({}, e, {
                                    currentStepPosition: null,
                                    error: t.error,
                                    project: a,
                                    projectFetched: t.error.body.availableLocales ? 1 : -1,
                                    slug: null
                                }));
                            case h.t:
                                var o = Object(A.f)(t.currentStepPosition),
                                    c = e.project.steps && e.project.steps[o] ? e.project.steps[o].title : O.a.t("project.steps.navigation.whats-next"),
                                    i = Object(A.g)(t.currentStepPosition, e.currentStepPosition);
                                return Object.assign({}, e, Object(f.a)({}, e, {
                                    currentStepPosition: o,
                                    nextStepTitle: c,
                                    slideDirection: i
                                }));
                            default:
                                return e
                        }
                    },
                    projects: function() {
                        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : T,
                            t = arguments.length > 1 ? arguments[1] : void 0;
                        switch (t.type) {
                            case h.e:
                                return E(e, {
                                    currentLocale: t.currentLocale || e.currentLocale,
                                    loading: !0
                                });
                            case h.l:
                                return E(e, {
                                    entities: t.response.projects || {},
                                    links: t.response.links,
                                    meta: t.response.meta,
                                    loading: !1
                                });
                            case h.g:
                                return E(e, {
                                    entities: {},
                                    links: {},
                                    meta: {},
                                    loading: !1,
                                    error: t.error
                                });
                            default:
                                return e
                        }
                    },
                    authentication: p.d
                }),
                S = n(2),
                R = n.n(S),
                w = n(16),
                I = n(133),
                N = n(46),
                k = n(417),
                L = n(418),
                F = n(420),
                x = n(421),
                D = n(419),
                U = n(28),
                V = n(102),
                G = n(14),
                B = n(134),
                Y = ["microbit-game-controller", "n-days-of-christmas", "sweet-shop-reaction-game"],
                H = function() {
                    return Y.map(function(e, t) {
                        return c.a.createElement(D.a, {
                            exact: !0,
                            path: "/:locale/projects/".concat(e),
                            key: t,
                            component: function() {
                                return window.location = "https://github.com/raspberrypilearning/".concat(e)
                            }
                        })
                    })
                },
                M = [{
                    from: "/en/projects/rpi-python-build-an-octapi",
                    to: "/en/projects/build-an-octapi"
                }, {
                    from: "/en/projects/rpi-python-tweeting-babbage",
                    to: "/en/projects/tweeting-babbage"
                }, {
                    from: "/en/projects/rpi-python-tweeting-babbage",
                    to: "/en/projects/tweeting-babbage"
                }, {
                    from: "/en/projects/rpi-python-google-aiy",
                    to: "/en/projects/google-voice-aiy"
                }, {
                    from: "/en/projects/rpi-python-gpio-music-box",
                    to: "/en/projects/gpio-music-box"
                }, {
                    from: "/en/projects/rpi-python-hamster-party-cam",
                    to: "/en/projects/hamster-party-cam"
                }, {
                    from: "/en/projects/rpi-python-octapi-public-key-cryptography",
                    to: "/en/projects/octapi-public-key-cryptography"
                }, {
                    from: "/en/projects/rpi-python-parent-detector",
                    to: "/en/projects/parent-detector"
                }, {
                    from: "/en/projects/rpi-python-whoopi-cushion",
                    to: "/en/projects/whoopi-cushion"
                }, {
                    from: "/en/projects/generic-javascript-cat-meme-generator",
                    to: "/en/projects/cat-meme-generator"
                }, {
                    from: "/en/projects/generic-scratch-space-maze",
                    to: "/en/projects/space-maze"
                }, {
                    from: "/en/projects/rpi-python-people-in-space-indicator",
                    to: "/en/projects/people-in-space-indicator"
                }],
                Q = function() {
                    return M.map(function(e) {
                        return c.a.createElement(L.a, {
                            key: e.from,
                            from: e.from,
                            to: e.to
                        })
                    })
                },
                X = [],
                q = function() {
                    return X.map(function(e) {
                        return c.a.createElement(D.a, {
                            exact: !0,
                            path: e.from,
                            key: e.from,
                            component: function() {
                                return window.location = e.to
                            }
                        })
                    })
                },
                W = Object(w.c)("translations")(function(e) {
                    var t = e.t;
                    return c.a.createElement("div", null, c.a.createElement(B.a, {
                        locale: "en"
                    }), c.a.createElement(G.a, {
                        error: {
                            status: g.a.notFoundStatus,
                            message: t("no-match.title.text")
                        }
                    }))
                }),
                z = function(e, t, n) {
                    return n ? e("enableAccountsLogin", !0, {
                        path: "/"
                    }) : t("enableAccountsLogin", {
                        path: "/"
                    }), c.a.createElement(L.a, {
                        to: {
                            pathname: "/"
                        }
                    })
                },
                J = Object(w.c)("translations")(function(e) {
                    var t = e.i18n,
                        n = Object(k.a)(["enableAccountsLogin"]),
                        r = Object(N.a)(n, 3),
                        a = (r[0], r[1]),
                        o = r[2];
                    return c.a.createElement(F.a, {
                        history: Object(V.a)(t)
                    }, c.a.createElement(x.a, null, c.a.createElement(D.a, {
                        exact: !0,
                        path: "/rpi/silent_renew",
                        component: U.n
                    }), Q(), H(), q(), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/",
                        render: function() {
                            return c.a.createElement(L.a, {
                                to: "/en/"
                            })
                        }
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: ["/:locale/enable-accounts", "/enable-accounts"],
                        render: function() {
                            return z(a, o, !0)
                        }
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: ["/:locale/disable-accounts", "/disable-accounts"],
                        render: function() {
                            return z(a, o, !1)
                        }
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/:locale",
                        render: function(e) {
                            return function(e) {
                                return g.a.availableHomePageLanguages.includes(e.match.params.locale) ? c.a.createElement(U.f, e) : c.a.createElement(L.a, {
                                    to: {
                                        pathname: "/".concat(e.match.params.locale, "/projects")
                                    }
                                })
                            }(e)
                        }
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/:locale/codeclub/",
                        component: U.c
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/:locale/coderdojo/",
                        component: U.e
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/:locale/jam",
                        render: function(e) {
                            var t = e.match.params.locale;
                            return c.a.createElement(L.a, {
                                to: "/".concat(t, "/jam/jam")
                            })
                        }
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/:locale/projects",
                        component: U.l
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/:locale/projects/:progress(progress|finished)",
                        component: U.j
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/:locale/projects/:slug/print",
                        component: U.m
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/:locale/projects/:slug/complete",
                        component: U.k
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/:locale/projects/:slug/:position?",
                        component: U.k
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/:locale/:pathwayType/:slug",
                        component: U.h
                    }), c.a.createElement(D.a, {
                        exact: !0,
                        path: "/rpi/callback",
                        component: U.a
                    }), c.a.createElement(D.a, {
                        component: W
                    })))
                }),
                K = Object(w.c)("translations")(function(e) {
                    var t = e.i18n,
                        n = R()("c-i18n-root", {
                            "c-i18n-root--rtl": "rtl" === t.dir()
                        });
                    return c.a.createElement("div", {
                        className: n,
                        dir: t.dir()
                    }, c.a.createElement(I.a, null), c.a.createElement(J, null))
                }),
                Z = function(e) {
                    return e.json()
                },
                $ = function(e) {
                    if (e.status >= g.a.okStatus && e.status < g.a.redirectedStatus) return e;
                    var t = new Error(e.statusText);
                    throw t.status = e.status, t.response = e, t
                },
                ee = "".concat("https://learning-admin.raspberrypi.org", "/api/v1"),
                te = function(e) {
                    var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                    if (!e.data) return null;
                    if (!Array.isArray(e.data)) return function(e) {
                        var t = Object(f.a)({}, e.data.attributes);
                        return e.data.relationships && Object.keys(e.data.relationships).map(function(n) {
                            return t["".concat(n, "Ids")] = e.data.relationships[n].data.map(function(e) {
                                return e.id
                            })
                        }), t
                    }(e);
                    var n = {
                        meta: {},
                        links: {}
                    };
                    return e.data && e.data.forEach(function(e, r) {
                        n[e.type] = n[e.type] || {}, e.attributes.id = e.id, t ? n[e.type][e.id] = e.attributes : n[e.type][r] = e.attributes
                    }), e.links && (n.links = e.links), n
                },
                ne = n(81),
                re = n(82),
                ae = n(84),
                oe = n(83),
                ce = n(85),
                ie = function(e) {
                    function t(e) {
                        var n;
                        return Object(ne.a)(this, t), (n = Object(ae.a)(this, Object(oe.a)(t).call(this, e))).state = {
                            hasError: !1
                        }, n
                    }
                    return Object(ce.a)(t, e), Object(re.a)(t, [{
                        key: "componentDidCatch",
                        value: function(e, t) {
                            this.setState({
                                hasError: !0
                            }), a.a.captureException(e, {
                                extra: t
                            })
                        }
                    }, {
                        key: "render",
                        value: function() {
                            return this.state.hasError ? c.a.createElement(G.a, {
                                error: {
                                    status: 500,
                                    message: this.props.t("error-boundary.message")
                                }
                            }) : this.props.children
                        }
                    }]), t
                }(o.Component),
                se = Object(w.c)("translations")(ie),
                le = n(204),
                ue = n.n(le),
                pe = n(205),
                de = {
                    name: "customDetector",
                    lookup: function() {
                        return window.location.pathname.split("/")[1]
                    }
                },
                me = new(n.n(pe).a);
            me.addDetector(de), O.a.use(ue.a).use(me).use(w.b).init({
                fallbackLng: "en",
                ns: ["translations"],
                defaultNS: "translations",
                debug: !1,
                detection: {
                    order: ["customDetector"]
                },
                interpolation: {
                    escapeValue: !1
                },
                react: {
                    wait: !0
                }
            });
            O.a, n(400);
            n.e(15).then(n.bind(null, 556)).then(function(e) {
                return e.init()
            }), a.a.config("https://d40346592202445d84f573aae73cff38@sentry.io/182298").install();
            var fe = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || l.d,
                ge = Object(l.e)(C, fe(Object(l.a)(function() {
                    return function(e) {
                        return function(t) {
                            var n = t[h.a];
                            if ("undefined" === typeof n) return e(t);
                            var r = n.types,
                                o = n.method,
                                c = n.endpoint,
                                i = n.authToken,
                                s = n.mapIds,
                                l = n.data;
                            if (!Array.isArray(r) || 3 !== r.length) throw new Error("Expected an array of three action types.");
                            if ("string" !== typeof c) throw new Error("Specify a string endpoint URL.");
                            var u = function(e) {
                                    var n = Object.assign({}, t, e);
                                    return delete n[h.a], n
                                },
                                p = Object(N.a)(r, 3),
                                d = p[0],
                                m = p[1],
                                E = p[2];
                            return e(u(Object(f.a)({
                                    type: d
                                }, n))),
                                function(e, t) {
                                    var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : null,
                                        r = arguments.length > 3 && void 0 !== arguments[3] && arguments[3],
                                        o = arguments.length > 4 && void 0 !== arguments[4] ? arguments[4] : {},
                                        c = -1 === t.indexOf(ee) ? "".concat(ee).concat(t) : t,
                                        i = {
                                            method: e
                                        };
                                    n && (i.headers = {
                                        Authorization: n
                                    });
                                    var s = {};
                                    return "GET" === e ? s = fetch(c, i).then($).then(Z).then(function(e) {
                                        return Object.assign({}, te(e, r))
                                    }) : (i.headers["Content-Type"] = "application/json", i.body = JSON.stringify(o), s = fetch(c, i).then(function() {
                                        return o
                                    })), s.catch(function(e) {
                                        return e.status !== g.a.notFoundStatus && a.a.captureMessage("API Error", {
                                            extra: {
                                                error: e
                                            }
                                        }), e.response.json().then(function(t) {
                                            return Promise.reject({
                                                message: e.message,
                                                status: e.status,
                                                body: Object.assign({}, te(t))
                                            })
                                        })
                                    })
                                }(o, c, i, s, l).then(function(t) {
                                    return e(u({
                                        response: t,
                                        type: m
                                    }))
                                }, function(t) {
                                    return e(u({
                                        type: E,
                                        error: t || {
                                            message: "Something went wrong",
                                            status: 500
                                        }
                                    }))
                                })
                        }
                    }
                })));
            "true" === Object({
                NODE_ENV: "production",
                PUBLIC_URL: "",
                REACT_APP_GA_ID: "UA-46270871-8",
                REACT_APP_SENTRY_DSN: "https://d40346592202445d84f573aae73cff38@sentry.io/182298",
                REACT_APP_BING_VERIFICATION: "7C9CCDF0A68F98FA2958BF187FB3734F",
                REACT_APP_GOOGLE_VERIFICATION: "DbEQRCm57B8ygmlhiOw2FWj31XGVinad0t29v0TyAvY",
                REACT_APP_YANDEX_VERIFICATION: "187b0cd22bb82d98",
                REACT_APP_API_URL: "https://learning-admin.raspberrypi.org",
                REACT_APP_TYPEKIT_ID: "nkn4hxx"
            }).REACT_APP_AUTHENTICATION && Object(p.c)(ge), s.a.render(c.a.createElement(se, null, c.a.createElement(u.a, {
                store: ge
            }, c.a.createElement(p.b, {
                userManager: p.e,
                store: ge
            }, c.a.createElement(K, null)))), document.getElementById("root"))
        },
        45: function(e, t, n) {
            "use strict";
            n.d(t, "e", function() {
                return i
            });
            var r = n(62);
            n.d(t, "a", function() {
                return r.CallbackComponent
            }), n.d(t, "b", function() {
                return r.OidcProvider
            }), n.d(t, "d", function() {
                return r.reducer
            });
            var a = n(101),
                o = "".concat(window.location.protocol, "//").concat(window.location.hostname).concat(window.location.port ? ":".concat(window.location.port) : ""),
                c = {
                    client_id: Object({
                        NODE_ENV: "production",
                        PUBLIC_URL: "",
                        REACT_APP_GA_ID: "UA-46270871-8",
                        REACT_APP_SENTRY_DSN: "https://d40346592202445d84f573aae73cff38@sentry.io/182298",
                        REACT_APP_BING_VERIFICATION: "7C9CCDF0A68F98FA2958BF187FB3734F",
                        REACT_APP_GOOGLE_VERIFICATION: "DbEQRCm57B8ygmlhiOw2FWj31XGVinad0t29v0TyAvY",
                        REACT_APP_YANDEX_VERIFICATION: "187b0cd22bb82d98",
                        REACT_APP_API_URL: "https://learning-admin.raspberrypi.org",
                        REACT_APP_TYPEKIT_ID: "nkn4hxx"
                    }).REACT_APP_AUTHENTICATION_CLIENT_ID,
                    redirect_uri: "".concat(o, "/rpi/callback"),
                    response_type: "id_token token",
                    scope: "openid profile force-consent",
                    authority: Object({
                        NODE_ENV: "production",
                        PUBLIC_URL: "",
                        REACT_APP_GA_ID: "UA-46270871-8",
                        REACT_APP_SENTRY_DSN: "https://d40346592202445d84f573aae73cff38@sentry.io/182298",
                        REACT_APP_BING_VERIFICATION: "7C9CCDF0A68F98FA2958BF187FB3734F",
                        REACT_APP_GOOGLE_VERIFICATION: "DbEQRCm57B8ygmlhiOw2FWj31XGVinad0t29v0TyAvY",
                        REACT_APP_YANDEX_VERIFICATION: "187b0cd22bb82d98",
                        REACT_APP_API_URL: "https://learning-admin.raspberrypi.org",
                        REACT_APP_TYPEKIT_ID: "nkn4hxx"
                    }).REACT_APP_AUTHENTICATION_URL,
                    silent_redirect_uri: "".concat(o, "/rpi/silent_renew"),
                    automaticSilentRenew: !0,
                    filterProtocolClaims: !1,
                    loadUserInfo: !1,
                    monitorSession: !1,
                    userStore: new a.WebStorageStateStore({
                        store: window.localStorage
                    }),
                    extraQueryParams: {
                        brand: "projects"
                    }
                },
                i = Object(r.createUserManager)(c);
            i.events.addAccessTokenExpired(function() {
                i.signinSilent()
            });
            t.c = function(e) {
                Object(r.loadUser)(e, i)
            }
        },
        6: function(e, t, n) {
            "use strict";
            n.d(t, "a", function() {
                return r
            }), n.d(t, "b", function() {
                return a
            }), n.d(t, "c", function() {
                return o
            }), n.d(t, "d", function() {
                return c
            }), n.d(t, "f", function() {
                return i
            }), n.d(t, "e", function() {
                return s
            }), n.d(t, "g", function() {
                return l
            }), n.d(t, "h", function() {
                return u
            }), n.d(t, "i", function() {
                return p
            }), n.d(t, "j", function() {
                return d
            }), n.d(t, "k", function() {
                return m
            }), n.d(t, "l", function() {
                return f
            }), n.d(t, "n", function() {
                return g
            }), n.d(t, "o", function() {
                return h
            }), n.d(t, "r", function() {
                return E
            }), n.d(t, "m", function() {
                return b
            }), n.d(t, "q", function() {
                return _
            }), n.d(t, "p", function() {
                return j
            }), n.d(t, "s", function() {
                return v
            }), n.d(t, "t", function() {
                return O
            });
            var r = "CALL_API",
                a = "FETCH_FAVOURITES_REQUEST",
                o = "FETCH_PATHWAY_REQUEST",
                c = "FETCH_PROGRESS_REQUEST",
                i = "FETCH_PROJECT_REQUEST",
                s = "FETCH_PROJECTS_REQUEST",
                l = "RECEIVE_ERROR",
                u = "RECEIVE_FAVOURITES",
                p = "RECEIVE_PATHWAY",
                d = "RECEIVE_PROGRESS",
                m = "RECEIVE_PROJECT",
                f = "RECEIVE_PROJECTS",
                g = "SEND_ERROR",
                h = "SEND_FAVOURITES_REQUEST",
                E = "SENT_FAVOURITES_OK",
                b = "SEND_DELETE_FAVOURITES_REQUEST",
                _ = "SENT_DELETE_FAVOURITES_OK",
                j = "SEND_PROGRESS_REQUEST",
                v = "SENT_PROGRESS_OK",
                O = "SET_PROJECT_STEP"
        },
        87: function(e, t, n) {
            "use strict";
            n.d(t, "a", function() {
                return u
            }), n.d(t, "b", function() {
                return l
            }), n.d(t, "c", function() {
                return c
            }), n.d(t, "d", function() {
                return p
            }), n.d(t, "e", function() {
                return d
            }), n.d(t, "f", function() {
                return m
            }), n.d(t, "g", function() {
                return f
            });
            var r = n(46),
                a = n(11),
                o = n(371),
                c = function(e) {
                    return "undefined" !== typeof e.challenge && e.challenge
                },
                i = function(e) {
                    return e.classList.contains("js-project-panel--initialise-swiper") || e.classList.contains("js-project-panel--swiper-initialised")
                },
                s = function(e) {
                    var t = e.target;
                    if (t.classList.contains("js-project-panel__toggle")) {
                        t.classList.toggle("c-project-panel__heading--has-close-icon");
                        var n = t.nextElementSibling;
                        n.classList.toggle("u-hidden"), i(n) && function(e) {
                            var t = e.querySelector(".c-project-panel__swiper");
                            null !== t && (new o(t, {
                                a11y: !0,
                                containerModifierClass: "c-project-panel__swiper--",
                                navigation: {
                                    disabledClass: "c-project-panel__swiper-button--disabled",
                                    nextEl: t.querySelector(".c-project-panel__swiper-button--next"),
                                    prevEl: t.querySelector(".c-project-panel__swiper-button--prev")
                                },
                                pagination: {
                                    clickable: !0,
                                    bulletActiveClass: "c-project-panel__swiper-bullet--active",
                                    bulletClass: "c-project-panel__swiper-bullet",
                                    el: t.querySelector(".c-project-panel__swiper-pagination"),
                                    hideOnClick: !1,
                                    type: "bullets"
                                },
                                setWrapperSize: !0,
                                slideClass: "c-project-panel__swiper-slide",
                                spaceBetween: 80,
                                wrapperClass: "c-project-panel__swiper-wrapper"
                            }), e.classList.remove("js-project-panel--initialise-swiper"), e.classList.add("js-project-panel--swiper-initialised"))
                        }(n), g(e, i(n))
                    }
                },
                l = function() {
                    Object.entries({
                        ".language-blocks": "scratch2",
                        ".language-blocks3": "scratch3"
                    }).forEach(function(e) {
                        var t = Object(r.a)(e, 2),
                            n = t[0],
                            o = t[1];
                        scratchblocks.renderMatching(n, {
                            languages: a.a.scratchSupportedLanguages,
                            style: o
                        })
                    })
                },
                u = function() {
                    var e = ['pre code:not([class*="language-blocks"])', 'code[class*="language-"]:not([class*="language-blocks"])'].join(", "),
                        t = document.querySelectorAll(e);
                    if (t.length > 0) {
                        var n = !0,
                            r = !1,
                            a = void 0;
                        try {
                            for (var o, c = t[Symbol.iterator](); !(n = (o = c.next()).done); n = !0) {
                                var i = o.value;
                                Prism.highlightElement(i)
                            }
                        } catch (s) {
                            r = !0, a = s
                        } finally {
                            try {
                                n || null == c.return || c.return()
                            } finally {
                                if (r) throw a
                            }
                        }
                    }
                },
                p = function() {
                    var e = document.querySelectorAll(".scratch-preview iframe"),
                        t = !0,
                        n = !1,
                        r = void 0;
                    try {
                        for (var a, o = e[Symbol.iterator](); !(t = (a = o.next()).done); t = !0) {
                            var c = a.value;
                            c.parentNode.removeChild(c)
                        }
                    } catch (i) {
                        n = !0, r = i
                    } finally {
                        try {
                            t || null == o.return || o.return()
                        } finally {
                            if (n) throw r
                        }
                    }
                },
                d = function() {
                    l(), u(),
                        function() {
                            var e = document.querySelector(".c-project__content");
                            null !== e && e.addEventListener("click", s)
                        }()
                },
                m = function(e) {
                    return e ? parseInt(e, 10) : 1
                },
                f = function(e, t) {
                    return e > t ? "left" : "right"
                },
                g = function(e, t) {
                    var n = e.target.textContent.trim();
                    window.ga("send", {
                        hitType: "event",
                        eventCategory: "Project ".concat(t ? "hint" : "ingredient"),
                        eventAction: "Clicked ".concat(t ? "hint" : "ingredient ".concat(n)),
                        eventLabel: e.target.baseURI
                    })
                }
        }
    },
    [
        [207, 11, 12]
    ]
]);
//# sourceMappingURL=main.8bc42423.chunk.js.map