import {
    FETCH_FAVOURITES_REQUEST,
    RECEIVE_ERROR,
    RECEIVE_FAVOURITES,
    SEND_ERROR,
    SEND_FAVOURITES_REQUEST,
    SENT_FAVOURITES_OK,
    SEND_DELETE_FAVOURITES_REQUEST,
    SENT_DELETE_FAVOURITES_OK,
} from 'redux/constants/ActionTypes'

import {
    createApiAction
} from './ApiActions'

// API actions
export const fetchFavourites = (currentLocale, authToken) => {
    return createApiAction({
        endpoint: `/${currentLocale}/favourites`,
        types: [FETCH_FAVOURITES_REQUEST, RECEIVE_FAVOURITES, RECEIVE_ERROR],
        mapIds: true,
        currentLocale,
        authToken,
    })
}

export const addFavourite = (currentLocale, authToken, projectId) => {
    return createApiAction({
        endpoint: `/${currentLocale}/favourites`,
        types: [SEND_FAVOURITES_REQUEST, SENT_FAVOURITES_OK, SEND_ERROR],
        method: 'POST',
        data: {
            id: projectId,
        },
        currentLocale,
        authToken,
    })
}

export const removeFavourite = (currentLocale, authToken, projectId) => {
    return createApiAction({
        endpoint: `/${currentLocale}/favourites/${projectId}`,
        types: [
            SEND_DELETE_FAVOURITES_REQUEST,
            SENT_DELETE_FAVOURITES_OK,
            SEND_ERROR,
        ],
        method: 'DELETE',
        data: {
            id: projectId,
        },
        currentLocale,
        authToken,
    })
}