import {
    FETCH_PROGRESS_REQUEST,
    RECEIVE_ERROR,
    RECEIVE_PROGRESS,
    SEND_ERROR,
    SEND_PROGRESS_REQUEST,
    SENT_PROGRESS_OK,
} from 'redux/constants/ActionTypes'

import {
    createApiAction
} from './ApiActions'

// API actions
export const fetchProgress = (currentLocale, authToken) => {
    return createApiAction({
        endpoint: `/${currentLocale}/progress`,
        types: [FETCH_PROGRESS_REQUEST, RECEIVE_PROGRESS, RECEIVE_ERROR],
        currentLocale,
        authToken,
    })
}

export const createProgress = (
    currentLocale,
    authToken,
    projectId,
    slug,
    step,
) => {
    return createApiAction({
        endpoint: `/${currentLocale}/progress`,
        types: [SEND_PROGRESS_REQUEST, SENT_PROGRESS_OK, SEND_ERROR],
        method: 'POST',
        data: {
            projectId,
            id: slug,
            step,
        },
        currentLocale,
        authToken,
    })
}

export const updateProgress = (
    currentLocale,
    authToken,
    projectId,
    slug,
    step,
) => {
    return createApiAction({
        endpoint: `/${currentLocale}/progress/${slug}`,
        types: [SEND_PROGRESS_REQUEST, SENT_PROGRESS_OK, SEND_ERROR],
        method: 'PUT',
        data: {
            projectId,
            id: slug,
            step,
        },
        currentLocale,
        authToken,
    })
}