import {
    FETCH_PROJECT_REQUEST,
    FETCH_PROJECTS_REQUEST,
    RECEIVE_ERROR,
    RECEIVE_PROJECT,
    RECEIVE_PROJECTS,
    SET_PROJECT_STEP,
} from 'redux/constants/ActionTypes'

import {
    createApiAction
} from './ApiActions'

// API actions
export const fetchProjects = (currentLocale, filterString) => {
    return createApiAction({
        endpoint: `/${currentLocale}/projects${filterString}`,
        types: [FETCH_PROJECTS_REQUEST, RECEIVE_PROJECTS, RECEIVE_ERROR],
        currentLocale,
    })
}

export const fetchProject = ({
    locale,
    slug,
    position
}) => {
    return createApiAction({
        endpoint: `/${locale}/projects/${slug}`,
        types: [FETCH_PROJECT_REQUEST, RECEIVE_PROJECT, RECEIVE_ERROR],
        currentLocale: locale,
        currentStepPosition: position,
        slug,
    })
}

// Standard action - maybe thunk this for async
export const setProjectStep = (currentStepPosition, authToken = null) => {
    return {
        type: SET_PROJECT_STEP,
        currentStepPosition,
        authToken,
    }
}