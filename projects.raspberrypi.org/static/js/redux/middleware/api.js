// Adapted from https://github.com/reactjs/redux/blob/734a2833e801b68cf64464d55ac98e8d75568004/examples/real-world/src/middleware/api.js
import Raven from 'raven-js'
import {
    CALL_API
} from 'redux/constants/ActionTypes'
import {
    json as parseJson,
    status
} from 'helpers/fetch'
import config from 'helpers/config'

const API_ROOT = `${process.env.REACT_APP_API_URL}/api/v1`

const normalizeRelationship = json => {
    const normalized = { ...json.data.attributes
    }
    if (json.data.relationships) {
        Object.keys(json.data.relationships).map(type => {
            return (normalized[`${type}Ids`] = json.data.relationships[type].data.map(
                entity => entity.id,
            ))
        })
    }

    return normalized
}

const normalize = (json, mapIds = false) => {
    if (!json.data) {
        return null
    }
    if (!Array.isArray(json.data)) {
        return normalizeRelationship(json)
    }
    const normalized = {}
    normalized.meta = {}
    normalized.links = {}
    if (json.data) {
        json.data.forEach((elem, ind) => {
            normalized[elem.type] = normalized[elem.type] || {}
            elem.attributes.id = elem.id
            if (mapIds) {
                normalized[elem.type][elem.id] = elem.attributes
            } else {
                normalized[elem.type][ind] = elem.attributes
            }
        })
    }
    if (json.links) {
        normalized.links = json.links
    }
    return normalized
}

const callApi = (
    method,
    endpoint,
    authToken = null,
    mapIds = false,
    data = {},
) => {
    const fullUrl =
        endpoint.indexOf(API_ROOT) === -1 ? `${API_ROOT}${endpoint}` : endpoint

    const fetchOptions = {
        method
    }

    if (authToken) {
        fetchOptions.headers = {
            Authorization: authToken,
        }
    }

    let response = {}

    if (method === 'GET') {
        response = fetch(fullUrl, fetchOptions)
            .then(status)
            .then(parseJson)
            .then(json => Object.assign({}, normalize(json, mapIds)))
    } else {
        fetchOptions.headers['Content-Type'] = 'application/json'
        fetchOptions.body = JSON.stringify(data)
        response = fetch(fullUrl, fetchOptions).then(() => data)
    }

    return response.catch(error => {
        if (error.status !== config.notFoundStatus) {
            Raven.captureMessage('API Error', {
                extra: {
                    error: error,
                },
            })
        }
        return error.response.json().then(x => {
            return Promise.reject({
                message: error.message,
                status: error.status,
                body: Object.assign({}, normalize(x)),
            })
        })
    })
}

export default () => next => action => {
    const callAPI = action[CALL_API]

    if (typeof callAPI === 'undefined') {
        return next(action)
    }

    const {
        types,
        method,
        endpoint,
        authToken,
        mapIds,
        data
    } = callAPI

    // This middleware expects to receive an array of 3 action types
    // eg `types: ['FETCH_PROJECT_REQUEST', 'RECEIVE_PROJECT', 'RECEIVE_ERROR']`
    // It uses these to handle api requests, the first to get data,
    // the second to handle success responses and the last failure responses.
    // eslint-disable-next-line no-magic-numbers
    if (!Array.isArray(types) || types.length !== 3) {
        throw new Error('Expected an array of three action types.')
    }

    if (typeof endpoint !== 'string') {
        throw new Error('Specify a string endpoint URL.')
    }

    const actionWith = actionData => {
        const finalAction = Object.assign({}, action, actionData)
        delete finalAction[CALL_API]
        return finalAction
    }
    const [requestType, successType, failureType] = types
    next(
        actionWith({
            type: requestType,
            ...callAPI,
        }),
    )

    return callApi(method, endpoint, authToken, mapIds, data).then(
        response =>
        next(
            actionWith({
                response,
                type: successType,
            }),
        ),
        error =>
        next(
            actionWith({
                type: failureType,
                error: error || {
                    message: 'Something went wrong',
                    status: 500,
                },
            }),
        ),
    )
}