import config from 'helpers/config'
import {
    FETCH_FAVOURITES_REQUEST,
    RECEIVE_FAVOURITES,
    RECEIVE_ERROR,
    SEND_ERROR,
    SEND_FAVOURITES_REQUEST,
    SENT_FAVOURITES_OK,
    SEND_DELETE_FAVOURITES_REQUEST,
    SENT_DELETE_FAVOURITES_OK,
} from 'redux/constants/ActionTypes'

import {
    updateObject
} from './utils'

const initialState = config.reducers.projects.initialState

export default function projects(state = initialState, action) {
    switch (action.type) {
        case FETCH_FAVOURITES_REQUEST:
            {
                return updateObject(state, {
                    currentLocale: action.currentLocale || state.currentLocale,
                    loading: true,
                })
            }
        case RECEIVE_FAVOURITES:
            {
                const entities = action.response.project || {}
                const ids = Object.values(entities).map(entity => entity.id)
                return updateObject(state, {
                    ...state,
                    entities,
                    ids,
                    loading: false,
                })
            }
        case RECEIVE_ERROR:
            {
                return updateObject(state, {
                    entities: {},
                    links: {},
                    meta: {},
                    loading: false,
                    error: action.error,
                })
            }
        case SEND_FAVOURITES_REQUEST:
        case SEND_DELETE_FAVOURITES_REQUEST:
            {
                return updateObject(state, {
                    ...state,
                    loading: true,
                })
            }
        case SENT_FAVOURITES_OK:
            {
                const ids = state.ids.slice()
                if (!ids.includes(action.response.id)) {
                    ids.push(action.response.id.toString())
                }
                return updateObject(state, {
                    ...state,
                    entities: {
                        ...state.entities,
                        [action.response.id]: {
                            ...state.entities[action.response.id],
                        },
                    },
                    loading: false,
                    ids,
                })
            }
        case SENT_DELETE_FAVOURITES_OK:
            {
                const ids = state.ids.filter(
                    item => item !== action.response.id.toString()
                )
                const {
                    [action.response.id]: values, ...entities
                } = state.entities
                return updateObject(state, {
                    ...state,
                    entities,
                    loading: false,
                    ids,
                })
            }
        case SEND_ERROR:
            {
                return updateObject(state, {
                    ...state,
                    error: action.error,
                    loading: false,
                })
            }
        default:
            {
                return state
            }
    }
}