import {
    combineReducers
} from 'redux'

import {
    reducer as authentication
} from 'helpers/authentication'

import favourites from './favourites'
import pathway from './pathway'
import project from './project'
import projects from './projects'
import progress from './progress'

export default combineReducers({
    favourites,
    pathway,
    progress,
    project,
    projects,
    authentication,
})