import {
    FETCH_PATHWAY_REQUEST,
    RECEIVE_PATHWAY,
    RECEIVE_ERROR,
} from 'redux/constants/ActionTypes'

const emptyPathway = {
    title: '',
    description: ' ',
    projectsIds: [],
}

const initialState = {
    error: null,
    loading: true,
    currentLocale: 'en',
    ...emptyPathway,
}

export default function pathway(state = initialState, action) {
    switch (action.type) {
        case FETCH_PATHWAY_REQUEST:
            {
                return Object.assign({}, state, {
                    ...emptyPathway,
                    error: null,
                    loading: true,
                    currentLocale: action.currentLocale || state.currentLocale,
                })
            }
        case RECEIVE_PATHWAY:
            {
                const fetchedPathway = action.response || {}
                return Object.assign({}, state, {
                    ...state,
                    ...fetchedPathway,
                    loading: false,
                })
            }
        case RECEIVE_ERROR:
            {
                return Object.assign({}, state, {
                    ...state,
                    ...emptyPathway,
                    error: action.error,
                    loading: false,
                })
            }
        default:
            {
                return state
            }
    }
}