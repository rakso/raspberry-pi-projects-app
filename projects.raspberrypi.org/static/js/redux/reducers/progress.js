import config from 'helpers/config'
import {
    FETCH_PROGRESS_REQUEST,
    RECEIVE_PROGRESS,
    RECEIVE_ERROR,
    SEND_ERROR,
    SEND_PROGRESS_REQUEST,
    SENT_PROGRESS_OK,
} from 'redux/constants/ActionTypes'

import {
    updateObject
} from './utils'

const initialState = config.reducers.projects.initialState

export default function progress(state = initialState, action) {
    switch (action.type) {
        case FETCH_PROGRESS_REQUEST:
            {
                return updateObject(state, {
                    currentLocale: action.currentLocale || state.currentLocale,
                    loading: true,
                })
            }
        case RECEIVE_PROGRESS:
            {
                const entities = action.response.project || {}
                const ids = Object.values(entities).map(entity => entity.id)
                return updateObject(state, {
                    ...state,
                    entities,
                    ids,
                    loading: false,
                })
            }
        case RECEIVE_ERROR:
            {
                return updateObject(state, {
                    ...state,
                    entities: {},
                    error: action.error,
                    loading: false,
                })
            }
        case SEND_PROGRESS_REQUEST:
            {
                return updateObject(state, {
                    ...state,
                    loading: true,
                })
            }
        case SENT_PROGRESS_OK:
            {
                const ids = state.ids.slice()
                if (!ids.includes(action.response.projectId)) {
                    ids.push(action.response.projectId.toString())
                }
                return updateObject(state, {
                    ...state,
                    entities: {
                        ...state.entities,
                        [action.response.projectId]: {
                            ...state.entities[action.response.projectId],
                            currentStep: action.response.step,
                        },
                    },
                    loading: false,
                    ids,
                })
            }
        case SEND_ERROR:
            {
                return updateObject(state, {
                    ...state,
                    error: action.error,
                    loading: false,
                })
            }
        default:
            {
                return state
            }
    }
}