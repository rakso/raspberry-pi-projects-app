import config from 'helpers/config'
import i18n from 'i18next'
import {
    stepSlideDirection,
    stepPositon
} from 'helpers/Step'
import {
    FETCH_PROJECT_REQUEST,
    RECEIVE_PROJECT,
    RECEIVE_ERROR,
    SET_PROJECT_STEP,
} from 'redux/constants/ActionTypes'

const initialState = config.reducers.project.initialState

// eslint-disable-next-line complexity
export default function project(state = initialState, action) {
    switch (action.type) {
        case FETCH_PROJECT_REQUEST:
            {
                const currentStepPosition = stepPositon(action.currentStepPosition)
                return Object.assign({}, state, {
                    currentLocale: action.currentLocale || state.currentLocale,
                    currentStepPosition,
                    error: null,
                    project: action.cachedProject ? action.cachedProject.content : {},
                    projectFetched: 0,
                    slideDirection: 'left',
                    slug: action.slug,
                })
            }
        case RECEIVE_PROJECT:
            {
                const fetchedProject =
                    Object.assign({}, action.response.content, {
                        id: action.response.id,
                        locale: action.response.locale,
                    }) || {}

                return Object.assign({}, state, {
                    ...state,
                    nextStepTitle: fetchedProject.steps[state.currentStepPosition] ?
                        fetchedProject.steps[state.currentStepPosition].title :
                        i18n.t('project.steps.navigation.whats-next'),
                    project: fetchedProject,
                    projectFetched: 1,
                })
            }
        case RECEIVE_ERROR:
            {
                const fetchedProject =
                    Object.assign({}, action.error.body.content, {
                        id: action.error.body.id,
                        availableLocales: action.error.body.availableLocales,
                    }) || {}
                return Object.assign({}, state, {
                    ...state,
                    currentStepPosition: null,
                    error: action.error,
                    project: fetchedProject,
                    projectFetched: action.error.body.availableLocales ? 1 : -1,
                    slug: null,
                })
            }
        case SET_PROJECT_STEP:
            {
                const currentStepPosition = stepPositon(action.currentStepPosition)
                const nextStepTitle =
                    state.project.steps && state.project.steps[currentStepPosition] ?
                    state.project.steps[currentStepPosition].title :
                    i18n.t('project.steps.navigation.whats-next')
                const slideDirection = stepSlideDirection(
                    action.currentStepPosition,
                    state.currentStepPosition,
                )

                return Object.assign({}, state, {
                    ...state,
                    currentStepPosition,
                    nextStepTitle,
                    slideDirection,
                })
            }
        default:
            {
                return state
            }
    }
}