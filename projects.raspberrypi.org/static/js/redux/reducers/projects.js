import config from 'helpers/config'
import {
    FETCH_PROJECTS_REQUEST,
    RECEIVE_PROJECTS,
    RECEIVE_ERROR,
} from 'redux/constants/ActionTypes'

import {
    updateObject
} from './utils'

const initialState = config.reducers.projects.initialState

export default function projects(state = initialState, action) {
    switch (action.type) {
        case FETCH_PROJECTS_REQUEST:
            {
                return updateObject(state, {
                    currentLocale: action.currentLocale || state.currentLocale,
                    loading: true,
                })
            }
        case RECEIVE_PROJECTS:
            {
                return updateObject(state, {
                    entities: action.response.projects || {},
                    links: action.response.links,
                    meta: action.response.meta,
                    loading: false,
                })
            }
        case RECEIVE_ERROR:
            {
                return updateObject(state, {
                    entities: {},
                    links: {},
                    meta: {},
                    loading: false,
                    error: action.error,
                })
            }
        default:
            {
                return state
            }
    }
}