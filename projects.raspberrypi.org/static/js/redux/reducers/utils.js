export const updateObject = (oldObject, newValues) =>
    Object.assign({}, oldObject, newValues)